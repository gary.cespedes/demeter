<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\BranchDetail;
use App\Models\Role;
use App\Models\TransferDetail;
use App\Traits\CategoryTrait;
use App\Traits\ClientTrait;
use App\Traits\BatchTrait;
use App\Traits\FileCabinetTrait;
use App\Traits\PermissionTrait;
use App\Traits\ProductTrait;
use App\Traits\ProviderTrait;
use App\Traits\QuotationTrait;
use App\Traits\SaleTrait;
use App\Traits\SubCategoryTrait;
use App\Traits\TransferTrait;
use App\Traits\UnitTrait;
use App\Traits\UserTrait;
use App\Traits\BranchTrait;
use App\Transfer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class WebController extends Controller
{
  use UserTrait;
  use BranchTrait;
  use ProductTrait;
  use CategoryTrait;
  use ProviderTrait;
  use TransferTrait;
  use BatchTrait;
  use UnitTrait;
  use SaleTrait;
  use ClientTrait;
  use FileCabinetTrait;
  use QuotationTrait;
  use PermissionTrait;
  use SubCategoryTrait;

  static $type_status = [
      0 => 'Cancelado',
      1 => 'Pendiente'
  ];

  static $type_sale = [
//    0 => 'Factura',
    1 => 'Orden de Venta',
//    2 => 'Boleta',
//    3 => 'Factura',
    4 => 'Cotizacion'
  ];

  static $payment_type = [
//    0 => 'Factura',
    1 => 'Efectivo',
    2 => 'Tarjeta',
    3 => 'Deposito',
    4 => 'Credito'
  ];

  static $type_document = [
    0 => 'CI',
    1 => 'RUC',
  ];

  static $type_account_bank = [
    0 => 'account1',
    1 => 'account2',
  ];

  static $issued_in = [
    0 => 'La Paz LP',
    1 => 'Santa Cruz SC',
    2 => 'Cochabamba CB',
    3 => 'Chuquisaca CH',
    4 => 'Oruro OR',
    5 => 'Potosi PT',
    6 => 'Tarija TJ',
    7 => 'Beni BE',
    8 => 'Pando PA',
  ];

  static $modules = [
//    'users' => 'Usuarios',
    'products' => 'Productos',
//    'branch' => 'Sucursal',
    'branch_detail' => 'Stock Sucursal',
    'batch_detail' => 'Lote',
//    'categories' => 'Categorias',
//    'prices' => 'Precios',
//    'providers' => 'Proveedores',
//    'units' => 'Unidades',
  ];

  function revert_transfer(){

    $transfers = Transfer::get();

    foreach ($transfers as $item){
      $transfers = \DB::table('transfers as t')
        ->where( 't.id' , $item->id)
        ->where( 't.date_transfer_end','<' , Carbon::now())
        ->where( 't.status','<>' , 3)
        ->where( 't.status','<>' , 5)
        ->update([
          'status' => 4
        ]);

      if ($transfers){
        $revert = TransferDetail::where('transfer_id', $item->id)->get();
        foreach ($revert as $element){
          BranchDetail::where('warehouse_id', $item->origin_id)
            ->where('product_id', $element->product_id)
            ->increment('stock', $element->quantity);

          $detail_warehouses = BranchDetail::where('warehouse_id', $item->destine_id)->where('product_id', $element->product_id)->first();
          ($detail_warehouses) ? BranchDetail::where('warehouse_id',$item->destine_id)->where('product_id', $element->product_id)->update([
            'stock' => $detail_warehouses->stock - $item->quantity
          ]) : BranchDetail::create($element);
        }
      }
    }
  }


  function unserialize_form($str) {
    $returndata = array();
    $strArray = explode("&", $str);
    $i = 0;
    foreach ($strArray as $item) {
      $array = explode("=", $item);
      $returndata[$array[0]] = $array[1];
    }

    return $returndata;
  }

  static function generate_code($number, $max_characters = 6){
    $number = (string) $number;
    $max_characters -= strlen($number);
    for ($i = 0; $i < $max_characters; $i++){
      $number = '0'.$number;
    }
    return $number;
  }


  function validator($data, $rules, $messages=null, $customAttributes=null){

    $validator = $this->getValidationFactory()
      ->make($data, $rules);
    return $validator;

  }

  static function get_branch(){
      $branch = Branch::find(Auth::user()->branch_id);
      return  ($branch) ? $branch->name : '' ;
  }

}

//$web = new WebController();
//$web->revert_transfer();
