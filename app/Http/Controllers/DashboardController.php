<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function dashboardEcommerce(){

      $quantity_sales = Sale::where('date_sale', Carbon::today())->count();
      $total_sale = Sale::where('date_sale', Carbon::today())->sum('price_end');
      $sales = Sale::where('date_sale', Carbon::today())
        ->where('branch_id', Auth::user()->branch_id)
      ->get();

      return view('pages.dashboard-ecommerce', compact('quantity_sales', 'total_sale', 'sales'));

    }
    // analystic
    public function dashboardAnalytics(){
        return view('pages.dashboard-analytics');
    }
}
