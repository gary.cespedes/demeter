<?php

namespace App\Exports;

use App\Models\FileCabinet;
use App\Models\Product;
use App\Models\Branch;
use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class KardexExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
  private $request;

  public function __construct($request)
  {
    $this->request = $request;
  }

  public function view($view = "file_cabinet"): View
  {
    $data_export = FileCabinet::where('product_id', $this->request->product_id)
      ->where('warehouse_id', $this->request->warehouse_id)
      ->whereBetween('date', [$this->request->date_start, $this->request->date_end])
      ->get();

    $this->request['branch'] = Branch::find($this->request->warehouse_id);
    $this->request['product'] = Product::find($this->request->product_id);
    $filters = $this->request;

    return view('exports.'.$view,
      compact('data_export', 'filters'));
  }
}
