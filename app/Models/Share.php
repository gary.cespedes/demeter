<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Share extends Model
{
  use SoftDeletes;
  protected $keyType = 'string';
  public $incrementing = false;

  protected $fillable = [
    'id','code', 'payment_id', 'date_payment', 'amount', 'user_id'
  ];
}
