<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    use SoftDeletes, Uuid;
    protected $keyType = 'string';
    protected $table = 'batch';
    public $incrementing = false;
    protected $fillable = [
      'id', 'code',
      'date_into',
      'note_batch',
      'is_paid_out',
      'user_id',
      'provider_id',
      'branch_id'
    ];

  protected static function boot()
  {
    parent::boot();

    static::updating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

    static::creating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

  }
}
