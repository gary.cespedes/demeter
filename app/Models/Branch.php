<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
  use SoftDeletes, Uuid;

  protected $table = 'branch';
  protected $keyType = 'string';
  public $incrementing = false;
  protected $fillable = [
    'id','name','address' , 'phone', 'phone_alternative','user_id', 'is_warehouse'
  ];
}
