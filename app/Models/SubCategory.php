<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
  use SoftDeletes, Uuid;

  protected $table = 'sub_categories';
  protected $keyType = 'string';
  public $incrementing = false;

  protected $fillable = [
    'id','name', 'description', 'category_id', 'user_id'
  ];
}
