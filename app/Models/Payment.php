<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
  use SoftDeletes, Uuid;

  protected $keyType = 'string';
  public $incrementing = false;

  protected $fillable = [
    'id',
    'code',
    'sale_id',
    'provider_id',
    'payment_type',
    'amount_total',
    'user_id',
    'is_paid_out',
    'next_payment_date',
    'days',
    'money_fine'
  ];
}
