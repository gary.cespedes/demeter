<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provieder extends Model
{
  use SoftDeletes, Uuid;
  protected $table = 'providers';
  protected $keyType = 'string';
  public $incrementing = false;
  protected $fillable = [
    'id','name', 'email', 'phone', 'type_document', 'number_document',  'user_id'

  ];
}
