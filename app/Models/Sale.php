<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
  use SoftDeletes, Uuid;
  protected $keyType = 'string';
  public $incrementing = false;
  protected $fillable = [
    'id','code','sale_note', 'date_sale','type_sale',
    'price_initial', 'price_end', 'user_id', 'branch_id',
    'payment_type', 'client_id'
  ];
}
