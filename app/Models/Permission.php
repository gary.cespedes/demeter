<?php

namespace App\Models;

use App\User;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
  use SoftDeletes, Uuid;

  protected $keyType = "string";
  public $incrementing = false;
  protected $fillable = [
    'id', 'name', 'module', 'visible'
  ];

  public function users(){
    return $this->belongsToMany(User::class, 'users');
  }
}
