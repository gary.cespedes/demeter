<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileCabinet extends Model
{
    use SoftDeletes, Uuid;
    protected $table = 'file_cabinets';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
      'id','code', 'description','code_operation',
      'quantity_input', 'quantity_out', 'price_unit', 'income_out',
      'income_input','product_id', 'branch_id','date'
    ];
}
