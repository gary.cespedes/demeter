<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeDocument extends Model
{

  protected $table = 'type_documents';
  protected $fillable = [
    'name'
  ];
}
