<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
  use SoftDeletes, Uuid;

  protected $keyType = 'string';
  public $incrementing = false;

  protected $fillable = [
    'id',
    'code',
    'name',
    'description',
    'quantity_minim',
    'user_id',
    'unit_id',
    'category_id'
  ];

  protected static function boot()
  {
    parent::boot();

    static::updating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

    static::creating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

  }
}
