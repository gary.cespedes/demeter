<?php

namespace App;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transfer extends Model
{
    use SoftDeletes, Uuid;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
      'id', 'code','note_transfer', 'date_transfer',
      'branch_origin_id', 'branch_destine_id', 'user_id','status'
    ];

  protected static function boot()
  {
    parent::boot();

    static::updating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

    static::creating(function ($model) {
      $model->user_id = \Auth::user()->id;
    });

  }
}
