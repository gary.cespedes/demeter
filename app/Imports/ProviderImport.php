<?php

namespace App\Imports;

use App\Models\Provieder;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ProviderImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Provieder([
          'id' => $row[0],
          'name' => $row[1],
          'phone' => $row[2],
          'email' => $row[3],
          'type_document' => 0,
          'number_document' => $row[5],
          'user_id' => $row[6],
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
