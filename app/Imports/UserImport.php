<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class UserImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
          'name' => $row[0],
          'email' => $row[1],
          'password' => bcrypt($row[2]),
          'branch_id' => $row[3],
          'photo' => 'image/profile/user-uploads/user_profile.png',
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
