<?php

namespace App\Imports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class CategoryImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      if(!array_filter($row)) {
        return null;
      }
      return new Category([
          'id' => $row[0],
          'name' => $row[1],
          'user_id' => $row[3]
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
