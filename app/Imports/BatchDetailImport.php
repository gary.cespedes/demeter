<?php

namespace App\Imports;

use App\Models\BatchDetail;
use App\Models\Product;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BatchDetailImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    protected $batch_id;
    function __construct($batch_id)
    {
      $this->batch_id = $batch_id;
    }

  public function model(array $row)
    {
        return new BatchDetail([
            'product_id' => Product::where('code', $row[0])->firstOrFail()->id,
            'quantity' => ($row[1]) ? $row[1] : 0,
            'cost' => ($row[2]) ? $row[2] : 0,
            'batch_id' => $this->batch_id,
            'total' => $row[1] * $row[2],
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
