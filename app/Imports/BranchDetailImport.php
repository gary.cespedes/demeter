<?php

namespace App\Imports;

use App\Models\Branch;
use App\Models\BranchDetail;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BranchDetailImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      if(!array_filter($row)) {
        return null;
      }

      return new BranchDetail([
            'product_id' => Product::where('code', $row[0])->firstOrFail()->id,
            'stock' => ($row[1]) ? $row[1] : 0,
            'acquisition_price' => $row[2],
            'branch_id' => Auth::user()->branch_id
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
