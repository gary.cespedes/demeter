<?php

namespace App\Imports;

use App\Models\BranchDetail;
use Maatwebsite\Excel\Concerns\ToModel;

class DetailWarehouseImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new BranchDetail([
            'id' => $row[0],
            'stock' => $row[1],
            'product_id' => $row[2],
            'user_id' => $row[3],
            'warehouse_id' => $row[4]
        ]);
    }
}
