<?php

namespace App\Imports;

use App\Models\Branch;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BranchImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Branch([
            'id' => $row[0],
            'name' => $row[1],
            'address' => $row[2],
            'phone' => $row[3],
          'user_id' => $row[6]
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
