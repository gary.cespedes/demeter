<?php

namespace App\Imports;

use App\Models\Unit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class UnitImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Unit([
          'id' => $row[0],
          'name' => $row[1],
          'description' => $row[2],
          'user_id' => $row[3]
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
