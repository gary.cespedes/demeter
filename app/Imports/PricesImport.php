<?php

namespace App\Imports;

use App\Models\Price;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PricesImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      if(!array_filter($row)) {
        return null;
      }

      $prices =  (new Price([
          'product_id' => $row[0],
          'description' => 'Precio Unitario',
          'price' => ($row[8]) ? $row[8] : 0,
          'is_predetermined' => 1,
          'user_id' => $row[6]
        ]))->save();

      return new Price([
        'product_id' => $row[0],
        'description' => 'Precio Oferta',
        'price' => $row[9],
        'is_predetermined' => 0,
        'user_id' => $row[6]
      ]);

    }

  public function startRow(): int
  {
    return 2;
  }
}
