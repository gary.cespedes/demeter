<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Price;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ProductImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      if(!array_filter($row)) {
        return null;
      }

      $category = Category::firstOrCreate([
        'name' => $row[3],
      ], ['name' => $row[3]])->id;

      $unit = Unit::firstOrCreate([
        'name' => $row[7],
      ], ['name' => $row[7]])->id;

      $product = Product::create([
        'code' => $row[0],
        'name' => $row[1],
        'description' => $row[2],
        'category_id' => $category,
        'quantity_minim' => ($row[6]) ? $row[6] : 0,
        'unit_id' => $unit,
      ]);


      (new Price([
        'product_id' => $product->id,
        'description' => 'Precio Unitario',
        'price' => ($row[4]) ? $row[4] : 0,
        'is_predetermined' => 1,
      ]))->save();

      return new Price([
        'product_id' => $product->id,
        'description' => 'Precio Oferta',
        'price' => ($row[5]) ? $row[5] : 0,
        'is_predetermined' => 0,
      ]);

    }

  public function startRow(): int
  {
    return 2;
  }
}
