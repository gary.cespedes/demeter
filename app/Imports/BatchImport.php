<?php

namespace App\Imports;

use App\Models\Batch;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BatchImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Batch([
          'id' => $row[0],
          'code' => Batch::count() + 1,
          'date_into' => Carbon::now(),
          'note_batch' => $row[3],
          'user_id' => $row[4],
          'provider_id' => $row[5],
          'branch_id' => $row[6],
        ]);
    }

  public function startRow(): int
  {
    return 2;
  }
}
