<?php


namespace App\Traits;

use App\Models\BankAccounts;
use App\Models\Client;
use App\Models\Role;
use App\Models\TypeDocument;
use App\TypeAccountBank;
use App\User;
use App\Http\Controllers\WebController as webController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait ClientTrait {

  private function rules_client(){
    return  [
      "name" => 'required|min:4|max:40',
    ];
  }

  public function client_index()
  {
    $pageConfigs = ['pageHeader' => true];

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentProvider')]
    ];
    $type_account_bank = BankAccounts::select('name_bank as id', 'name_bank')
      ->get();

    return view('client.index', compact('breadcrumbs','type_account_bank'));

  }


  public function client_edit_view(Request $request)
  {
    $attribute_find = AttributeModel::find($request->id);
    return response()->json([
      'data' => $request->id,
      'view' => view('attribute.edit', compact('attribute_find'))->render()
    ]);
  }

  public function client_register_view(Request $request)
  {
    $roles = Role::get();
    return response()->json([
      'data' => 0,
      'view' => view('client.register', compact('roles'))->render()
    ]);
  }

  public function store_client(Request $request)
  {
    $validate = $this->validator($request->all(), $this->rules_client());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $request['user_id'] = Auth::user()->id;
    $client = Client::create($request->all());

    return response()->json([
      'data' => $client
    ], 200);
  }

  public function update_client(Request $request)
  {
    Client::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function client_delete(Request $request)
  {
    Client::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_clients(){
    $clients = \DB::table('clients')->where('deleted_at', null)->get();
    return response()->json([
      'data' => $clients
    ]);
  }

  function get_client(Request $request){

    $client = \DB::table('clients')->where('deleted_at', null)->where('id', $request->id)->first();
    return response()->json([
      'data' => $client
    ]);
  }

}

