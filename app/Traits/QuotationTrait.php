<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Client;
use App\Models\SaleDetail;
use App\Models\BranchDetail;
use App\Models\Batch;
use App\Models\BatchDetail;
use App\Models\FileCabinet;
use App\Models\Mesures;
use App\Models\OriginProduct;
use App\Models\Product;
use App\Models\Provieder;
use App\Models\Role;
use App\Models\Sale;
use App\Models\TypeDocument;
use App\Models\TypeFabrication;
use App\Models\TypeProduct;
use App\Models\Unit;
use App\TypeAccountBank;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;

trait QuotationTrait {


  public function quotation_index(Request $request)
  {
    $pageConfigs = ['pageHeader' => true];
    $warehouse = Branch::find(Auth::user()->warehouse_id);
    $clients = Client::get();
    $request['type_sale'] = 1;
    $products = $this->get_products_by_filters($request);
    $type = TypeDocument::select('name as id', 'name')
      ->get();

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentSale')]
    ];

    return view('quotation.index',
      compact('breadcrumbs',
        'products',
        'categories',
        'warehouse',
        'type',
        'clients'));
  }

  public function quotation_edit(Request $request)
  {
    $pageConfigs = ['pageHeader' => true];
    $warehouse = Branch::find(Auth::user()->warehouse_id);
    $clients = Client::get();
    $request['type_sale'] = 1;
    $products = $this->get_products_by_filters($request);
    $type = TypeDocument::select('name as id', 'name')
      ->get();

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentSale')]
    ];

    $sale = Sale::where('id', $request->id)->withTrashed()->first();

    $detail_sale = SaleDetail::where('sale_id', $request->id)
      ->get();

    return view('sale.edit',
      compact('pageConfigs',
        'breadcrumbs',
        'products',
        'categories',
        'warehouse',
        'type',
        'clients', 'sale','detail_sale'));
  }

  public function quotation_list(Request $request)
  {
    $pageConfigs = ['pageHeader' => true];

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentSale')]
    ];

    return view('sale.list-sale');
  }

  public function store_quotation(Request $request)
  {

    $data = $this->unserialize_form($request->data);

    $validate = $this->validator($data, $this->rules_sale());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $number_branch = Branch::where('id',Auth::user()->warehouse_id)->first();

    $sales = Sale::where('warehouse_id', Auth::user()->warehouse_id)
      ->withTrashed()
      ->count();
    $sales = $sales + 1;

    if (!isset($data['date_sale'])){
      $data['date_sale'] = Carbon::now();
    }

    if (!isset($data['user_id'])){
      $data['user_id'] = Auth::user()->id;
    }

    $data['code_sale'] = $number_branch->code.'-'.WebController::generate_code($sales);
    $data['warehouse_id'] = Auth::user()->warehouse_id;
    $data['price_initial'] = $request->price_initial;
    $data['price_end'] = $request->price_end;
    $sale = Sale::create($data);

    if (isset($request['attributeListArray'])){
      foreach ($request['attributeListArray'] as $attribute){
        if ($attribute !== null){
          $element= [
            'product_id' => $attribute['product_id'],
            'price_unit' => $attribute['price'],
            'quantity' => $attribute['quantity'],
            'price_total' => $attribute['total_price'],
            'sale_id' => $sale->id,
            'user_id' => Auth::user()->id,
          ];

          FileCabinet::create([
            'code_kardex' => WebController::generate_code(FileCabinet::count() + 1),
            'description' => 'Venta',
            'code_operation' => $sale->code_sale,
            'quantity_input' => 0,
            'quantity_out' => $attribute['quantity'],
            'price_unit' => $attribute['price'],
            'income_out' => 0,
            'income_input' => $attribute['total_price'],
            'product_id' => $attribute['product_id'],
            'warehouse_id' => $sale->warehouse_id,
            'date' => $sale->date_sale
          ]);

          SaleDetail::create($element);

          BranchDetail::where('warehouse_id',$sale->warehouse_id)
            ->where('product_id', $attribute['product_id'])
            ->decrement('stock', $attribute['quantity']);
        }
      }
    }

    return response()->json(['data' => $sale], 200);
  }

  public function quatation_delete(Request $request)
  {
    $sale = Sale::find($request->id);
    $detail = SaleDetail::where('sale_id', $sale->id)->get();
    foreach ($detail as $item){

      BranchDetail::where('warehouse_id',$sale->warehouse_id)
        ->where('product_id', $item->product_id)
        ->increment('stock', $item->quantity);

      FileCabinet::create([
        'code_kardex' => WebController::generate_code(FileCabinet::count() + 1),
        'description' => 'Anulacion de Venta',
        'code_operation' => $sale->code_sale,
        'quantity_input' => $item->quantity,
        'quantity_out' => 0,
        'price_unit' => $item->price_unit,
        'income_out' => $item->price_total,
        'income_input' => 0,
        'product_id' => $item->product_id,
        'warehouse_id' => $sale->warehouse_id,
        'date' => Carbon::now()
      ]);
    }
    $sale->delete();

    return response()->json(['message' => 'Great!'], 200);
  }

  function get_quotations(Request $request){
    if (isset($request->date_start)){
      $sale = \DB::table('sales as s')
        ->select('s.id',
          's.code_sale',
          's.type_sale',
          's.date_sale',
          's.price_initial',
          's.price_end'
        )
        ->where('deleted_at', null)
        ->whereBetween('date_sale', [$request->date_start, $request->date_end])
        ->get();
    }
    else{
      $sale = \DB::table('sales as s')
        ->select('s.id',
          's.code_sale',
          's.type_sale',
          's.date_sale',
          's.price_initial',
          's.price_end'
        )
        ->where('deleted_at', null)
        ->get();
    }

    return response()->json([
      'data' => $sale
    ]);
  }

  function get_quotations_inactive(Request $request){
    $sale = \DB::table('sales as s')
      ->select('s.id',
        's.code_sale',
        's.type_sale',
        's.date_sale',
        's.price_initial',
        's.price_end'
      )
      ->where('deleted_at','<>' ,null)
      ->get();

    return response()->json([
      'data' => $sale
    ]);
  }

  function get_quotation(Request $request){

    $sale = Batch::find($request->id)->first();
    return response()->json([
      'data' => $sale
    ]);
  }

  public function print_all_quotation(){
    $pdf = App::make('dompdf.wrapper');
    $sales = Sale::get();
    $sum = Sale::sum('price_end');
    $pdf->loadHTML(view('pdf.sale_all_small', compact('sales', 'detail_sale', 'sum')));
    $pdf->setPaper('a7','portrait');
    return $pdf->stream('sale_detail.pdf', array('Attachment'=> 1));
  }

  public function print_quotation(Request $request)
  {
    $pdf = App::make('dompdf.wrapper');

    $sale = Sale::join('users as u', 'sales.user_id','=','u.id')
      ->join('clients as c', 'sales.client_id', '=', 'c.id')
      ->select('sales.*', 'u.name as user', 'c.name', 'c.last_name')
      ->where('sales.id',$request->id)
      ->first();

    $type_sale = WebController::$type_sale[$sale->type_sale];

    $formatter = new NumeroALetras();
    $mont_to_letter = $formatter->toMoney($sale->price_end, 2, 'SOLES', 'CENTAVOS');

    $detail_sale = \DB::table('detail_sales as ds')
      ->join('products as p', 'ds.product_id', '=', 'p.id')
      ->join('sales as s', 'ds.sale_id', '=', 's.id')
      ->select('ds.id', 'p.name', 'p.description', 's.date_sale' ,'ds.price_unit', 'ds.quantity', 'ds.price_total', 'p.code')
      ->where('sale_id', $request->id)
      ->get();

//    $pdf->loadHTML(view('pdf.sale_detail', compact('sale', 'detail_sale', 'matrix', 'mont_to_letter', 'type_sale')));
    $pdf->loadHTML(view('pdf.sale_small', compact('sale', 'detail_sale', 'matrix', 'mont_to_letter', 'type_sale')));
    $pdf->setPaper('a7','portrait');
    return $pdf->stream('sale_detail.pdf', array('Attachment'=> 1));

  }


}

