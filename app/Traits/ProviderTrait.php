<?php


namespace App\Traits;

use App\Models\BankAccounts;
use App\Models\Price;
use App\Models\Provieder;
use App\Models\Role;
use App\Models\TypeDocument;
use App\TypeAccountBank;
use App\User;
use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait ProviderTrait {

  private function rules_provider(){
    return  [
      "name" => 'required|min:4|max:40',
      "email" => 'required|email',
    ];
  }

  public function provider_index()
  {
    $pageConfigs = ['pageHeader' => true];

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentProvider')]
    ];

    $type_account_bank = \App\Http\Controllers\WebController::$type_account_bank;
    //$type_account_bank = BankAccounts::select('name_bank as id', 'name_bank')->get();

    return view('provider.index', compact('breadcrumbs', 'type_account_bank'));

  }


  public function provider_edit_view(Request $request)
  {
    $attribute_find = AttributeModel::find($request->id);
    return response()->json([
      'data' => $request->id,
      'view' => view('attribute.edit', compact('attribute_find'))->render()
    ]);
  }

  public function provider_register_view(Request $request)
  {
    $roles = Role::get();
    return response()->json([
      'data' => 0,
      'view' => view('provider.register', compact('roles'))->render()
    ]);
  }

  public function store_provider(Request $request)
  {
    //dd($request);
    $request['attributeListArray'] = json_decode($request->attributeListArray);
    $validate = $this->validator($request->all(), $this->rules_provider());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $request['user_id'] = Auth::user()->id;
    $provider = Provieder::create($request->all());

    if (isset($request['attributeListArray']) && ($request['attributeListArray'] != null)){
      foreach ($request['attributeListArray'] as $attribute){
        $element= [
          'name_bank' => $attribute->name_bank,
          'number_account' => $attribute->number_account,
          'type_account_bank' => $attribute->type_account_bank,
          'user_id' => Auth::user()->id,
          'provider_id' => $provider->id,
        ];
        BankAccounts::create($element);
      }
    }

    return response()->json(['data' => $provider], 200);
  }

  public function update_provider(Request $request)
  {
    Provieder::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function provider_delete(Request $request)
  {
    Provieder::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_providers(){
    $providers = \DB::table('providers')->where('deleted_at', null)->get();
    return response()->json([
      'data' => $providers
    ]);
  }

  function get_provider(Request $request){

    $provider = \DB::table('providers')->where('deleted_at', null)->where('id', $request->id)->first();
    return response()->json([
      'data' => $provider
    ]);
  }

}

