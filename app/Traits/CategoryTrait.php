<?php


namespace App\Traits;

use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use Illuminate\Http\Request;

trait CategoryTrait {

  private function rules_category(){
    return  [
      "name" => 'required',
    ];
  }

  public function category_index()
  {

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentCategory')]
    ];

    return view('category.index', compact('breadcrumbs'));
  }

  public function store_category(Request $request)
  {
    $validate = $this->validator($request->all(), $this->rules_category());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $category = Category::create($request->all());

    return response()->json(['data' => $category], 200);
  }

  public function update_category(Request $request)
  {
    Category::findOrFail($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function category_delete(Request $request)
  {
    Category::findOrFail($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_categories(Request $request){

    $categories = (isset($request->deleted) ? Category::onlyTrashed()->get() : Category::get());

    return response()->json([
      'data' => $categories
    ]);
  }

  function get_category(Request $request){
    $category = Category::findOrFail($request->id);
    return response()->json([
      'data' => $category,
      'view' => view('category.show', compact('category'))->render()
    ]);
  }

}

