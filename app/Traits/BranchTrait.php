<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Models\Branch;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait BranchTrait {

  private function rules_branch(){
    return  [
      "name" => 'required|min:4|max:40',
    ];
  }

  public function branch_index()
  {
    return view('branch.index');
  }


  public function get_branch_with_stock(Request $request)
  {
    $warehouse = \DB::table('branch_details as bd')
      ->join('products as p', 'bd.product_id', '=', 'p.id')
      ->join('prices as pr', 'p.id', '=', 'pr.product_id')
      ->join('branch as b', 'bd.branch_id', '=', 'b.id')
      ->select('b.id','b.name','bd.stock','p.name as product', \DB::raw('GROUP_CONCAT(CONCAT(pr.description,":", pr.price)) as price'))
      ->groupBy('b.id', 'b.name', 'bd.stock', 'p.name')
      ->where('bd.product_id', $request->id)
      ->where('bd.stock','>', 0)
      ->where('pr.price','>', 0)
      ->get();


    $warehouse->map(function($column) {
      $column->price = explode(',', $column->price);
    });

    return response()->json(['data' => $warehouse], 200);
  }

  public function branch_register_view(Request $request)
  {
    $roles = Role::get();
    return response()->json([
      'data' => 0,
      'view' => view('branch.register', compact('roles'))->render()
    ]);
  }

  public function store_branch(Request $request)
  {
    $validate = $this->validator($request->all(), $this->rules_branch());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $count = Branch::count() + 1;

    $request['user_id'] = Auth::user()->id;
    $request['code'] = 'A'.WebController::generate_code($count, 2);
    Branch::create($request->all());

    return response()->json(['message' => 'Great!'], 200);
  }

  public function update_branch(Request $request)
  {
    Branch::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function branch_delete(Request $request)
  {
    Branch::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_branches(){
    $branches = \DB::table('branch as b')
      ->join('users as u', 'b.user_id', '=', 'u.id')
      ->select('b.*', 'u.name as user')
      ->where('b.deleted_at', null)->get();
    return response()->json([
      'data' => $branches
    ]);
  }

  function get_branchs(Request $request){

    $branch = Branch::findOrFail($request->id);

    return response()->json([
      'data' => $branch
    ]);
  }

  function get_detail_branch(Request $request){
    $branch = \DB::table('branch_details as bd')
      ->join('products as p', 'bd.product_id', '=', 'p.id')
      ->join('branch as b', 'bd.branch_id', '=', 'b.id')
      ->select('p.id as product_id', 'p.name as product_name',
      'bd.id', 'bd.stock', 'p.code as product_code')
      ->where('bd.deleted_at', null)
      ->where('p.id', $request->product_id)
      ->where('b.id', $request->id)
      ->first();
    return response()->json([
      'data' => $branch
    ]);
  }

  function change_origin(Request $request){
    $products = \DB::table('detail_warehouses as dw')
      ->join('warehouses as w', 'dw.warehouse_id', '=','w.id')
      ->join('products as p', 'dw.product_id', '=','p.id')
      ->select('p.id', 'p.name as text')
      ->where('w.id', $request->id)
      ->where('dw.stock','>', 0)
      ->get();

    return response()->json(['data' => $products]);
  }

}

