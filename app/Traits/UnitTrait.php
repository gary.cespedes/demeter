<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use App\Models\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait UnitTrait {

  private function rules_unit(){
    return  [
      "name" => 'required|min:4|max:40',
    ];
  }

  public function unit_index()
  {
    $units = Unit::get();
    return view('unit.index', compact( 'units'));
  }

  public function store_unit(Request $request)
  {
    $validate = $this->validator($request->all(), $this->rules_unit());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $unit = Unit::create($request->all());

    return response()->json(['data' => $unit], 200);
  }

  public function update_unit(Request $request)
  {
    Unit::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function unit_delete(Request $request)
  {
    Unit::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_unit(Request $request){

    $unit = Unit::findOrFail($request->id);
    return response()->json([
      'data' => $unit
    ]);
  }

  function get_units(Request $request){

    $units = (isset($request->deleted) ? Unit::onlyTrashed()->get() : Unit::get());
    return response()->json([
      'data' => $units
    ]);
  }

}

