<?php
namespace App\Traits;


use App\Models\Permission;
use App\Models\PermissionTemplate;
use App\User;
use Illuminate\Http\Request;

trait PermissionTrait{

    public function user_permission_view(Request $request){
        $user = User::find($request->id);
        $templates = PermissionTemplate::get();
        return response()->json([
          'view' => view('user.permission', compact('user','templates' ))->render()
        ]);
    }

    public function list_module_permission(Request $request)
    {
        $array_permission_module = [];
        $names_module = \DB::table('permissions')->groupBy('module')->pluck('module');
        foreach ($names_module as $name_module){
            $array_permission_user = [];
            $names_of_module = \DB::table('permissions')
                ->where('module', $name_module)
                ->get();


            foreach ($names_of_module as $name_of_module){

                $user_permission = [];
                if (\DB::table('user_permissions')->where('responsible_user_id', $request->id)
                    ->where('permission_id', $name_of_module->id)
                    ->exists()) {
                    $user_permission = [
                      'permission' => $name_of_module->name,
                      'value' => 1,
                      'id' => $name_of_module->id,
                    ];
                }
                else{
                    $user_permission = [
                        'permission' => $name_of_module->name,
                        'value' => 0,
                        'id' => $name_of_module->id,

                    ];
                }
                array_push($array_permission_user, $user_permission );
            }
            $array_union = [
                'name_module' => $name_module,
                'list' => $array_permission_user
            ];
            array_push($array_permission_module, $array_union);

        }
        return response()->json($array_permission_module);
    }

}
