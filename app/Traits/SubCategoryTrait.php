<?php


namespace App\Traits;

use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait SubCategoryTrait {

  private function rules_sub_category(){
    return  [
      "name" => 'required',
    ];
  }

  public function sub_category_index()
  {

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentCategory')]
    ];

    return view('category.index', compact('breadcrumbs'));
  }

  public function store_sub_category(Request $request)
  {
    $validate = $this->validator($request->all(), $this->rules_category());

    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }
    $request['user_id'] = Auth::user()->id;
    $sub_category = SubCategory::create($request->all());

    return response()->json(['data' => $sub_category], 200);
  }

  public function update_sub_category(Request $request)
  {
    SubCategory::findOrFail($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function sub_category_delete(Request $request)
  {
    SubCategory::findOrFail($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_sub_categories(Request $request){

    $sub_categories = (isset($request->deleted) ? SubCategory::onlyTrashed()->get() : SubCategory::get());

    return response()->json([
      'data' => $sub_categories
    ]);
  }

  public function get_sub_category(Request $request){
    $id = $request->id;
    
    $sub_category = DB::table('sub_categories')->where('category_id', $id)->get();
    $cat = DB::table('categories')->where('id', $id)->get();
    //dd($sub_category);
    $category = Category::findOrFail($request->id);
    
    return response()->json([
      'data' => $sub_category,
      'view' => view('category.show', compact('category','sub_category'))->render()
    ]);
  }

  
  function sub_category_get(Request $request){
    $sub_category = SubCategory::findOrFail($request->id);
    return response()->json([
      'data' => $sub_category,
    ]);
  }
}

