<?php


namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\Branch;
use App\User;

trait UserTrait {

  private function rules_user(){
    return  [
      "name" => 'required|min:4|max:40',
      "email" => 'required|email',
      "password" => 'required',
    ];
  }

  public function user_index()
  {
    $pageConfigs = ['pageHeader' => true];
    
    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentUser')]
    ];

    $branches = Branch::all();

    return view('user.index', compact('breadcrumbs', 'branches'));
  }

  public function store_user(Request $request)
  {

    $validate = $this->validator($request->all(), $this->rules_user());

    if ($validate->fails()){
      return response()->json(['data' => -1, 'validate_message' => $validate->messages()], 200);
    }

    $data = \DB::transaction(function () use ($request) {
      try{
        $request['password'] = bcrypt($request['password']);
        $request['user_id'] = Auth::user()->id;

        if (isset($request->file)){
          $file = $request->file('file');
          $name_image =  $file->getClientOriginalName();
          $path = Storage::disk('public')->put('image/profile/user-uploads/'.$name_image, \File::get($file));
          $request['photo'] = 'image/profile/user-uploads/'.$name_image;
        }

        $user = User::create($request->all());
        \DB::commit();
        return $user;

      }catch (\Exception $e){
        \DB::rollback();
        return -1;
      }
    });

    return response()->json(['data' => $data], 200);
  }

  public function update_user(Request $request)
  {

    $data = \DB::transaction(function () use ($request) {

      try {


        if (isset($request['password']) && !empty($request['password'])) {
          $request['password'] = bcrypt($request['password']);
        }
        else{
          unset($request['password']);
        }

        if (!isset($request['is_power_user']) ) {
          $request['is_power_user'] = 0;
        }

        if (isset($request->file)) {
          $file = $request->file('file');
          $name_image = $file->getClientOriginalName();
          $path = Storage::disk('public')->put('image/profile/user-uploads/' . $name_image, \File::get($file));
          $request['photo'] = 'image/profile/user-uploads/' . $name_image;
        }

        $user = User::findOrFail($request->id)->update($request->all());
        \DB::commit();
        return $user;
      } catch (\Exception $e) {
        \DB::rollback();
        return -1;
      }
    });

    return response()->json(['data' => $data], 200);
  }

  public function user_delete(Request $request)
  {
    User::findOrFail($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_users(Request $request){
    $users = (isset($request->deleted) ? User::onlyTrashed()->get() : User::get() );

    return response()->json([
      'data' => $users
    ]);
  }

  function get_user(Request $request){

    $user = User::findOrFail($request->id);

    return response()->json([
      'data' => $user
    ]);

  }

}

