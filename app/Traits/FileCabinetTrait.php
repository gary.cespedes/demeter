<?php


namespace App\Traits;

use App\Exports\KardexExport;
use App\Http\Controllers\WebController;
use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use App\Models\Branch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

trait FileCabinetTrait {

  public function file_cabinet_index()
  {
    $origin = Branch::where('id',Auth::user()->branch_id)->get();

    $products = \DB::table('branch_details as dw')
      ->join('branch as w', 'dw.branch_id', '=','w.id')
      ->join('products as p', 'dw.product_id', '=','p.id')
      ->select('p.id', 'p.name')
      ->where('w.id', $origin[0]->id)
      ->get();

    return view('file_cabinet.index', compact( 'origin', 'products'));
  }


  function get_kardexs(Request $request){
    if ($request->excel == 1){
      return Excel::download(new KardexExport($request), 'file_cabinet.xlsx');
    }

    if ($request->pdf == 1){
      $pdf = App::make('dompdf.wrapper');

      $pdf->loadHTML((new KardexExport($request))->view()->render());
      $pdf->setPaper('a4','landscape');

      return $pdf->download('file_cabinet.pdf', array('Attachment' => 0));

    }
      return response()->json((new KardexExport($request))->view()->render());
  }

}

