<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Models\BankAccounts;
use App\Models\Branch;
use App\Models\Category;
use App\Models\BranchDetail;
use App\Models\Batch;
use App\Models\BatchDetail;
use App\Models\FileCabinet;
use App\Models\Mesures;
use App\Models\OriginProduct;
use App\Models\Product;
use App\Models\Provieder;
use App\Models\Role;
use App\Models\TypeDocument;
use App\Models\TypeFabrication;
use App\Models\TypeProduct;
use App\Models\Unit;
use App\TypeAccountBank;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait BatchTrait {

  private function rules_batch(){
    return  [
    ];
  }

  public function batch_index()
  {

    $products = Product::select('*', \DB::raw('CONCAT(code," - ", name ) AS name'))
      ->get();

    $providers = Provieder::get();
    $branch = Branch::get();

    $type_account_bank = BankAccounts::select('name_bank as id', 'name_bank')
      ->get();

    $categories = Category::get();
    $units = Unit::get();

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentIncome')]
    ];

    return view('batch.index',
      compact(
        'breadcrumbs',
        'providers',
        'products',
        'type_account_bank',
        'units',
        'branch',
        'categories'));
  }

  public function show_batch(Request $request)
  {
    $batch = \DB::table('batch as b')
      ->join('providers as p', 'b.provider_id', '=', 'p.id')
      ->join('users as u', 'b.user_id', '=', 'u.id')
      ->join('branch as br', 'b.branch_id', '=', 'br.id')
      ->select('b.*', 'p.name as provider', 'p.phone', 'u.name as user', 'br.name as branch')
      ->where('b.id', $request->id)
      ->first();

    $batch_detail = \DB::table('batch_details as bd')
      ->join('products as p', 'bd.product_id', '=','p.id')
      ->join('prices as pr', 'p.id', '=','pr.product_id')
      ->select('bd.id', 'p.name as product','p.id as product_id', 'p.code', 'bd.cost', 'bd.quantity',
      'bd.total', 'pr.price')
      ->where('bd.batch_id', $request->id)
      ->where('pr.is_predetermined', 1)
      ->get();

    $categories = \DB::table('batch_details as bd')
      ->join('products as p', 'bd.product_id', '=','p.id')
      ->join('categories as c', 'p.category_id', '=','c.id')
      ->select('c.id', 'c.name')
      ->groupBy('c.id', 'c.name')
      ->where('bd.batch_id', $request->id)
      ->get();

    return response()->json([
      'data' => 0,
      'view' => view('batch.show', compact('batch', 'batch_detail', 'categories'))->render()
    ]);
  }

  public function store_batch(Request $request)
  {
    $data = $this->unserialize_form($request->data);

    $validate = $this->validator($data, $this->rules_batch());
    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $response = \DB::transaction(function () use ($request, $data) {
      try {
        $data['code'] = Batch::count() + 1;
        $data['date_into'] = Carbon::now();
        $batch = Batch::create($data);

        if (isset($request['attributeListArray'])){
          foreach ($request['attributeListArray'] as $attribute){
            $element= [
              'cost' => $attribute['cost'],
              'quantity' => $attribute['quantity'],
              'stock' => $attribute['quantity'],
              'total' => $attribute['total'],
              'product_id' => $attribute['id'],
              'acquisition_price' => $attribute['cost'],
              'batch_id' => $batch->id,
              'branch_id' => $batch->branch_id,
            ];
            BatchDetail::create($element);

            FileCabinet::create([
              'code' => WebController::generate_code(FileCabinet::count() + 1),
              'description' => 'Ingreso de Lote ',
              'code_operation' => $batch->code,
              'quantity_input' => $attribute['quantity'],
              'quantity_out' => 0,
              'price_unit' => $attribute['cost'],
              'income_out' => $attribute['total'],
              'income_input' => 0,
              'product_id' => $attribute['id'],
              'branch_id' => $batch->branch_id,
              'date' => $batch->date_into
            ]);

            $detail_branch = BranchDetail::where('branch_id', $batch->branch_id)->where('product_id', $attribute['id'])->first();
            ($detail_branch) ? BranchDetail::find($detail_branch->id)->update([
              'stock' => $detail_branch->stock + $attribute['quantity']
            ]) : BranchDetail::create($element);
          }
        }
        \DB::commit();

        return $batch;
      }catch (\Exception $e){
        \DB::rollback();
        return $e;
      }
    });

    return response()->json(['response' => $response], 200);
  }

  public function update_batch(Request $request)
  {
    Product::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function batch_delete(Request $request)
  {
    Batch::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_batches(){

    $income = Batch::orderBy('code', 'asc')->get();
    return response()->json([
      'data' => $income
    ]);
  }

  function get_batch_for_category(Request $request){
    $products = \DB::table('batch_details as bd')
      ->join('products as p', 'bd.product_id', '=','p.id')
      ->join('categories as c', 'p.category_id', '=','c.id')
      ->join('prices as pr', 'p.id', '=','pr.product_id')
      ->select('p.id', 'p.name', 'p.description', 'p.code', 'bd.quantity',
         'pr.price', 'c.name as category_name')
      ->where('c.id', $request->category_id)
      ->where('pr.is_predetermined', 1)
      ->get();

    return response()->json(['data' => $products]);
  }

  function get_batch(Request $request){

    $batch = Batch::find($request->id)->first();
    return response()->json([
      'data' => $batch
    ]);
  }

}

