<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Imports\BranchDetailImport;
use App\Imports\BranchImport;
use App\Imports\CategoryImport;
use App\Imports\BatchDetailImport;
use App\Imports\DetailWarehouseImport;
use App\Imports\BatchImport;
use App\Imports\PricesImport;
use App\Imports\ProductImport;
use App\Imports\ProviderImport;
use App\Imports\UnitImport;
use App\Imports\UserImport;
use App\Models\Batch;
use App\Models\BranchDetail;
use App\Models\Category;
use App\Models\FileCabinet;
use App\Models\Price;
use App\Models\Product;
use App\Models\Provieder;
use App\Models\Role;
use App\Models\Unit;
use Carbon\Carbon;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

trait ProductTrait {

  private function rules_product(){
    return  [
      "name" => 'required|min:4|max:40',
    ];
  }

  public function product_index()
  {
    $categories = Category::get();
    $units = Unit::get();

    $products = Product::get();
    $count = 1;
    foreach ($products as $item){

      $find = Product::where('code', $item->code)
                ->where('id','<>', $item->id)
                ->first();

      if ($find) {
        $find->update([
          'code' => $item->code.$count
        ]);
      }
      $count++;
    }

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentProduct')]
    ];

    return view('product.index', compact('breadcrumbs', 'categories', 'units', 'prices'));
  }

  public function list_products()
  {
    $products = Product::get();
    return view('product.list_products', compact( 'products'));
  }

  public function import_excel()
  {
      return view('system.import_excel');
  }

  function factory_import($module){
    switch ($module) {
      case 'users':
        return new UserImport();
        break;
      case 'products':
        return new ProductImport();
        break;
      case 'branch_detail':
        return new BranchDetailImport();
        break;
      case 'category':
        return new CategoryImport();
        break;
      case 'batch_detail':
        $batch = Batch::create([
          'code' => Batch::count() + 1,
          'date_into' => Carbon::now(),
          'provider_id' => Provieder::first()->id,
          'branch_id' => Auth::user()->branch_id,
        ]);
        return new BatchDetailImport($batch->id);
    }
  }

  public function upload_file_excel(Request $request)
  {
    $data = \DB::transaction(function () use ($request) {
      try {
        $import = $this->factory_import($request->module);
        Excel::import($import, $request->file('file'));
        \DB::commit();
        return 1;
      } catch (\Exception $e) {
        \DB::rollback();
        return $e->getMessage();
      }
    });
    $status = ($data === 1) ? 200 : 500;
    return response()->json($data, $status);
  }

  public function import_product_third()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end3/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end3/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end3/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end3/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end3/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end3/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_seven()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end7/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end7/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end7/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end7/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end7/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end7/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_eight()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end8/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end8/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end8/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end8/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end8/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end8/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_nine()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end9/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end9/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end9/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end9/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end9/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end9/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_ten()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end10/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end10/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end10/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end10/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end10/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end10/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }


  public function import_product_eleven()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end11/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end11/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end11/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end11/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end11/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end11/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_twelve()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end12/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end12/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end12/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end12/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end12/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end12/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_trece()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end13/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end13/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end13/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end13/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end13/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end13/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_fourteen()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end14/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end14/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end14/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end14/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end14/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end14/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_fifteen()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end15/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end15/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end15/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end15/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end15/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end15/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_fourth()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end4/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end4/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end4/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end4/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end4/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end4/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_five()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end5/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end5/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end5/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end5/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end5/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end5/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_six()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/end6/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/end6/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/end6/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/end6/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/end6/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/end6/products.xlsx');
        \DB::commit();

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_second()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new CategoryImport(), 'migrations/stage_first/category.xlsx');
        Excel::import(new ProductImport(), 'migrations/stage_first/products.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/stage_first/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/stage_first/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/stage_first/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/stage_first/products.xlsx');

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }

  public function import_product_first()
  {
    $data = \DB::transaction(function () {
      try {

        Excel::import(new UserImport(), 'migrations/migration_end/users.xlsx');
        Excel::import(new UnitImport(), 'migrations/migration_end/unit.xlsx');
        Excel::import(new CategoryImport(), 'migrations/migration_end/category.xlsx');
        Excel::import(new ProviderImport(), 'migrations/migration_end/providers.xlsx');
        Excel::import(new ProductImport(), 'migrations/migration_end/products.xlsx');
        Excel::import(new BranchImport(), 'migrations/migration_end/branch.xlsx');
        Excel::import(new BranchDetailImport(), 'migrations/migration_end/branch_detail.xlsx');
        Excel::import(new BatchImport(), 'migrations/migration_end/batch.xlsx');
        Excel::import(new BatchDetailImport(), 'migrations/migration_end/detail_batch.xlsx');
        Excel::import(new PricesImport(), 'migrations/migration_end/products.xlsx');

      }catch (\Exception $e){
        \DB::rollback();
        echo $e->getMessage();
        Log::info($e);
      }
    });
  }




  public function product_register_view(Request $request)
  {
    $roles = Role::get();
    return response()->json([
      'data' => 0,
      'view' => view('product.register', compact('roles'))->render()
    ]);
  }

  public function update_price(Request $request)
  {
    $price = Price::find($request->id)->update([
      'price' => (float) $request->value
    ]);

    return response()->json('great!', 200);
  }

  public function delete_price(Request $request)
  {
    $price = Price::find($request->id)->delete();
    return response()->json('great!', 200);
  }

  public function price_store(Request $request)
  {
    $request['user_id'] = Auth::user()->id;
    $price = Price::create($request->all());
    return response()->json('great!', 200);
  }

  public function show_product(Request $request)
  {
    $product = Product::where('id', $request->id)
      ->first();

    $prices = Price::where('product_id', $request->id)->get();

    return response()->json([
      'data' => 0,
      'view' => view('product.show', compact('prices', 'product', 'photos'))->render()
    ]);
  }

  public function update_price_product(Request $request)
  {

    $price = Price::findOrFail($request->id);

    if ($request->checked){
      Price::where('deleted_at', null)
        ->where('product_id', $price->product_id)
        ->update(['is_predetermined' => false]);

      Price::findOrFail($request->id)
        ->update(['is_predetermined' => true]);
    }
    else{
      Price::where('deleted_at', null)->update(['is_predetermined' => false]);
    }

    return response()->json('great!', 200);
  }


  public function store_product(Request $request)
  {

    $request['attributeListArray'] = json_decode($request->attributeListArray);

    $validate = $this->validator($request->all(), $this->rules_product());
    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $data = \DB::transaction(function () use ($request) {
      try {

        $product = Product::create($request->all());

        $element= [
          'description' => __('common.prices.price_offer'),
          'price' => $request['price_offer'],
          'product_id' => $product->id,
          'is_predetermined' => 0
        ];

        Price::create($element);

        $is_predetermined = 1;
        $element['description'] = __('common.prices.price_unit');
        $element['price'] = $request['price_unit'];

        if (isset($request['attributeListArray'])){
          foreach ($request['attributeListArray'] as $attribute){
            $item= [
              'description' => $attribute->description,
              'price' => $attribute->price,
              'product_id' => $product->id,
              'is_predetermined' => $attribute->is_predetermined
            ];

            if ($attribute->is_predetermined){
              $is_predetermined = 0;
            }

            Price::create($item);
          }
        }

        $element['is_predetermined'] = $is_predetermined;
        Price::create($element);

        if (isset($request->file)){
          $file = $request->file('file');
          $name_image =  $file->getClientOriginalName();
          $path = Storage::disk('public')->put('images_product/'.$product->id.'/'.$name_image, \File::get($file) );
          $request['photo'] = 'images_product/'.$product->id.'/'.$name_image;
          $product->fill([
            'photo' => $request['photo']
          ])->update();
        }

        FileCabinet::create([
          'code' => WebController::generate_code(FileCabinet::count() + 1),
          'description' => 'Producto Registrado',
          'code_operation' => $product->code,
          'quantity_input' => 0,
          'quantity_out' => 0,
          'price_unit' => 0,
          'income_out' => 0,
          'income_input' => 0,
          'product_id' => $product->id,
          'branch_id' => Auth::user()->branch_id,
          'date' => Carbon::now()
        ]);

        \DB::commit();

        return $product;
      }catch (\Exception $e){
        \DB::rollback();
        return $e;
      }
    });

    return response()->json(['data' => $data], 200);
  }

  public function update_product(Request $request)
  {
    $product = Product::find($request->id);

    FileCabinet::create([
      'code' => WebController::generate_code(FileCabinet::count() + 1),
      'description' => 'Producto Actualizado',
      'code_operation' => $product->code,
      'quantity_input' => 0,
      'quantity_out' => 0,
      'price_unit' => 0,
      'income_out' => 0,
      'income_input' => 0,
      'product_id' => $product->id,
      'warehouse_id' => Auth::user()->warehouse_id,
      'date' => Carbon::now()

    ]);
    $product->update($request->all());

    return response()->json(['message' => 'Great!'], 200);
  }

  public function product_delete(Request $request)
  {
    $product = Product::find($request->id);
    FileCabinet::create([
      'code' => WebController::generate_code(FileCabinet::count() + 1),
      'description' => 'Producto Eliminado',
      'code_operation' => $product->code,
      'quantity_input' => 0,
      'quantity_out' => 0,
      'price_unit' => 0,
      'income_out' => 0,
      'income_input' => 0,
      'product_id' => $product->id,
      'warehouse_id' => Auth::user()->warehouse_id,
      'date' => Carbon::now()

    ]);

    $product->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  public function get_product_with_prices(Request $request)
  {
    $product = \DB::table('products as p')
      ->join('branch_details as bd', 'p.id', '=', 'bd.product_id')
      ->join('prices as pr', 'p.id', '=', 'pr.product_id')
      ->select('p.*','pr.id as price_id', 'pr.price', 'pr.description as type_price', 'bd.stock')
      ->where('p.id', $request->id)
      ->first();

    $prices = Price::select('id', 'description as name', 'product_id', 'price')
      ->where('product_id', $request->id)
      ->get();

    return response()->json([
      'product' => $product,
      'prices' => $prices
    ], 200);
  }



  function get_products(Request $request){

    $product = (isset($request->deteled) ?Product::select('id',
      \DB::raw('CONCAT(code," - ", name ) AS name'),
      'name as text',
      'description',
      'quantity_minim'
    )->onlyTrashed()
      ->get() : Product::select('id',
      \DB::raw('CONCAT(code," - ", name ) AS name'),
      'name as text',
      'description',
      'quantity_minim'
    )->get());

    return response()->json([
      'data' => $product
    ]);
  }

  function get_products_by_filters(Request $request){
    $products = \DB::table('branch_details as bd')
      ->join('branch as b', 'bd.branch_id', '=', 'b.id')
      ->join('products as p', 'bd.product_id', '=', 'p.id')
      ->join('categories as c', 'p.category_id', '=', 'c.id')
      ->select(\DB::raw('CONCAT(p.code," - ", p.name ) AS name'), 'p.id', \DB::raw('CONCAT(p.code, " - ",  p.name ) AS text'))
      ->where(function ($query) use ($request) {
        if ($request->type_sale !== '2'){
          $query->where('bd.stock', '>', 0);
        }
        if (isset($request->category)){
          $query->where('c.id', $request->category);
        }
      })
      ->where('b.id', Auth::user()->branch_id)
      ->get();


    if($request->ajax()){
      return response()->json(['data' => $products]);

    }else{
      return $products;
    }

  }

  function get_product(Request $request){
    if (isset($request->code)){
      $product = Product::join('prices as pr', 'products.id', '=', 'pr.product_id')
        ->select('products.*', 'pr.price')
        ->where('code', $request->code)
        ->where('pr.is_predetermined', 1)
        ->first();
    }
    else{
      $product = Product::join('prices as pr', 'products.id', '=', 'pr.product_id')
        ->select('products.*', 'pr.price')
        ->where('pr.is_predetermined', 1)
        ->where('products.id', $request->id)
        ->first();
    }
    return response()->json([
      'data' => $product
    ]);
  }

  function print_tags(Request $request){
    return view('product.print_tags');

  }

}

