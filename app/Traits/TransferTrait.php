<?php


namespace App\Traits;

use App\Http\Controllers\WebController;
use App\Models\BranchDetail;
use App\Models\Batch;
use App\Models\FileCabinet;
use App\Models\Product;
use App\Models\TransferDetail;
use App\Models\Branch;
use App\Transfer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait TransferTrait {

  private function get_status() {
    return [
      0 => 'Solicitud',
      1 => 'Pendiente',
      2 => 'En traspaso',
      3 => 'Finalizado',
      4 => 'Vencido',
      5 => 'Cancelado',
    ];
  }

  private function rules_transfer(){
    return  [
    ];
  }

  public function transfer_index(Request $request)
  {

    $request['type_sale'] = 2;
    $branch = Branch::first();

    $products = \DB::table('branch_details as bd')
      ->join('branch as b', 'bd.branch_id', '=','b.id')
      ->join('products as p', 'bd.product_id', '=','p.id')
      ->select('p.id', \DB::raw('CONCAT(p.code," - ", p.name ) AS name'))
      ->where('b.id', $branch->id)
      ->where('bd.stock', '>', 0)
      ->get();

    $branch = Branch::get();

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.ManagamentTransfer')]
    ];

    return view('transfer.index',
      compact('breadcrumbs',
        'products',
        'branch'));
  }

  public function get_transfers_for_status(Request $request){

    if ($request->status == 0){
      $transfers = Transfer::where('branch_destine_id', Auth::user()->branch_id)
        ->where('status', $request->status)
        ->where('status','<>' ,4)
        ->where('status','<>' ,5)
        ->get();
    }
//
//    if ($request->status == 1){
//      $transfers = Transfer::where('origin_id', Auth::user()->warehouse_id)
//        ->where('status', $request->status)
//        ->where('status','<>' ,4)
//        ->where('status','<>' ,5)
//        ->get();
//    }
//
//    if ($request->status == 2 || $request->status == 3){
//      $transfers = Transfer::where('destine_id', Auth::user()->warehouse_id)
//        ->where('status', $request->status)
//        ->where('status','<>' ,4)
//        ->where('status','<>' ,5)
//        ->get();
//    }


    return response()->json(['data' => $transfers], 200);

  }

  public function transfer_pending()
  {
    $pageConfigs = ['pageHeader' => true];


    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.Pending')]
    ];

    return view('transfer.pending',
      compact('pageConfigs',
        'breadcrumbs'));
  }

  public function transfer_transfer()
  {
    $pageConfigs = ['pageHeader' => true];


    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.Transfer')]
    ];

    return view('transfer.transfer',
      compact('pageConfigs',
        'breadcrumbs'));
  }

  public function transfer_solicitude()
  {
    $pageConfigs = ['pageHeader' => true];

    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.Solicitude')]
    ];

    $origin = Branch::where('id',Auth::user()->warehouse_id)->get();

    return view('transfer.solicitude',
      compact('pageConfigs',
        'breadcrumbs', 'origin'));
  }

  public function transfer_received()
  {
    $pageConfigs = ['pageHeader' => true];


    $breadcrumbs = [
      ["link" => "/", "name" => "Home"],["name" => __('locale.received')]
    ];

    return view('transfer.received',
      compact('pageConfigs',
        'breadcrumbs'));
  }

  public function change_status_transfer(Request $request)
  {
    $transfer = Transfer::find($request->id)
      ->update($request->all());

    $transfer = Transfer::find($request->id);

    if ($transfer->status === 1){
      $detail = TransferDetail::where('transfer_id', $transfer->id)->get();
      foreach ($detail as $item){

        $element= [
          'product_id' => $item->product_id,
          'stock' => $item->quantity,
          'branch_id' => $transfer->branch_destine_id,
          'user_id' => Auth::user()->id,
        ];

        FileCabinet::create([
          'code' => WebController::generate_code(FileCabinet::count() + 1),
          'description' => 'Salida por Transferencia de produto',
          'code_operation' => $transfer->code_transfer,
          'quantity_input' => 0,
          'quantity_out' => $item->quantity,
          'price_unit' => 0,
          'income_out' => 0,
          'income_input' => 0,
          'product_id' => $item->product_id,
          'branch_id' => $item->branch_origin_id,
          'date' => Carbon::parse($transfer->date_transfer)->format('Y-m-d')
        ]);

        BranchDetail::where('branch_id',$transfer->brnach_origin_id)
          ->where('product_id', $item->product_id)
          ->decrement('stock', $item->quantity);

        FileCabinet::create([
          'code' => WebController::generate_code(FileCabinet::count() + 1),
          'description' => 'Entrada por Transferencia de produto',
          'code_operation' => $transfer->code_transfer,
          'quantity_input' => $item->quantity,
          'quantity_out' => 0,
          'price_unit' => 0,
          'income_out' => 0,
          'income_input' => 0,
          'product_id' => $item->product_id,
          'branch_id' => $item->branch_destine_id,
          'date' => Carbon::parse($transfer->date_transfer)->format('Y-m-d')

        ]);

        $detail_branch = BranchDetail::where('branch_id', $transfer->branch_destine_id)->where('product_id', $item->product_id)->first();
        ($detail_branch) ? BranchDetail::where('branch_id', $transfer->branch_destine_id)->where('product_id', $item->product_id)->update([
          'stock' => $detail_branch->stock + $item->quantity
        ]) : BranchDetail::create($element);
      }

    }

    return response()->json(['data' => $transfer], 200);
  }

  public function print_transfer(Request $request)
  {
    $pdf = App::make('dompdf.wrapper');

    $transfer = \DB::table('transfers as t')
      ->join('users as u', 't.user_id', '=', 'u.id')
      ->join('warehouses as wo', 't.origin_id', '=', 'wo.id')
      ->join('warehouses as wd', 't.destine_id', '=', 'wd.id')
      ->select('t.*', 'wo.name as origin_name',
        'wd.name as destine_name', 'u.name as user')
      ->where('t.id', $request->id)
      ->where('status','<>' ,4)
      ->where('status','<>' ,5)
      ->first();

    $details_transfer = \DB::table('transfer_details as td')
      ->join('products as p', 'td.product_id', '=','p.id')
      ->select('p.name as product', 'p.code', 'td.quantity')
      ->where('td.transfer_id', $request->id)
      ->get();

    $status = $this->get_status();
    $name_status = $status[$transfer->status];

    $pdf->loadHTML(view('pdf.transfer_detail', compact('name_status','transfer','details_transfer')));
    $pdf->setPaper('a4','landscape');
    return $pdf->stream('sale_detail.pdf', array('Attachment'=> 1));
  }

  public function show_transfer(Request $request)
  {

    $transfer = \DB::table('transfers as t')
      ->join('users as u', 't.user_id', '=', 'u.id')
      ->join('branch as b', 't.branch_origin_id', '=', 'b.id')
      ->join('branch as bd', 't.branch_destine_id', '=', 'bd.id')
      ->select('t.*', 'b.name as origin_name',
        'bd.name as destine_name', 'u.name as user')
      ->where('t.id', $request->id)
      ->where('status','<>' ,4)
      ->where('status','<>' ,5)
      ->first();

    $details_transfer = \DB::table('transfer_details as td')
      ->join('products as p', 'td.product_id', '=','p.id')
      ->select('p.name as product', 'p.code', 'td.quantity')
      ->where('td.transfer_id', $request->id)
      ->get();

    return response()->json([
      'data' => 0,
      'view' => view('transfer.show', compact('transfer', 'details_transfer'))->render()
    ]);
  }

  public function store_transfer(Request $request)
  {
    $request['attributeListArray'] = json_decode($request->attributeListArray);
    $data = $request->all();

    $validate = $this->validator($data, $this->rules_transfer());
    if ($validate->fails()){
      return response()->json($validate->messages(), 400);
    }

    $data = \DB::transaction(function () use ($request, $data) {
      try {
        $data['code'] = WebController::generate_code((Transfer::count() + 1));
        $data['date_transfer'] = Carbon::parse($data['date_transfer']);

        $transfer = Transfer::create($data);
        if (isset($request['attributeListArray'])){
          foreach ($request['attributeListArray'] as $attribute){
            $element= [
              'quantity' => $attribute->quantity,
              'product_id' => $attribute->product_id,
              'stock' => $attribute->quantity,
              'transfer_id' => $transfer->id,
              'user_id' => Auth::user()->id,
              'status' => $data['status']
            ];
            TransferDetail::create($element);

          }
        }
        \DB::commit();
        return $transfer;
      }catch (\Exception $e){
        \DB::rollback();
        return $e;
      }
    });



    return response()->json(['data' => $data], 200);
  }

  public function update_transfer(Request $request)
  {
    Product::find($request->id)->update($request->all());
    return response()->json(['message' => 'Great!'], 200);
  }

  public function transfer_delete(Request $request)
  {
    Transfer::find($request->id)->delete();
    return response()->json(['message' => 'Great!'], 200);
  }

  function get_transfers(){

    $transfer = Transfer::where('status','<>' ,4)
      ->where('status','<>' ,5)->get();
    return response()->json([
      'data' => $transfer
    ]);
  }

  function get_transfer(Request $request){

    $transfer = Batch::find($request->id)->first();
    return response()->json([
      'data' => $transfer
    ]);
  }

}

