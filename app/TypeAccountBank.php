<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAccountBank extends Model
{
  protected $fillable = [
    'name'
  ];
}
