<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Datatable extends Component
{
    public $data;
    public $thead;
    public function __construct($data = [], $thead = [])
    {
        $this->data = $data;
        $this->thead = $thead;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.datatable');
    }
}
