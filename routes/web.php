<?php
use App\Http\Controllers\LanguageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login Routes
Auth::routes();

Route::get('prueba', function (\Illuminate\Http\Request $request) {


  $category = \App\Models\Category::firstOrCreate([
    'name' => $request->name,
  ], ['name' => $request->name])->id;

  dd($category);
});

Route::get('/', function (){
  return redirect('dashboard_admin');
});

Route::middleware('auth')->group(function() {

  Route::get('excel', function(\Illuminate\Http\Request $request) {
    return (new \App\Exports\KardexExport($request))->download('invoices.xlsx');
  });

  Route::prefix('managament_user')->group(function () {
    Route::get('/', ['as' => 'user_index', 'uses' => 'WebController@user_index']);
    Route::post('store_user', ['as' => 'store_user', 'uses' => 'WebController@store_user']);
    Route::put('update_user', ['as' => 'update_user', 'uses' => 'WebController@update_user']);
    Route::get('user_register_view', ['as' => 'user_register_view', 'uses' => 'WebController@user_register_view']);
    Route::get('user_permission_view', ['as' => 'user_permission_view', 'uses' => 'WebController@user_permission_view']);
    Route::post('get_users', ['as' => 'get_users', 'uses' => 'WebController@get_users']);
    Route::post('get_user', ['as' => 'get_user', 'uses' => 'WebController@get_user']);
    Route::delete('user_delete', ['as' => 'user_delete', 'uses' => 'WebController@user_delete']);
  });

  Route::prefix('managament_provider')->group(function () {
    Route::get('/', ['as' => 'provider_index', 'uses' => 'WebController@provider_index']);
    Route::post('store_provider', ['as' => 'store_provider', 'uses' => 'WebController@store_provider']);
    Route::put('update_provider', ['as' => 'update_provider', 'uses' => 'WebController@update_provider']);
    Route::get('provider_register_view', ['as' => 'provider_register_view', 'uses' => 'WebController@provider_register_view']);
    Route::post('get_providers', ['as' => 'get_providers', 'uses' => 'WebController@get_providers']);
    Route::post('get_provider', ['as' => 'get_provider', 'uses' => 'WebController@get_provider']);
    Route::delete('provider_delete', ['as' => 'provider_delete', 'uses' => 'WebController@provider_delete']);
  });

  Route::prefix('managament_client')->group(function () {
    Route::get('/', ['as' => 'client_index', 'uses' => 'WebController@client_index']);
    Route::post('store_client', ['as' => 'store_client', 'uses' => 'WebController@store_client']);
    Route::put('update_client', ['as' => 'update_client', 'uses' => 'WebController@update_client']);
    Route::get('client_register_view', ['as' => 'client_register_view', 'uses' => 'WebController@client_register_view']);
    Route::post('get_clients', ['as' => 'get_clients', 'uses' => 'WebController@get_clients']);
    Route::post('get_client', ['as' => 'get_client', 'uses' => 'WebController@get_client']);
    Route::delete('client_delete', ['as' => 'client_delete', 'uses' => 'WebController@client_delete']);
  });


  Route::prefix('managament_branch')->group(function () {
    Route::get('/', ['as' => 'branch_index', 'uses' => 'WebController@branch_index']);
    Route::post('get_branch_with_stock', ['as' => 'get_branch_with_stock', 'uses' => 'WebController@get_branch_with_stock']);
    Route::post('store_branch', ['as' => 'store_branch', 'uses' => 'WebController@store_branch']);
    Route::put('update_branch', ['as' => 'update_branch', 'uses' => 'WebController@update_branch']);
    Route::get('branch_register_view', ['as' => 'branch_register_view', 'uses' => 'WebController@branch_register_view']);
    Route::post('get_branches', ['as' => 'get_branches', 'uses' => 'WebController@get_branches']);
    Route::post('get_detail_branch', ['as' => 'get_detail_branch', 'uses' => 'WebController@get_detail_branch']);
    Route::post('get_branchs', ['as' => 'get_branchs', 'uses' => 'WebController@get_branchs']);
    Route::post('change_origin', ['as' => 'change_origin', 'uses' => 'WebController@change_origin']);
    Route::delete('branch_delete', ['as' => 'branch_delete', 'uses' => 'WebController@branch_delete']);
  });

  Route::prefix('managament_category')->group(function () {
    Route::get('/', ['as' => 'category_index', 'uses' => 'WebController@category_index']);
    Route::post('store_category', ['as' => 'store_category', 'uses' => 'WebController@store_category']);
    Route::put('update_category', ['as' => 'update_category', 'uses' => 'WebController@update_category']);
    Route::get('category_register_view', ['as' => 'category_register_view', 'uses' => 'WebController@category_register_view']);
    Route::post('get_categories', ['as' => 'get_categories', 'uses' => 'WebController@get_categories']);
    Route::post('get_category', ['as' => 'get_category', 'uses' => 'WebController@get_category']);
    Route::get('get_category', ['as' => 'get_category', 'uses' => 'WebController@get_category']);
    Route::post('show_category', ['as' => 'show_category', 'uses' => 'WebController@show_category']);
    Route::delete('category_delete', ['as' => 'category_delete', 'uses' => 'WebController@category_delete']);
  });

  Route::prefix('managament_sub_category')->group(function () {
    Route::get('/', ['as' => 'sub_category_index', 'uses' => 'WebController@sub_category_index']);
    Route::post('store_sub_category', ['as' => 'store_sub_category', 'uses' => 'WebController@store_sub_category']);
    Route::put('update_sub_category', ['as' => 'update_sub_category', 'uses' => 'WebController@update_sub_category']);
    Route::post('get_sub_categories', ['as' => 'get_sub_categories', 'uses' => 'WebController@get_sub_categories']);
    Route::post('get_sub_category', ['as' => 'get_sub_category', 'uses' => 'WebController@get_sub_category']);
    Route::get('get_sub_category', ['as' => 'get_sub_category', 'uses' => 'WebController@get_sub_category']);
    Route::post('sub_category_get', ['as' => 'sub_category_get', 'uses' => 'WebController@sub_category_get']);
    Route::delete('sub_category_delete', ['as' => 'sub_category_delete', 'uses' => 'WebController@sub_category_delete']);
  });


  Route::prefix('managament_unit')->group(function () {
    Route::get('/', ['as' => 'unit_index', 'uses' => 'WebController@unit_index']);
    Route::post('store_unit', ['as' => 'store_unit', 'uses' => 'WebController@store_unit']);
    Route::put('update_unit', ['as' => 'update_unit', 'uses' => 'WebController@update_unit']);
    Route::post('get_unit', ['as' => 'get_unit', 'uses' => 'WebController@get_unit']);
    Route::post('get_units', ['as' => 'get_units', 'uses' => 'WebController@get_units']);
    Route::delete('unit_delete', ['as' => 'unit_delete', 'uses' => 'WebController@unit_delete']);

  });

  Route::prefix('managament_product')->group(function () {
    Route::get('/', ['as' => 'product_index', 'uses' => 'WebController@product_index']);
    Route::post('store_product', ['as' => 'store_product', 'uses' => 'WebController@store_product']);
    Route::put('update_product', ['as' => 'update_product', 'uses' => 'WebController@update_product']);
    Route::get('product_register_view', ['as' => 'product_register_view', 'uses' => 'WebController@product_register_view']);
    Route::post('get_products', ['as' => 'get_products', 'uses' => 'WebController@get_products']);
    Route::post('get_product', ['as' => 'get_product', 'uses' => 'WebController@get_product']);
    Route::delete('product_delete', ['as' => 'product_delete', 'uses' => 'WebController@product_delete']);
    Route::get('show_product', ['as' => 'show_product', 'uses' => 'WebController@show_product']);
    Route::post('update_price_product', ['as' => 'update_price_product', 'uses' => 'WebController@update_price_product']);
    Route::post('get_products_by_filters', ['as' => 'get_products_by_filters', 'uses' => 'WebController@get_products_by_filters']);
    Route::post('update_price', ['as' => 'update_price', 'uses' => 'WebController@update_price']);
    Route::post('delete_price', ['as' => 'delete_price', 'uses' => 'WebController@delete_price']);
    Route::post('price_store', ['as' => 'price_store', 'uses' => 'WebController@price_store']);
    Route::post('get_product_with_prices', ['as' => 'get_product_with_prices', 'uses' => 'WebController@get_product_with_prices']);
  });

  Route::prefix('managament_batch')->group(function () {
    Route::get('/', ['as' => 'batch_index', 'uses' => 'WebController@batch_index']);
    Route::post('store_batch', ['as' => 'store_batch', 'uses' => 'WebController@store_batch']);
    Route::put('update_batch', ['as' => 'update_batch', 'uses' => 'WebController@update_batch']);
    Route::get('batch_register_view', ['as' => 'batch_register_view', 'uses' => 'WebController@batch_register_view']);
    Route::post('get_batches', ['as' => 'get_batches', 'uses' => 'WebController@get_batches']);
    Route::get('show_batch', ['as' => 'show_batch', 'uses' => 'WebController@show_batch']);
    Route::post('get_batch', ['as' => 'get_batch', 'uses' => 'WebController@get_batch']);
    Route::post('get_batch_for_category', ['as' => 'get_batch_for_category', 'uses' => 'WebController@get_batch_for_category']);
    Route::delete('batch_delete', ['as' => 'batch_delete', 'uses' => 'WebController@batch_delete']);
  });

  Route::prefix('managament_file_cabinet')->group(function () {
    Route::get('/', ['as' => 'file_cabinet_index', 'uses' => 'WebController@file_cabinet_index']);
    Route::get('get_file_cabinets', ['as' => 'get_file_cabinets', 'uses' => 'WebController@get_file_cabinets']);
  });

  Route::prefix('managament_sale')->group(function () {
    Route::get('/index', ['as' => 'sale_index', 'uses' => 'WebController@sale_index']);
    Route::get('list', ['as' => 'sale_list', 'uses' => 'WebController@sale_list']);
    Route::post('store_sale', ['as' => 'store_sale', 'uses' => 'WebController@store_sale']);
    Route::put('update_sale', ['as' => 'update_sale', 'uses' => 'WebController@update_sale']);
    Route::get('print_sale', ['as' => 'print_sale', 'uses' => 'WebController@print_sale']);
    Route::get('print_all_sale', ['as' => 'print_all_sale', 'uses' => 'WebController@print_all_sale']);
    Route::get('sale_register_view', ['as' => 'sale_register_view', 'uses' => 'WebController@sale_register_view']);
    Route::post('get_sales', ['as' => 'get_sales', 'uses' => 'WebController@get_sales']);
    Route::post('get_sales_inactive', ['as' => 'get_sales_inactive', 'uses' => 'WebController@get_sales_inactive']);
    Route::get('show_sale', ['as' => 'show_sale', 'uses' => 'WebController@show_sale']);
    Route::get('sale_edit', ['as' => 'sale_edit', 'uses' => 'WebController@sale_edit']);
    Route::post('get_sale', ['as' => 'get_sale', 'uses' => 'WebController@get_sale']);
    Route::post('enabled_discount_total', ['as' => 'enabled_discount_total', 'uses' => 'WebController@enabled_discount_total']);
    Route::delete('sale_delete', ['as' => 'sale_delete', 'uses' => 'WebController@sale_delete']);
    Route::delete('sale_restore', ['as' => 'sale_restore', 'uses' => 'WebController@sale_restore']);
  });

  Route::prefix('managament_quotation')->group(function () {
    Route::get('/index', ['as' => 'quotation_index', 'uses' => 'WebController@quotation_index']);
    Route::get('list', ['as' => 'quotation_list', 'uses' => 'WebController@quotation_list']);
    Route::post('store_quotation', ['as' => 'store_quotation', 'uses' => 'WebController@store_quotation']);
    Route::put('update_quotation', ['as' => 'update_quotation', 'uses' => 'WebController@update_quotation']);
    Route::get('print_quotation', ['as' => 'print_quotation', 'uses' => 'WebController@print_quotation']);
    Route::get('print_all_quotation', ['as' => 'print_all_quotation', 'uses' => 'WebController@print_all_quotation']);
    Route::get('quotation_register_view', ['as' => 'quotation_register_view', 'uses' => 'WebController@quotation_register_view']);
    Route::post('get_quotations', ['as' => 'get_quotations', 'uses' => 'WebController@get_quotations']);
    Route::post('get_quotations_inactive', ['as' => 'get_quotations_inactive', 'uses' => 'WebController@get_quotations_inactive']);
    Route::get('show_quotation', ['as' => 'show_quotation', 'uses' => 'WebController@show_quotation']);
    Route::get('quotation_edit', ['as' => 'quotation_edit', 'uses' => 'WebController@quotation_edit']);
    Route::post('get_quotation', ['as' => 'get_quotation', 'uses' => 'WebController@get_quotation']);
    Route::post('enabled_discount_total', ['as' => 'enabled_discount_total', 'uses' => 'WebController@enabled_discount_total']);
    Route::delete('quotation_delete', ['as' => 'quotation_delete', 'uses' => 'WebController@quotation_delete']);
    Route::delete('quotation_restore', ['as' => 'quotation_restore', 'uses' => 'WebController@quotation_restore']);
  });

  Route::prefix('managament_transfer')->group(function () {
    Route::get('/index', ['as' => 'transfer_index', 'uses' => 'WebController@transfer_index']);
    Route::get('pending', ['as' => 'transfer_pending', 'uses' => 'WebController@transfer_pending']);
    Route::get('solicitude', ['as' => 'transfer_solicitude', 'uses' => 'WebController@transfer_solicitude']);
    Route::get('received', ['as' => 'transfer_received', 'uses' => 'WebController@transfer_received']);
    Route::get('transfer', ['as' => 'transfer_transfer', 'uses' => 'WebController@transfer_transfer']);
    Route::post('store_transfer', ['as' => 'store_transfer', 'uses' => 'WebController@store_transfer']);
    Route::put('update_transfer', ['as' => 'update_transfer', 'uses' => 'WebController@update_transfer']);
    Route::get('transfer_register_view', ['as' => 'transfer_register_view', 'uses' => 'WebController@transfer_register_view']);
    Route::post('get_transfers', ['as' => 'get_transfers', 'uses' => 'WebController@get_transfers']);
    Route::post('get_transfers_for_status', ['as' => 'get_transfers_for_status', 'uses' => 'WebController@get_transfers_for_status']);
    Route::post('change_status_transfer', ['as' => 'change_status_transfer', 'uses' => 'WebController@change_status_transfer']);
    Route::post('get_transfer', ['as' => 'get_transfer', 'uses' => 'WebController@get_transfer']);
    Route::delete('transfer_delete', ['as' => 'transfer_delete', 'uses' => 'WebController@transfer_delete']);
    Route::get('show_transfer', ['as' => 'show_transfer', 'uses' => 'WebController@show_transfer']);
    Route::get('print_transfer', ['as' => 'print_transfer', 'uses' => 'WebController@print_transfer']);
  });


  Route::get('list_products', ['as' => 'list_products', 'uses' => 'WebController@list_products']);
  Route::get('print_tags', ['as' => 'print_tags', 'uses' => 'WebController@print_tags']);


  Route::get('/dashboard_admin', 'DashboardController@dashboardEcommerce');
  Route::get('/import_excel', ['as' => 'import_excel', 'uses' => 'WebController@import_excel']);
  Route::post('/upload_file_excel', ['as' => 'upload_file_excel', 'uses' => 'WebController@upload_file_excel']);

  Route::get('/import_product_third', 'WebController@import_product_third');
  Route::get('/import_product_second', 'WebController@import_product_second');
  Route::get('/import_product_first', 'WebController@import_product_first');
  Route::get('/import_product_fourth', 'WebController@import_product_fourth');
  Route::get('/import_product_five', 'WebController@import_product_five');
  Route::get('/import_product_six', 'WebController@import_product_six');
  Route::get('/import_product_seven', 'WebController@import_product_seven');
  Route::get('/import_product_eight', 'WebController@import_product_eight');
  Route::get('/import_product_nine', 'WebController@import_product_nine');
  Route::get('/import_product_ten', 'WebController@import_product_ten');
  Route::get('/import_product_eleven', 'WebController@import_product_eleven');
  Route::get('/import_product_twelve', 'WebController@import_product_twelve');
  Route::get('/import_product_trece', 'WebController@import_product_trece');
  Route::get('/import_product_fourteen', 'WebController@import_product_fourteen');
  Route::get('/import_product_fifteen', 'WebController@import_product_fifteen');

  Route::get('/dashboard-ecommerce', 'DashboardController@dashboardEcommerce');
  Route::get('/dashboard-analytics', 'DashboardController@dashboardAnalytics');

//Application Routes
  Route::get('/app-email', 'ApplicationController@emailApplication');
  Route::get('/app-chat', 'ApplicationController@chatApplication');
  Route::get('/app-todo', 'ApplicationController@todoApplication');
  Route::get('/app-calendar', 'ApplicationController@calendarApplication');
  Route::get('/app-kanban', 'ApplicationController@kanbanApplication');
  Route::get('/app-invoice-view', 'ApplicationController@invoiceApplication');
  Route::get('/app-invoice-list', 'ApplicationController@invoiceListApplication');
  Route::get('/app-invoice-edit', 'ApplicationController@invoiceEditApplication');
  Route::get('/app-invoice-add', 'ApplicationController@invoiceAddApplication');
  Route::get('/app-file-manager', 'ApplicationController@fileManagerApplication');

// Content Page Routes
  Route::get('/content-grid', 'ContentController@gridContent');
  Route::get('/content-typography', 'ContentController@typographyContent');
  Route::get('/content-text-utilities', 'ContentController@textUtilitiesContent');
  Route::get('/content-syntax-highlighter', 'ContentController@contentSyntaxHighlighter');
  Route::get('/content-helper-classes', 'ContentController@contentHelperClasses');
  Route::get('/colors', 'ContentController@colorContent');
// icons
  Route::get('/icons-livicons', 'IconsController@liveIcons');
  Route::get('/icons-boxicons', 'IconsController@boxIcons');
// card
  Route::get('/card-basic', 'CardController@basicCard');
  Route::get('/card-actions', 'CardController@actionCard');
  Route::get('/widgets', 'CardController@widgets');
// component route
  Route::get('/component-alerts', 'ComponentController@alertComponenet');
  Route::get('/component-buttons-basic', 'ComponentController@buttonComponenet');
  Route::get('/component-breadcrumbs', 'ComponentController@breadcrumbsComponenet');
  Route::get('/component-carousel', 'ComponentController@carouselComponenet');
  Route::get('/component-collapse', 'ComponentController@collapseComponenet');
  Route::get('/component-dropdowns', 'ComponentController@dropdownComponenet');
  Route::get('/component-list-group', 'ComponentController@listGroupComponenet');
  Route::get('/component-modals', 'ComponentController@modalComponenet');
  Route::get('/component-pagination', 'ComponentController@paginationComponenet');
  Route::get('/component-navbar', 'ComponentController@navbarComponenet');
  Route::get('/component-tabs-component', 'ComponentController@tabsComponenet');
  Route::get('/component-pills-component', 'ComponentController@pillComponenet');
  Route::get('/component-tooltips', 'ComponentController@tooltipsComponenet');
  Route::get('/component-popovers', 'ComponentController@popoversComponenet');
  Route::get('/component-badges', 'ComponentController@badgesComponenet');
  Route::get('/component-pill-badges', 'ComponentController@pillBadgesComponenet');
  Route::get('/component-progress', 'ComponentController@progressComponenet');
  Route::get('/component-media-objects', 'ComponentController@mediaObjectComponenet');
  Route::get('/component-spinner', 'ComponentController@spinnerComponenet');
  Route::get('/component-bs-toast', 'ComponentController@toastsComponenet');
// extra component
  Route::get('/ex-component-avatar', 'ExComponentController@avatarComponent');
  Route::get('/ex-component-chips', 'ExComponentController@chipsComponent');
  Route::get('/ex-component-divider', 'ExComponentController@dividerComponent');
// form elements
  Route::get('/form-inputs', 'FormController@inputForm');
  Route::get('/form-input-groups', 'FormController@inputGroupForm');
  Route::get('/form-number-input', 'FormController@numberInputForm');
  Route::get('/form-select', 'FormController@selectForm');
  Route::get('/form-radio', 'FormController@radioForm');
  Route::get('/form-checkbox', 'FormController@checkboxForm');
  Route::get('/form-switch', 'FormController@switchForm');
  Route::get('/form-textarea', 'FormController@textareaForm');
  Route::get('/form-quill-editor', 'FormController@quillEditorForm');
  Route::get('/form-file-uploader', 'FormController@fileUploaderForm');
  Route::get('/form-date-time-picker', 'FormController@datePickerForm');
  Route::get('/form-layout', 'FormController@formLayout');
  Route::get('/form-wizard', 'FormController@formWizard');
  Route::get('/form-validation', 'FormController@formValidation');
  Route::get('/form-repeater', 'FormController@formRepeater');
// table route
  Route::get('/table', 'TableController@basicTable');
  Route::get('/extended', 'TableController@extendedTable');
  Route::get('/datatable', 'TableController@dataTable');
// page Route
  Route::get('/page-branch-profile', 'PageController@userProfilePage');
  Route::get('/page-faq', 'PageController@faqPage');
  Route::get('/page-knowledge-base', 'PageController@knowledgeBasePage');
  Route::get('/page-knowledge-base/categories', 'PageController@knowledgeCatPage');
  Route::get('/page-knowledge-base/categories/question', 'PageController@knowledgeQuestionPage');
  Route::get('/page-search', 'PageController@searchPage');
  Route::get('/page-account-settings', 'PageController@accountSettingPage');
// User Route
  Route::get('/page-users-list', 'UsersController@listUser');
  Route::get('/page-users-view', 'UsersController@viewUser');
  Route::get('/page-users-edit', 'UsersController@editUser');
// Authentication  Route
  Route::get('/auth-login', 'AuthenticationController@loginPage');
  Route::get('/auth-register', 'AuthenticationController@registerPage');
  Route::get('/auth-forgot-password', 'AuthenticationController@forgetPasswordPage');
  Route::get('/auth-reset-password', 'AuthenticationController@resetPasswordPage');
  Route::get('/auth-lock-screen', 'AuthenticationController@authLockPage');
// Miscellaneous
  Route::get('/page-coming-soon', 'MiscellaneousController@comingSoonPage');
  Route::get('/error-404', 'MiscellaneousController@error404Page');
  Route::get('/error-500', 'MiscellaneousController@error500Page');
  Route::get('/page-not-authorized', 'MiscellaneousController@notAuthPage');
  Route::get('/page-maintenance', 'MiscellaneousController@maintenancePage');
// Charts Route
  Route::get('/chart-apex', 'ChartController@apexChart');
  Route::get('/chart-chartjs', 'ChartController@chartJs');
  Route::get('/chart-chartist', 'ChartController@chartist');
  Route::get('/maps-google', 'ChartController@googleMap');
// extension route
  Route::get('/ext-component-sweet-alerts', 'ExtensionsController@sweetAlert');
  Route::get('/ext-component-toastr', 'ExtensionsController@toastr');
  Route::get('/ext-component-noui-slider', 'ExtensionsController@noUiSlider');
  Route::get('/ext-component-drag-drop', 'ExtensionsController@dragComponent');
  Route::get('/ext-component-tour', 'ExtensionsController@tourComponent');
  Route::get('/ext-component-swiper', 'ExtensionsController@swiperComponent');
  Route::get('/ext-component-treeview', 'ExtensionsController@treeviewComponent');
  Route::get('/ext-component-block-ui', 'ExtensionsController@blockUIComponent');
  Route::get('/ext-component-media-player', 'ExtensionsController@mediaComponent');
  Route::get('/ext-component-miscellaneous', 'ExtensionsController@miscellaneous');
  Route::get('/ext-component-i18n', 'ExtensionsController@i18n');
// locale Route
  Route::get('lang/{locale}', [LanguageController::class, 'swap']);

// acess controller
  Route::get('/access-control', 'AccessController@index');
  Route::get('/access-control/{roles}', 'AccessController@roles');
  Route::get('/ecommerce', 'AccessController@home')->middleware('role:Admin');
});

