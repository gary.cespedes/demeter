<x-form method="post" id="form_client_register">
  <x-slot name="route">{{route('store_client')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">

      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>

      <x-form-group name="last_name" type="text">
        <x-slot name="text_label">{{__('label.last_name')}}</x-slot>
        <x-slot name="placeholder">{{__('label.last_name_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>

      <x-form-group name="firm_name" type="text">
        <x-slot name="text_label">{{__('label.firm_name')}}</x-slot>
        <x-slot name="placeholder">{{__('label.firm_name_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>
      <x-form-group name="tax_name" type="text">
        <x-slot name="text_label">{{__('label.tax_name')}}</x-slot>
        <x-slot name="placeholder">{{__('label.tax_name_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>
      <x-form-group name="tax_number" type="number">
        <x-slot name="text_label">{{__('label.tax_number')}}</x-slot>
        <x-slot name="placeholder">{{__('label.tax_number_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="city" type="text">
        <x-slot name="text_label">{{__('label.city')}}</x-slot>
        <x-slot name="placeholder">{{__('label.city_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="email" type="email">
        <x-slot name="text_label">{{__('label.email')}}</x-slot>
        <x-slot name="placeholder">{{__('label.email_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="phone" type="number">
        <x-slot name="text_label">{{__('label.phone1')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone1_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="phone_alternative" type="number">
        <x-slot name="text_label">{{__('label.phone2')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone2_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="address" type="text">
        <x-slot name="text_label">{{__('label.address')}}</x-slot>
        <x-slot name="placeholder">{{__('label.address_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>
      <x-row>
        <x-col sm="12" xs="12" md="6" lg="6">
          <div class="form-group">
            <label>{{__('label.issued_in')}}<span class="text-danger">*</span></label>
            <div class="controls">
              <select class="select form-control default-select" name="issued_in" id="issued_in">
                @foreach(\App\Http\Controllers\WebController::$issued_in as $key => $item)
                  <option value="{{$key}}">{{$item}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </x-col>
        <x-col sm="12" xs="12" md="6" lg="6">
          <x-form-group name="number_document" type="text">
            <x-slot name="text_label">{{__('label.number_document')}}</x-slot>
            <x-slot name="placeholder">{{__('label.number_document_placeholder')}}</x-slot>
            <x-slot name="rules">
              data-validation-required-message="Este Campo es requerido" required
            </x-slot>
          </x-form-group>
        </x-col>
      </x-row>
    </x-col>

    <hr>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>


  </x-row>
</x-form>
