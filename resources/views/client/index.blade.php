@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title',__('common.title_pages.client'))

@section('page-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/validation/form-validation.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
@endsection
@section('content')
  <section class="input-validation" id="register_client">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_client')}}</x-slot>
          @include('client.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_client">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_client')}}</x-slot>
          @include('client.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_client')}}</x-slot>
        @include('client.list')
      </x-card>
    </x-col>
  </x-row>



@endsection

@section('page-scripts')
  <script src="{{asset('js/pages/client/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/client/main.js')}}" type="module"></script>
  <script>
    $('#thead_clients').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.last_name')}}</th>
        <th>{{__('common.table.number_document')}}</th>
        <th>{{__('common.table.email')}}</th>
    </tr>`)
  </script>

@endsection
