<x-form method="put" id="form_client_edit" file="true">
  <x-slot name="route">{{route('update_client')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="name" type="text">
        <x-slot name="text_label">Nombre</x-slot>
        <x-slot name="placeholder">Ingrese el nombre</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>

      <x-form-group name="last_name" type="text">
        <x-slot name="text_label">Apellido(s)</x-slot>
        <x-slot name="placeholder">Ingrese el apellido</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>

      <x-form-group name="firm_name" type="text">
        <x-slot name="text_label">Nombre de la firma</x-slot>
        <x-slot name="placeholder">Ingrese el nombre de la firma</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>
      <x-form-group name="tax_name" type="text">
        <x-slot name="text_label">Nombre del impuesto</x-slot>
        <x-slot name="placeholder">Ingrese el nombre de impuesto</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>
      <x-form-group name="tax_number" type="number">
        <x-slot name="text_label">Número de impuesto</x-slot>
        <x-slot name="placeholder">Ingrese el numero de impuesto</x-slot>
        <x-slot name="rules">
          data-validation-required-message="Este Campo es Obligatorio" required
        </x-slot>
      </x-form-group>

    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="city" type="text">
        <x-slot name="text_label">{{__('label.city')}}</x-slot>
        <x-slot name="placeholder">{{__('label.city_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="email" type="email">
        <x-slot name="text_label">{{__('label.email')}}</x-slot>
        <x-slot name="placeholder">{{__('label.email_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="phone" type="number">
        <x-slot name="text_label">{{__('label.phone1')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone1_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="phone_alternative" type="number">
        <x-slot name="text_label">{{__('label.phone2')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone2_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>

      <x-form-group name="address" type="text">
        <x-slot name="text_label">{{__('label.address')}}</x-slot>
        <x-slot name="placeholder">{{__('label.address_placeholder')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>
      <x-row>
        <x-col sm="12" xs="12" md="6" lg="6">
          <div class="form-group">
            <label>Tipo de Documento<span class="text-danger">*</span></label>
            <div class="controls">
              <select class="select form-control default-select" name="document" id="document">
                <option value="cedula_identidad">Cedula de Identidad</option>
                <option value="nit">NIT</option>
                <option value="rut">RUT</option>
              </select>
            </div>
          </div>
        </x-col>
        <x-col sm="12" xs="12" md="6" lg="6">
          <x-form-group name="number_document" type="number">
            <x-slot name="text_label">{{__('label.number_document')}}</x-slot>
            <x-slot name="placeholder">{{__('label.number_document_placeholder')}}</x-slot>
            <x-slot name="rules">
              data-validation-required-message="Este Campo es requerido" required
            </x-slot>
          </x-form-group>
        </x-col>
      </x-row>
    </x-col>


    <hr>
    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="warning" id="update">
        {{__('common.button.update')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel2">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
