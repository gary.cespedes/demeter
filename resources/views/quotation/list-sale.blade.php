@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.sale'))
@section('content')

  <section class="input-validation" id="register_kardex">
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card>
          <x-slot name="title">Consulta de Venta</x-slot>
          <x-form method="post" id="sale_generate">
            <x-slot name="route">{{route('get_sales')}}</x-slot>
            <div class="col-11 d-flex justify-content-end" style="left: -0.5%">
              <x-form-check  value="1" name="all_sale" checked="1" id="all_sales">
                <x-slot name="title">Todas las Ventas</x-slot>
              </x-form-check>
            </div>

            <x-row id="sale_detail_date" style="display: none">
              <x-col sm="12" xs="12" md="4" lg="4">
                <x-form-group name="date_start" type="date">
                  <x-slot name="text_label">{{__('label.date_start')}}</x-slot>
                  <x-slot name="placeholder">{{__('label.date_start')}}</x-slot>
                  <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d')}}</x-slot>
                  <x-slot name="rules">
                    data-validation-required-message="This field is required" required
                  </x-slot>
                </x-form-group>
              </x-col>
              <x-col sm="12" xs="12" md="4" lg="4">
                <x-form-group name="date_end" type="date">
                  <x-slot name="text_label">{{__('label.date_end')}}</x-slot>
                  <x-slot name="placeholder">{{__('label.date_end')}}</x-slot>
                  <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d')}}</x-slot>
                  <x-slot name="rules">
                    data-validation-required-message="This field is required" required
                  </x-slot>
                </x-form-group>
              </x-col>
            </x-row>
            <div class="col-12 d-flex justify-content-end">
              <x-button type="submit" color="primary" id="create">
                Generar
              </x-button>
            </div>
          </x-form>
        </x-card>
      </x-col>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card id="list_sale_actions">
        <x-slot name="title">{{__('common.table_title.list_sale')}}</x-slot>
        <x-slot name="actions">true</x-slot>
        @include('sale.list')
      </x-card>
    </x-col>
  </x-row>

  <x-modal title="Detalle de Venta" id="info_sale">
    <div id="container_info_sale"></div>
  </x-modal>
@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset('js/pages/sale/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/sale/main.js')}}" type="module"></script>
  <script>
    $('#thead_sales').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.type_sale')}}</th>
        <th>{{__('common.table.date_sale')}}</th>
        <th>{{__('common.table.price_initial')}}</th>
        <th>{{__('common.table.price_end')}}</th>
    </tr>`)
  </script>

@endsection
