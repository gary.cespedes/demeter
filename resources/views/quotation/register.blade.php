<x-form method="post" id="form_sale" file="true">
  <x-slot name="route">{{route('store_sale')}}</x-slot>
  <div class="text-right">
    <div>
      <label><b>Vendedor: </b></label>
      <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
    </div>
    <div>
      <label><b>Tienda:</b></label>
      <span>{{$warehouse->name}}</span>
    </div>
  </div>

  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-row class="mt-2">
        <x-col sm="12" xs="12" md="6" lg="6">

          <x-select2 >
            @slot('options', $clients)
            <x-slot name="title">
              Cliente <a class="right text-primary" data-toggle="modal" data-target="#client_register"><i class="bx bx-plus-medical"></i>Agregar nuevo Cliente</a>
            </x-slot>
            <x-slot name="id">client_id</x-slot>
            <x-slot name="name">client_id</x-slot>
          </x-select2>

        </x-col>

        <x-col sm="12" xs="12" md="6" lg="6" >
          <x-form-group name="date_transfer" type="datetime-local">
            <x-slot name="text_label">{{__('label.date')}}</x-slot>
            <x-slot name="placeholder">{{__('label.date')}}</x-slot>
            <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d\TH:i')}}</x-slot>
            <x-slot name="rules">
              data-validation-required-message="This field is required" required
            </x-slot>
          </x-form-group>
        </x-col>
      </x-row>

      <x-row>
        <x-col sm="12" xs="12" md="6" lg="6">
          <label>Escoja el tipo de pago</label>
          <div class="form-group">
            <select class="select2 form-control default-select" name="payment_type" id="payment_type">
              @foreach(\App\Http\Controllers\WebController::$payment_type as $index => $item)
                <option value="{{$index}}">{{$item}}</option>
              @endforeach
            </select>
          </div>

        </x-col>
        <x-col sm="12" xs="12" md="6" lg="6">
          <x-form-group name="detail_sale" type="text">
            <x-slot name="text_label">{{__('label.detail_sale')}}</x-slot>
            <x-slot name="placeholder">{{__('label.detail_sale')}}</x-slot>
            <x-slot name="rules">
            </x-slot>
          </x-form-group>

        </x-col>
      </x-row>

  
    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-row class="mt-2">
        <x-col sm="12" xs="12" md="12" lg="12">
          <div id="list_warehouses" style="display: none; overflow-x: auto; height: 250px">
            <h6>Stock en Almacenes</h6>
            <div class="col-12">
              <div id="container_product_price"></div>
            </div>
          </div>
        </x-col>
      </x-row>
    </x-col>
  </x-row>

  <x-row>
    <x-col sm="12" xs="12" md="10" lg="10">
      <x-select2 >
        @slot('options', $products)
        <x-slot name="title">
          Escoja el Producto
        </x-slot>
        <x-slot name="id">product_id</x-slot>
        <x-slot name="name">product_id</x-slot>
      </x-select2>
    </x-col>

    <x-col  sm="12" xs="12" md="2" lg="2">
      <div style="margin-top: 20px">
        <button id="addAttributeInList" class="btn btn-icon btn-info glow mr-1">
          <i class="bx bx-send"></i>
        </button>
      </div>
    </x-col>
  </x-row>

  @include('sale.list_prices')

  <x-row>
    <div class="container">
      <div class="text-right">
        <table class="table">
          <tr>
            <td width="90%"><b>Monto Inicial: </b></td>
            <td> <b><span id="price_init"></span></b> </td>
          </tr>
          <tr>
            <td width="90%"> <b>Aplicar Descuento: </b> </td>
            <td>
              <button class="btn btn-icon btn-primary" id="applicate_discount">Aplicar</button>
              <input style="display: none" type="text" id="descount_to_total" class="form-control" size="4" maxlength="4" value="0">
            </td>
          </tr>
          <tr>
            <td width="90%"> <b>Monto Total: </b> </td>
            <td><b><span id="price_end"></span></b></td>
          </tr>

          <tr>
            <td width="90%"> <b>Efectivo: </b> </td>
            <td><input type="text" id="efectivo_payment" class="form-control" size="4" maxlength="4" value="0"></td>
          </tr>

          <tr>
            <td width="90%"> <b>Vuelto: </b> </td>
            <td><b><span id="vuelto_payment"></span></b></td>
          </tr>

        </table>

      </div>
    </div>
  </x-row>
  <x-row>
    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>

  <x-row>
    <div class="col-11 d-flex justify-content-end" style="left: -0.5%">
      <x-form-check  value="1" name="print_note" checked="1" id="note">
        <x-slot name="title">{{__('label.print_note')}}</x-slot>
      </x-form-check>
    </div>
  </x-row>

</x-form>
