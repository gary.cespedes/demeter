@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.sale'))
@section('content')
  <section class="input-validation" id="register_sale">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">Actualizacion de Orden</x-slot>
          @include('sale.edit-sale')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-modal title="Registro Cliente" id="client_register">
    @include('client.register')
  </x-modal>

@endsection

@section('page-scripts')
  <script src="{{asset('js/pages/sale/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/sale/main.js')}}" type="module"></script>

  <script >

    id_sale_edit = `{{$sale->id}}`
    $('#client_id').val({{$sale->client_id}}).trigger('change');
    let array_detail = `<?php echo json_encode($detail_sale);?>`
    array_detail = JSON.parse(array_detail)
    array_detail.forEach( (value, key) => {
       add_car(value.product_id, value.quantity)
    })
  </script>

@endsection
