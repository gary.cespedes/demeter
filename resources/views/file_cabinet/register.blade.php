<x-form method="post" id="form_kardex" file="true">
  <x-slot name="route">{{route('get_file_cabinets')}}</x-slot>
  <x-row>
      <x-col sm="12" xs="12" md="12" lg="12">
        <x-row>
          <x-col sm="12" xs="12" md="6" lg="6">
            <x-form-group name="date_start" type="date">
              <x-slot name="text_label">{{__('label.date_start')}}</x-slot>
              <x-slot name="placeholder">{{__('label.date_start')}}</x-slot>
              <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d')}}</x-slot>
              <x-slot name="rules">
                data-validation-required-message="This field is required" required
              </x-slot>
            </x-form-group>
          </x-col>
          <x-col sm="12" xs="12" md="6" lg="6">
            <x-form-group name="date_end" type="date">
              <x-slot name="text_label">{{__('label.date_end')}}</x-slot>
              <x-slot name="placeholder">{{__('label.date_end')}}</x-slot>
              <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d')}}</x-slot>
              <x-slot name="rules">
                data-validation-required-message="This field is required" required
              </x-slot>
            </x-form-group>
          </x-col>
        </x-row>

        <x-row>
          <x-col sm="12" xs="12" md="6" lg="6">
            <x-select2 >
              @slot('options', $origin)
              <x-slot name="title">
                Almacen
              </x-slot>
              <x-slot name="id">warehouse_id</x-slot>
              <x-slot name="name">warehouse_id</x-slot>
              <x-slot name="rules">
                {{--                    disabled--}}
              </x-slot>
            </x-select2>
          </x-col>
          <x-col sm="12" xs="12" md="6" lg="6">
            <x-select2 >
              @slot('options', $products)
              <x-slot name="title">
                Productos
              </x-slot>
              <x-slot name="id">product_id</x-slot>
              <x-slot name="name">product_id</x-slot>
              <x-slot name="rules">
                {{--                    disabled--}}
              </x-slot>
            </x-select2>
          </x-col>
        </x-row>

      </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">
        <div class="col-11 d-flex justify-content-end" style="left: -0.5%">
          <x-form-check  value="1" name="excel" checked="0" id="excel">
            <x-slot name="title">{{__('label.print_excel')}}</x-slot>
          </x-form-check>
          <x-form-check  value="1" name="pdf" checked="1" id="pdf">
            <x-slot name="title">{{__('label.print_pdf')}}</x-slot>
          </x-form-check>
        </div>
    </x-col>

    <hr>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        Generar
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>


  </x-row>
</x-form>
