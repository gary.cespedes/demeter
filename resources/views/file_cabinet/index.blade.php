@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title',__('common.title_pages.file_cabinet'))

@section('page-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/validation/form-validation.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
@endsection
@section('content')
  <section class="input-validation" id="register_kardex">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_kardex')}}</x-slot>
          @include('file_cabinet.register')
        </x-card>
      </x-col>
    </x-row>
  </section>


  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_kardex')}}</x-slot>
        <x-row class="p-2" style="background-color: #EDEDED">
          <div id="container_kardex_result"></div>
        </x-row>
      </x-card>
    </x-col>
  </x-row>

@endsection

@section('vendor-scripts')

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset  ('js/scripts/forms/validation/form-validation.js')}}"></script>
  <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
  <script src="{{asset('js/pages/file_cabinet/crud.js')}}" type="module"></script>

@endsection
