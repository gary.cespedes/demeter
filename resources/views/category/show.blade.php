<h1 id="category_name" style="text-transform: uppercase;">{{$category->name}}</h1>
<div class="col-12 d-flex justify-content-end">
  <x-button type="button" color="primary" id="add_sub_category">
    {{__('common.button.add_sub_category')}}
  </x-button>
</div>

<x-table id="list_sub_categories" thead="thead_list_sub_categories">
</x-table>
<hr>
<div>
  @include('category.register_sub_category')
</div>
<hr>
<div>
  @include('category.edit_sub_category')
</div>

<script src="{{asset('js/pages/sub_categories/crud.js')}}" type="module"></script>
<script type="module">
     $('#thead_list_sub_categories').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.description')}}</th>
    </tr>`)
    $("#add_sub_category").click(function(){
      $('#register_sub_category').show();
    });

    $("#close_register").click(function(){
      $('#register_sub_category').hide();
    });
    $("#cancel-edit").click(function(){
      $('#form_sub_category_edit').hide();
    });

</script>
