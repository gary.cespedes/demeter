<section class="input-validation" id="register_sub_category" style="display:none">
  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card >
        <x-slot name="title">{{__('common.section_title.register_sub_category')}}</x-slot>
        <x-form method="post" id="form_sub_category" file="true">
          <x-slot name="route">{{route('store_sub_category')}}</x-slot>
          <x-row>
            <x-col sm="12" xs="12" md="12" lg="12">
              <x-form-group name="name" type="text">
                <x-slot name="text_label">{{__('label.name')}}</x-slot>
                <x-slot name="placeholder">{{__('label.name_placeholder')}}</x-slot>
                <x-slot name="rules">
                  data-validation-required-message="{{__('common.validation.required')}}"
                  required
                </x-slot>
              </x-form-group>
            </x-col>

            <x-col sm="12" xs="12" md="12" lg="12">

              <x-form-group name="description" type="text">
                <x-slot name="text_label">{{__('label.description')}}</x-slot>
                <x-slot name="placeholder">{{__('label.description_placeholder')}}</x-slot>
              </x-form-group>

            </x-col>
            <input type="hidden" name="category_id" value="{{$category->id}}">

            <div class="col-12 d-flex justify-content-end">
              <x-button type="submit" color="primary" >
                {{__('common.button.register')}}
              </x-button>
              <x-button type="reset" color="danger" id="close_register">
                {{__('common.button.cancel')}}
              </x-button>
            </div>

          </x-row>
        </x-form>

      </x-card>
    </x-col>
  </x-row>
</section>
