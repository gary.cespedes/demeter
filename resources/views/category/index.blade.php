@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.category'))
@section('content')
  <section class="input-validation" id="register_category">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_category')}}</x-slot>
          @include('category.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_category" style="display: none">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_category')}}</x-slot>
          @include('category.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_category')}}</x-slot>
        @include('category.list')
      </x-card>
    </x-col>
  </x-row>

  <x-modal title="Detalle de Categoria" id="category_detail">
    <div id="container_detail_category"></div>
  </x-modal>
@endsection
@section('page-scripts')
  <script src="{{asset('js/pages/category/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/category/main.js')}}" type="module"></script>
  <script>
    $('#thead_categories').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.description')}}</th>
    </tr>`)
  </script>

@endsection
