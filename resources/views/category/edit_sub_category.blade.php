<section class="input-validation" id="update_sub_category" style="display:none">
  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card >
        <x-slot name="title">{{__('common.section_title.edit_sub_category')}}</x-slot>
          <x-form method="put" id="form_sub_category_edit" file="true">
            <x-slot name="route">{{route('update_sub_category')}}</x-slot>
              <x-row>
                <x-col sm="12" xs="12" md="6" lg="6">
                  <x-form-group name="name" type="text">
                    <x-slot name="text_label">{{__('label.name')}}</x-slot>
                    <x-slot name="placeholder">{{__('label.name_placeholder')}}</x-slot>
                    <x-slot name="rules">
                      data-validation-required-message="This field is required" required
                    </x-slot>
                  </x-form-group>
                  <x-form-group name="description" type="text">
                    <x-slot name="text_label">{{__('label.description')}}</x-slot>
                    <x-slot name="placeholder">{{__('label.description_placeholder')}}</x-slot>
                    <x-slot name="rules">
                      data-validation-required-message="This field is required" required
                    </x-slot>
                  </x-form-group>
              </x-col>
                <div class="col-12 d-flex justify-content-end">
                  <x-button type="submit" color="warning" id="update">
                    {{__('common.button.update')}}
                  </x-button>
                  <x-button type="reset" color="danger" id="cancel2">
                    {{__('common.button.cancel')}}
                  </x-button>
                </div>
              </x-row>
            </x-form>
      </x-card>
    </x-col>
  </x-row>
</section>
