<div >
  <table>
    <tr>
      <td> <b>Codigo</b> </td>
      <td>{{$transfer->code_transfer}}</td>
    </tr>
    <tr>
      <td> <b>Fecha Transferencia</b> </td>
      <td>{{\Carbon\Carbon::parse($transfer->date_transfer)->format('d-m-Y')}}</td>
    </tr>
    <tr>
      <td> <b>Origen</b> </td>
      <td>{{$transfer->origin_name}}</td>
    </tr>
    <tr>
      <td> <b>Destino</b> </td>
      <td>{{$transfer->destine_name}}</td>
    </tr>

    <tr>
      <td> <b>Estado</b> </td>
      <td>{{$name_status}}</td>
    </tr>

  </table>
</div>
<h3>Lista de Productos</h3>
<table border="1" style="width: 100%">
  <thead >
    <tr>
      <th>{{__('label.code')}}</th>
      <th>{{__('label.product')}}</th>
      <th>{{__('label.quantity')}}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($details_transfer as $item)
      <tr >
        <td>{{$item->code}}</td>
        <td>{{$item->product}}</td>
        <td>{{$item->quantity}}</td>
      </tr>
    @endforeach
  </tbody>
</table>
