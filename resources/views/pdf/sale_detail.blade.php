<title>Venta</title>

<style>
  body{
    background-color: white !important;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
  }
  .table{
    border-collapse: collapse;
  }
  h2{
    text-transform: uppercase;
    font-weight: lighter;
  }

</style>
<div style="width: 100%; height: 180px ">
  <div style="width: 33.33%; float: left">
    LOGO
  </div>
  <div style="width: 33.33%; float: left; text-align: center; line-height: normal">
    <h2 >Nombre Empresa</h2>
    <p>Descripcion</p>
    <b>Telefono: </b>
    <b>Email: </b>
  </div>
  <div style="width: 33.33%; float: left; text-align: right; line-height: normal">
    <h2 >{{$type_sale}}</h2>
    <p >{{\Carbon\Carbon::parse($sale->date_sale)->format('d-m-Y')}}</p>
    <b>TIKET {{$sale->code}}</b>
  </div>
</div>

<table width="100%" >
  <tr>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>NIT: </b> </td>
    <td></td>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>FECHA: </b> </td>
    <td>{{\Carbon\Carbon::parse($sale->date_sale)->format('d-m-Y')}}</td>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>VENTA POR MENOR: </b> </td>
    <td></td>
  </tr>
  <tr>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>FACTURA: </b> </td>
    <td></td>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>CLIENTE: </b> </td>
    <td></td>
    <td style="text-align: right; border-bottom: black dotted 1px"> <b>VENDEDOR: </b> </td>
    <td>{{$sale->user}}</td>
  </tr>
</table>
<br>

<table border="1" width="100%" class="table" >
  <thead >
  <tr>
    <th>Nª</th>
    <th>{{__('label.product')}}</th>
    <th>{{__('label.description')}}</th>
    <th>{{__('label.quantity')}}</th>
    <th>{{__('label.price_unit')}}</th>
    <th>{{__('label.price_total')}}</th>
  </tr>
  </thead>
  <tbody>
  @foreach($detail_sale as $item)
    <tr>

      <td>{{$loop->iteration}}</td>
      <td>{{$item->name}}</td>
      <td>{{$item->description}}</td>
      <td>{{$item->quantity}}</td>
      <td>{{$item->price_unit}}</td>
      <td>{{$item->price_total}}</td>
    </tr>
  @endforeach
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><b>Total</b></td>
    <td>{{$sale->price_end}}</td>
  </tr>
  <tr>
    <td colspan="6">
      <div style="text-align: right; width: 100%">
        {{$mont_to_letter}}
      </div>
      <div style="width: 100%; height: 100px; padding: 10px">
        <div style="width: 80%; float: left; ">
          OBSERVACIONES
        </div>
        <div style="width: 20%; float: right; ">
          <table border="0">
            <tr>
              <td style="background-color: #d9d1d1">Descuento</td>
              <td></td>
            </tr>
            <tr>
              <td style="background-color: #d9d1d1">Precio Original</td>
              <td>{{$sale->price_initial}}</td>
            </tr>
            <tr>
              <td style="background-color: #d9d1d1">Precio Final</td>
              <td>{{$sale->price_end}}</td>
            </tr>
            <tr>
              <td style="background-color: #d9d1d1">A cuenta</td>
              <td>0</td>
            </tr>
            <tr>
              <td style="background-color: #d9d1d1">Saldo</td>
              <td>0</td>
            </tr>
          </table>
        </div>
      </div>
    </td>
  </tr>
  </tbody>
</table>
