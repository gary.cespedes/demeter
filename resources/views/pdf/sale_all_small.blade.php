<title>Venta</title>
<style>
  *{
    margin: 0;
    padding: 4px;
  }
  span{
    font-size: 8px;
  }
  body{
    background-color: white !important;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: normal;
  }
  .table{
    border-style:dashed;
  }
  .table > thead > tr > th{
    font-size: 8px;
    text-transform: uppercase;
    padding: 1px;
    text-align: center;
  }
  .table > tbody > tr > td{
    font-size: 8px;
    text-transform: uppercase;
    width:20%;
    text-align: center;
    word-wrap:break-word;
  }
</style>

<center>
  <img src="{{asset('images/edman/logo.jpg')}}" style="margin-top: 3px" alt="" srcset="" width="50px">
  <span>RUC: 20434893217</span> <br>
  <span>METALINOX EDMAN S.R.L</span>
</center>
<table style="margin-left: 5px" width="200" class="table" >
  <thead >
  <tr>
    <th>Codigo</th>
    <th>Tipo de Venta</th>
    <th>Tipo de Pago</th>
    <th>Precio Inicial</th>
    <th>Precio Final</th>
  </tr>
  </thead>
  <tbody>
  @foreach($sales as $item)
    <tr>
      <td>{{$item->code}}</td>
      <td>{{\App\Http\Controllers\WebController::$type_sale[$item->type_sale]}}</td>
      <td>{{\App\Http\Controllers\WebController::$payment_type[$item->payment_type]}}</td>
      <td>{{$item->price_initial}}</td>
      <td>{{$item->price_end}}</td>
    </tr>
  @endforeach
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td><b>Total</b></td>
    <td>{{$sum}}</td>
  </tr>
  </tbody>
</table>
<label style="font-size: 10px"><b>DOCUMENTO NO TRIBUTARIO-CONTROL INTERNO</b></label>
<div style="text-align: center">
  <p style="font-size: 7px">ESTE DOCUMENTO PUEDE SER CANJEADO POR UN COMPROBANTE DE PAGO EN EL PUNTO DE EMISION</p>
  <p style="font-size: 7px">www.edmanperu.com.pe</p>
  <p style="font-size: 7px">edman.adm.ventas@gmail.com</p>
  <p style="font-size: 7px">EDMAN PERU</p>
</div>
