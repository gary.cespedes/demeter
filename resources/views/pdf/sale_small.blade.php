<title>Venta</title>
<style>

  span{
    font-size: 8px;
  }
  body{
    background-color: white !important;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: normal;
  }
  .table{
    border-style:dashed;
  }


</style>

<center>
  <p style="font-size: 8px">ORDEN DE VENTA Nª {{$sale->code}}</p>

  <span>CLIENTE: {{$sale->name}} {{$sale->last_name}}</span> <br>
  <span>FECHA EMISION: {{\Carbon\Carbon::parse($sale->date_sale)->format('d-m-Y')}}</span> <br>
  <span>VENDEDOR: {{$sale->user}}</span>

    <table class="table" >
      <thead >
      <tr>
        <th>CODIGO</th>
        <th>PRODUCTO</th>
        <th>CANT</th>
        <th>P.U</th>
        <th>SUBTOTAL</th>
      </tr>
      </thead>
      <tbody>
      @foreach($detail_sale as $item)
        <tr>
          <td>{{$item->code}}</td>
          <td>{{$item->name}}</td>
          <td>{{$item->quantity}}</td>
          <td>{{$item->price_unit}}</td>
          <td>{{$item->price_total}}</td>
        </tr>
      @endforeach
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td><b>Total</b></td>
        <td>{{$sale->price_end}}</td>
      </tr>
      </tbody>
    </table>
</center>
