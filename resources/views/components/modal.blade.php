<div class="modal fade text-left" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="myModalLabel1">{{$title}}</h3>
        <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
          <i class="bx bx-x"></i>
        </button>
      </div>
      <div class="modal-body">
        {{$slot}}
      </div>
    </div>
  </div>
</div>
