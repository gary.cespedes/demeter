<div class="form-group">
  <label>{{$text_label}} @isset($rules) <span class="text-danger"> *</span>@endisset</label>
  <div class="controls">
    <input type="{{$type}}" name="{{$name}}" class="form-control" @isset($rules) {{$rules}} @endisset placeholder="{{$placeholder}}" @isset($value) value="{{$value}}" @endisset @isset($id) id="{{$id}}" @endisset>
  </div>
</div>
