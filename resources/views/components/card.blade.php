<div class="card" @isset($id) id="{{$id}}" @endisset>
  <div class="card-header">
    <h4 class="card-title">{{$title}}</h4>
      <a class="heading-elements-toggle">
        <i class="bx bx-dots-vertical font-medium-3"></i>
      </a>
      <div class="heading-elements">
        <ul class="list-inline mb-0">
          <li>
            <a data-action="collapse">
              <i class="bx bx-chevron-down"></i>
            </a>
          </li>
          @isset($actions)
            {{$actions}}
          @endisset
        </ul>
      </div>
  </div>
  <div class="card-content">
    <div class="card-body">
      {{$slot}}
    </div>
  </div>
</div>
