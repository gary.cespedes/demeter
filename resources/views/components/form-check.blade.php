<div class="checkbox">
  <input class="checkbox-input" type="checkbox" @isset($id) id="{{$id}}" @endisset value="{{$value}}" name="{{$name}}" data-name="{{$name}}" @if($checked != '0') checked @endif @isset($rules) {{$rules}} @endisset >
  <label @isset($id) for="{{$id}}" @endisset >
    @isset($title)
      {{$title}}
    @endisset
  </label>
</div>
