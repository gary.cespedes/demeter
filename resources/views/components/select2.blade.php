<label>{{$title}}</label>
<div class="form-group">
<select class="select2 form-control default-select" name="{{$name}}" @isset($id) id="{{$id}}" @endisset @isset($rules) {{$rules}} @endisset>
  @foreach($options as $option)
    <option value="{{$option->id}}">{{$option->name}}</option>
  @endforeach
</select>
{{$slot}}

</div>

