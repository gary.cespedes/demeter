<form class="formValidate form-horizontal" novalidate action="{{$route}}" method="{{$method}}" id="{{$id}}" @isset($file) enctype="multipart/form-data" @endisset autocomplete="off">
  {{$slot}}
</form>
