<x-form method="post" id="form_product" file="true">
  <x-slot name="route">{{route('store_user')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="4" lg="4">
      <x-select2 >
        @slot('options', $categories)
        <x-slot name="title">
          {{__('label.category')}} <a class="right text-primary" data-toggle="modal" data-target="#category_register"><i class="bx bx-plus-medical"></i>{{__('label.add_more_item')}}</a>
        </x-slot>
        <x-slot name="name">category_id</x-slot>
        <x-slot name="id">category_id</x-slot>
      </x-select2>

      <x-form-group name="code" type="text">
        <x-slot name="text_label">{{__('label.code')}}</x-slot>
        <x-slot name="placeholder">{{__('label.code')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}"
          required
        </x-slot>
      </x-form-group>
      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_product')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_product')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}"
          required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="4" lg="4">


      <x-form-group name="description" type="text">
        <x-slot name="text_label">{{__('label.description')}}</x-slot>
        <x-slot name="placeholder">{{__('label.description')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}"
          required
        </x-slot>
      </x-form-group>

      <x-form-group name="price_offer" type="text">
        <x-slot name="text_label">{{__('label.price_offer')}}</x-slot>
        <x-slot name="placeholder">{{__('label.price_offer')}}</x-slot>
      </x-form-group>

      <x-form-group name="price_unit" type="text">
        <x-slot name="text_label">{{__('label.price_unit')}}</x-slot>
        <x-slot name="placeholder">{{__('label.price_unit')}}</x-slot>
      </x-form-group>

    </x-col>

    <x-col sm="12" xs="12" md="4" lg="4">
      <x-form-group name="quantity_minim" type="text">
        <x-slot name="text_label">{{__('label.quantity_minim')}}</x-slot>
        <x-slot name="placeholder">{{__('label.quantity_minim')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}"
          required
        </x-slot>
      </x-form-group>

      <x-select2 >
        @slot('options', $units)
        <x-slot name="title">{{__('label.unit')}} <a class="right text-primary" data-toggle="modal" data-target="#unit_register"><i class="bx bx-plus-medical"></i>{{__('label.add_more_item')}}</a></x-slot>
        <x-slot name="name">unit_id</x-slot>
        <x-slot name="id">unit_id</x-slot>

      </x-select2>
      <x-input-file title="Foto Producto" name="file" labelFile="Escoja La foto" multiple="true">
      </x-input-file>
    </x-col>
  </x-row>
  @include('product.list_prices')
  <x-row>
    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" >
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
