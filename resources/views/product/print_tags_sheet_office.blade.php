<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
  <style>
    p{
      line-height: normal;
    }
    .title{
      font-size: 20px;
    }
  </style>
</head>
<body>

  <div id="content" class="container-fluid row">

  </div>

<script src="{{asset('vendors/js/vendors.min.js')}}"></script>

<script src="{{asset('js/scripts/jsbarcode.all.min.js')}}"></script>

<script>

  var tags = JSON.parse(window.localStorage.getItem('tags'))

  let div_item = ""
  tags.forEach((value) => {
      let count = 0;
      while(count < value.quantity){
        div_item += `<div style="width: 30%;margin-bottom: 20px; margin-right: 10px " class="text-center">
            <b class="title">MODA DEL CELULAR</b>
            <svg id="${value.code}${count}"></svg>
            <div   class="text-center container-fluid">
                <p>${value.description} - ${value.name}</p>
                <b style="font-size: 20px">${parseFloat(value.price).toFixed(2)} Bs.-</b>
            </div>

        </div> `
        count ++;
      }
  })


  $('#content').html(div_item)

  tags.forEach((value) => {
    let count = 0
    while(count < value.quantity){
      JsBarcode(`#${value.code}${count}`, value.code, {
        width: 4,
        height: 60
      })
      count++
    }


  })

</script>
</body>
</html>
