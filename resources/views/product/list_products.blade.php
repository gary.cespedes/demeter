<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-extended.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/colors.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/components.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/themes/dark-layout.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">

  <title>Document</title>
</head>
<body>

<div class="container">
  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_product')}}</x-slot>
        <x-table id="list_products" thead="thead_products">
        </x-table>

      </x-card>
    </x-col>
  </x-row>
</div>

<script>
  var routes = {
    get_products: `{{route('get_products')}}`,
  }

  var columns_active_product = [

    {
      data: "name",
    },
    {
      data: "description"
    },
    {
      data: "quantity_minim"
    },
  ];

  var table_product_instance;
  var table_product;

</script>

<script src="{{asset('vendors/js/vendors.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/common/common.js')}}"></script>

<script src="{{asset('js/pages/products/main.js')}}" type="module"></script>
<script>
  $('#thead_products').html(`<tr>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.description')}}</th>
        <th>{{__('common.table.quantity_minim')}}</th>
    </tr>`)
</script>

</body>
</html>
