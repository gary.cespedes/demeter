<x-row>
  <div class="container">

    <table class="table table-bordered">
      <tr>
        <td> <b>PRODUCTO</b> </td>
        <td>{{$product->name}}</td>
      </tr>
      <tr>
        <td> <b>Codigo</b> </td>
        <td>{{$product->code}}</td>
      </tr>
      <tr>
        <td> <b>Codigo de Barra</b> </td>
        <td>{{$product->code_bar}}</td>
      </tr>
      <tr>
        <td> <b>Descripcion</b> </td>
        <td>{{$product->description}}</td>
      </tr>

    </table>

  </div>
</x-row>

<hr>
<h3>Navegacion</h3>
<div class="card-body">
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item current">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" role="tab" aria-selected="true">
        <i class="bx bxs-file-image align-middle"></i>
        <span class="align-middle">Imagen</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="false">
        <i class="bx bx-list-check align-middle"></i>
        <span class="align-middle">Precios</span>
      </a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
      @if($product->photo)
        <img class="img-fluid" src="{{asset($product->photo)}}" style="max-width: 50%; height: auto" alt="Imagen Producto">
      @else
        <h5>No Existe Imagen</h5>
      @endif
     </div>
    <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
      <div class="container" style="margin-bottom: 10px" >
        @if(count($prices) < 6)
          <a class="btn btn-white" id="add_new_price">+Agregar nuevo precio</a>
          <x-form method="post" id="form_price" route="{{route('price_store')}}">
            <x-row style="display: none" id="container_new_price">
              <x-col xs="4" sm="4" md="4" lg="4">
                <x-form-group type="text" name="description" id="description">
                  <x-slot name="text_label">Descripcion</x-slot>
                  <x-slot name="placeholder">Descripcion</x-slot>
                  <x-slot name="rules">
                    required = "true"
                  </x-slot>
                </x-form-group>
              </x-col>
              <x-col xs="4" sm="4" md="4" lg="4">
                <x-form-group type="text" name="price" id="price">
                  <x-slot name="text_label">Precio</x-slot>
                  <x-slot name="placeholder">Precio</x-slot>
                  <x-slot name="rules">
                    required="true" number="true"
                  </x-slot>
                </x-form-group>
              </x-col>
              <x-col xs="4" sm="4" md="4" lg="4">
                <button class="btn btn-primary" type="submit" data-id="{{$product->id}}">Agregar</button>
              </x-col>
            </x-row>
          </x-form>
        @endif
      </div>
      <table class="table table-bordered" id="show_prices text-nowrap"  style="width: 100%">
        <thead >
        <th>{{__('label.description')}}</th>
        <th>{{__('label.price')}}</th>
        <th>{{__('label.is_predetermined')}}</th>
        <th>{{__('common.table.options')}}</th>
        </thead>

        <tbody>
        @foreach($prices as $price)
          <tr @if($price->is_predetermined) style="background-color: rgb(156,243,243)" @endif>
            <td>{{$price->description}}</td>
            <td><input type="text" class="form-control" value="{{$price->price}}" id="{{$price->id}}" readonly="true" onfocus="this.readOnly='';" onblur="this.readOnly='true';"></td>
            <td>

              <input type="checkbox" class="form-control" value="1" @if($price->is_predetermined)  checked @endif  data-id="{{$price->id}}" onChange="update_price(this)">

            </td>
            <td>
              <button class="btn btn-icon btn-warning " data-id = "{{$price->id}}" data-price = "{{$price->price}}" title="Revertir" onclick="reload_price(this)"> <i class="bx bx-revision" ></i></button>
              <button class="btn  btn-success btn-icon" data-id = "{{$price->id}}" title="Guardar" onclick="update_data(this)"> <i class="bx bx-save" ></i></button>
              <button class=" btn  btn-danger btn-icon" data-id = "{{$price->id}}" title="Eliminar" onclick="delete_price(this)"> <i class="bx bx-trash" ></i></button>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>


<script>

  function update_data(button){
    let id = $(button).data('id')
    let value = $(`#${id}`).val()
    $.post(`{{route('update_price')}}`, {id, value}, (data) => {
      toastr.success('', common.success);
    })

  }

  function update_price(input) {
    let id = $(input).data('id');
    let checked = ($(input).is(':checked')) ? 1 : 0
    $.post(routes.update_price_product, {id, checked})
      .done(function( data ) {
        load_view_product.main = null
        load_view_product.load_view('register')
      });
  }

  function delete_price(button){
    let id = $(button).data('id')

    $.post(`{{route('delete_price')}}`, {id}, (data) => {
      toastr.success('', common.success);
      load_view_product.main = null
      load_view_product.load_view('register')
    })

  }


  function reload_price(button){
    let id = $(button).data('id')
    let price = $(button).data('price')
    let value = $(`#${id}`).val(price)
  }


  $('#add_new_price').click(() => {
    $('#container_new_price').toggle();
  })

  $('#form_price').submit((e) => {
    e.preventDefault()
    let id = `{{$product->id}}`
     $.post(`{{route('price_store')}}`, {
       product_id: id,
       description: $('#description').val(),
       price: $('#form_price input[name="price"]').val(),
       is_predetermined: 0,

     }, (response) => {
       toastr.success('', common.success);
       load_view_product.main = null
       load_view_product.load_view('register')
     })
  })

</script>



