<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <style>
    .contenido > b{
      line-height: normal;
      font-family: Arial;
    }
    .body-text {
      font-size: 20px;
    }
    .title{
      font-size: 25px;
      font-family: Arial;
      margin-top: 30px;
    }
  </style>
</head>
<body>
<center>
  <div id="content">

  </div>
</center>

<script src="{{asset('vendors/js/vendors.min.js')}}"></script>

<script src="{{asset('js/scripts/jsbarcode.all.min.js')}}"></script>

<script>

  var tags = JSON.parse(window.localStorage.getItem('tags'))
  let div_item = ""
  let duplicated = []
  tags.forEach((value) => {
      console.log(value)
      let count = 0;
      while(count < value.quantity){
        console.log(value.code)
        div_item += `<div style="width: 590px;  padding: 20px">
            <b class="title" style="padding-top: 20px">MODA DEL CELULAR</b> <br>
            <svg id="${value.code}${count}"></svg>
            <div class="contenido">
                <b class="body-text">${value.description} - ${value.name}</b> <br>
                <b style="font-size: 25px">${parseFloat(value.price).toFixed(2)} Bs.-</b>
            </div>
        </div> `
        count ++;
      }
  })

  console.log(div_item)
  $('#content').html(div_item)

  tags.forEach((value) => {
    let count = 0
    while(count < value.quantity){
      JsBarcode(`#${value.code}${count}`, value.code, {
        width: 5,
        height: 100
      })
      count++
    }

    window.print();
  })

</script>
</body>
</html>
