@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.product'))

@section('content')
  <section class="input-validation" id="register_product">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_product')}}</x-slot>
          @include('product.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_product" style="display: none">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_product')}}</x-slot>
          @include('product.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>

    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_product')}}</x-slot>
          @include('product.list')
      </x-card>
    </x-col>
  </x-row>

  <x-modal title="Registro Categoria" id="category_register">
    @include('category.register')
  </x-modal>

  <x-modal title="Registro Unidad" id="unit_register">
    @include('unit.register')
  </x-modal>

  <x-modal title="DETALLE DEL PRODUCTO" id="info_product">
    <div id="container_info_product"></div>
  </x-modal>
@endsection

@section('page-scripts')
  <script src="{{asset('js/pages/products/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/products/main.js')}}" type="module"></script>
  <script>
    $('#thead_users').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.description')}}</th>
        <th>{{__('common.table.quantity_minim')}}</th>
    </tr>`)
  </script>
  <script src="{{asset('js/scripts/extensions/sweet-alerts.js')}}"></script>
  <script src="{{asset('js/scripts/extensions/toastr.js')}}"></script>

@endsection
