<x-row class="p-2" style="background-color: #EDEDED">
  <div class="col x12 s12 l12 m12 table-responsive">
    <table class="table highlight text-nowrap" >
      <tr>
        <td>
          <x-form-group name="description_price" type="text">
            <x-slot name="text_label">{{__('label.name_price')}}</x-slot>
            <x-slot name="id">price_description</x-slot>
            <x-slot name="placeholder">{{__('label.name_price')}}</x-slot>
          </x-form-group>
        </td>
        <td >
          <x-form-group name="price" type="text">
            <x-slot name="text_label">{{__('label.price')}}</x-slot>
            <x-slot name="id">price</x-slot>
            <x-slot name="placeholder">{{__('label.price')}}</x-slot>
          </x-form-group>
        </td>
        <td>
          <x-form-check id="is_predetermined" value="1" name="is_predetermined" checked="0" >
            <x-slot name="title">{{__('label.is_predetermined')}}</x-slot>
          </x-form-check>
        </td>
        <td >
          <x-button type="button"  color="info btn btn-sm" id="addProductsInList">
            <i class="bx bx-plus-circle"></i>
          </x-button>
        </td>
      </tr>
    </table>
  </div>
</x-row>

<x-row>
  <div class="col s12 l12 m12 table-responsive">
    <table class="table highlight text-nowrap" id="table_attributes_products" >
      <thead>
      <tr>
        <th>{{__('common.table.description')}}</th>
        <th>{{__('common.table.price')}}</th>
        <th>{{__('label.is_predetermined')}}</th>
        <th>{{__('common.table.options')}}</th>
      </tr>
      </thead>
      <tbody id="productsListBody">
      </tbody>
    </table>
    <div class="input-field">
      <div class="error_table_attributes"></div>
    </div>
  </div>
</x-row>
