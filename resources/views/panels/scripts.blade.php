
    <!-- BEGIN: Vendor JS-->
    <script>
        var assetBaseUrl = "{{ asset('') }}";
        var routes = {
          //USERS
          user_register_view: `{{route('user_register_view')}}`,
          user_permission_view: `{{route('user_permission_view')}}`,
          user_store: `{{route('store_user')}}`,
          update_user: `{{route('update_user')}}`,
          user_delete: `{{route('user_delete')}}`,
          get_users: `{{route('get_users')}}`,
          get_user: `{{route('get_user')}}`,

          //Transfers
          transfer_register_view: `{{route('transfer_register_view')}}`,
          transfer_store: `{{route('store_transfer')}}`,
          update_transfer: `{{route('update_transfer')}}`,
          transfer_delete: `{{route('transfer_delete')}}`,
          get_transfers: `{{route('get_transfers')}}`,
          get_transfers_for_status: `{{route('get_transfers_for_status')}}`,
          get_transfer: `{{route('get_transfer')}}`,
          change_status_transfer: `{{route('change_status_transfer')}}`,
          show_transfer: `{{route('show_transfer')}}`,
          print_transfer: `{{route('print_transfer')}}`,

          //Income
          batch_register_view: `{{route('batch_register_view')}}`,
          batch_store: `{{route('store_batch')}}`,
          update_batch: `{{route('update_batch')}}`,
          batch_delete: `{{route('batch_delete')}}`,
          get_batches: `{{route('get_batches')}}`,
          show_batch: `{{route('show_batch')}}`,
          get_batch: `{{route('get_batch')}}`,
          get_batch_for_category: `{{route('get_batch_for_category')}}`,

          //Sale
          sale_register_view: `{{route('sale_register_view')}}`,
          sale_index: `{{route('sale_index')}}`,
          enabled_discount_total: `{{route('enabled_discount_total')}}`,
          sale_store: `{{route('store_sale')}}`,
          update_sale: `{{route('update_sale')}}`,
          sale_delete: `{{route('sale_delete')}}`,
          sale_restore: `{{route('sale_restore')}}`,
          sale_edit: `{{route('sale_edit')}}`,
          get_sales: `{{route('get_sales')}}`,
          get_sales_inactive: `{{route('get_sales_inactive')}}`,
          show_sale: `{{route('show_sale')}}`,
          get_sale: `{{route('get_sale')}}`,
          print_sale: `{{route('print_sale')}}`,
          print_all_sale: `{{route('print_all_sale')}}`,
          //QUOTATION
          quotation_register_view: `{{route('quotation_register_view')}}`,
          quotation_index: `{{route('quotation_index')}}`,
          quotation_store: `{{route('store_quotation')}}`,
          update_quotation: `{{route('update_quotation')}}`,
          quotation_delete: `{{route('quotation_delete')}}`,
          quotation_restore: `{{route('quotation_restore')}}`,
          quotation_edit: `{{route('quotation_edit')}}`,
          get_quotations: `{{route('get_quotations')}}`,
          get_quotations_inactive: `{{route('get_quotations_inactive')}}`,
          show_quotation: `{{route('show_quotation')}}`,
          get_quotation: `{{route('get_quotation')}}`,
          print_quotation: `{{route('print_quotation')}}`,
          print_all_quotation: `{{route('print_all_quotation')}}`,

          //PROVIDER
          provider_register_view: `{{route('provider_register_view')}}`,
          provider_store: `{{route('store_provider')}}`,
          update_provider: `{{route('update_provider')}}`,
          provider_delete: `{{route('provider_delete')}}`,
          get_providers: `{{route('get_providers')}}`,
          get_provider: `{{route('get_provider')}}`,

          //KARDEX
          get_file_cabinets: `{{route('get_file_cabinets')}}`,

          //CLIENT
          client_register_view: `{{route('client_register_view')}}`,
          client_store: `{{route('store_client')}}`,
          update_client: `{{route('update_client')}}`,
          client_delete: `{{route('client_delete')}}`,
          get_clients: `{{route('get_clients')}}`,
          get_client: `{{route('get_client')}}`,

          //Branch
          branch_register_view: `{{route('branch_register_view')}}`,
          branch_store: `{{route('store_branch')}}`,
          update_branch: `{{route('update_branch')}}`,
          branch_delete: `{{route('branch_delete')}}`,
          get_branches: `{{route('get_branches')}}`,
          get_branchs: `{{route('get_branchs')}}`,
          get_detail_branch: `{{route('get_detail_branch')}}`,
          change_origin: `{{route('change_origin')}}`,
          get_branch_with_stock: `{{route('get_branch_with_stock')}}`,

          //Product
          product_register_view: `{{route('product_register_view')}}`,
          product_store: `{{route('store_product')}}`,
          update_product: `{{route('update_product')}}`,
          product_delete: `{{route('product_delete')}}`,
          get_products: `{{route('get_products')}}`,
          get_product: `{{route('get_product')}}`,
          show_product: `{{route('show_product')}}`,
          update_price_product: `{{route('update_price_product')}}`,
          get_product_with_prices: `{{route('get_product_with_prices')}}`,
          get_products_by_filters: `{{route('get_products_by_filters')}}`,

          //Category
          category_register_view: `{{route('category_register_view')}}`,
          category_store: `{{route('store_category')}}`,
          update_category: `{{route('update_category')}}`,
          category_delete: `{{route('category_delete')}}`,
          get_categories: `{{route('get_categories')}}`,
          show_category: `{{route('show_category')}}`,
          get_category: `{{route('get_category')}}`,

          //SubCategory
          sub_category_store: `{{route('store_sub_category')}}`,
          update_sub_category: `{{route('update_sub_category')}}`,
          sub_category_delete: `{{route('sub_category_delete')}}`,
          get_sub_categories: `{{route('get_sub_categories')}}`,
          get_sub_category: `{{route('get_sub_category')}}`,
          sub_category_get: `{{route('sub_category_get')}}`,

          //Unit
          unit_store: `{{route('store_unit')}}`,
          get_unit: `{{route('get_unit')}}`,
          update_unit: `{{route('update_unit')}}`,
          unit_delete: `{{route('unit_delete')}}`,
          get_units: `{{route('get_units')}}`,

          list_products: `{{route('list_products')}}`,
          print_tags: `{{route('print_tags')}}`
        }
    </script>
    <script src="{{asset('vendors/js/vendors.min.js')}}"></script>
    <script src="{{asset('fonts/LivIconsEvo/js/LivIconsEvo.tools.js')}}"></script>
    <script src="{{asset('fonts/LivIconsEvo/js/LivIconsEvo.defaults.js')}}"></script>
    <script src="{{asset('fonts/LivIconsEvo/js/LivIconsEvo.min.js')}}"></script>
    <script src="{{asset('js/common/common.js')}}"></script>
    <script src="{{asset('js/common/vars_common.js')}}"></script>
    <script src="{{asset('js/components/Button.js')}}"></script>
    <script src="{{asset('js/language/es.js')}}"></script>
    <script src="{{asset('js/own_creations/barcodeListener.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <script src="{{asset('vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{asset('vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('vendors/js/extensions/polyfill.min.js')}}"></script>
    <script src="{{asset('js/own_creations/table_dynamic.js')}}"></script>
    <script src="{{asset('js/scripts/jsbarcode.all.min.js')}}"></script>

    @yield('vendor-scripts')

    <!-- BEGIN: Theme JS-->
    @if($configData['mainLayoutType'] == 'vertical-menu')
    <script src="{{asset('js/scripts/configs/vertical-menu-light.js')}}"></script>
    @else
    <script src="{{asset('js/scripts/configs/horizontal-menu.js')}}"></script>
    @endif
    <script src="{{asset('js/core/app-menu.js')}}"></script>
    <script src="{{asset('js/core/app.js')}}"></script>
    <script src="{{asset('js/scripts/components.js')}}"></script>
    <script src="{{asset('js/scripts/footer.js')}}"></script>
    <script src="{{asset('js/scripts/customizer.js')}}"></script>
    <script src="{{asset('assets/js/scripts.js')}}"></script>

    <script src="{{asset  ('js/scripts/forms/validation/form-validation.js')}}"></script>
    <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
    <script src="{{asset('js/scripts/datatables/datatable.js')}}"></script>

    <!-- END: Theme JS-->
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
    <script src="{{asset('js/scripts/datatables/datatable.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/sweetalert2.min.css')}}">
    <!-- BEGIN: Page JS-->
    @yield('page-scripts')
    <!-- END: Page JS-->
