<x-form method="post" id="form_branch_register">
  <x-slot name="route">{{route('store_branch')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="12" lg="12">
      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_warehouse')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_warehouse')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">
      <x-form-group name="address" type="text">
        <x-slot name="text_label">{{__('label.address')}}</x-slot>
        <x-slot name="placeholder">{{__('label.address')}}</x-slot>
        <x-slot name="rules">
          required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">
      <x-form-group name="phone" type="text">
        <x-slot name="text_label">{{__('label.phone')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone')}}</x-slot>
        <x-slot name="rules">
          required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">
      <x-form-group name="phone_alternative" type="text">
        <x-slot name="text_label">{{__('label.phone_alternative')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone_alternative')}}</x-slot>
        <x-slot name="rules">
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">
      <x-form-check  value="1" name="is_warehouse" checked="0" id="is_warehouse">
        <x-slot name="title">{{__('label.is_warehouse')}}</x-slot>
      </x-form-check>
    </x-col>


    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
