@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title',__('common.title_pages.branch'))
{{-- page styles --}}
@include('pages.app-chat-sidebar')
@section('content')

  <section class="input-validation" id="register_branch">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_warehouse')}}</x-slot>
          @include('branch.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_branch" style="display: none">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_warehouse')}}</x-slot>
          @include('branch.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_warehouse')}}</x-slot>
        @include('branch.list')
      </x-card>
    </x-col>
  </x-row>
@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset('js/pages/branch/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/branch/main.js')}}" type="module"></script>
  <script>
    $('#thead_branch').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.address')}}</th>
        <th>{{__('common.table.phone')}}</th>
        <th>{{__('common.table.is_warehouse')}}</th>
        <th>{{__('common.table.user')}}</th>
    </tr>`)
  </script>

@endsection
