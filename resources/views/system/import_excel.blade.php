@extends('layouts.contentLayoutMaster')
@section('vendor-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/ui/prism.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/file-uploaders/dropzone.min.css')}}">
@endsection
@section('page-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/file-uploaders/dropzone.css')}}">
@endsection
@section('content')

  <section id="dropzone-examples">

      <div class="row">
        <div class="col-12">

          <x-card >
            <x-slot name="title">Selecciona tu Excel</x-slot>
            <p class="card-text">Para que su archivo excel se importe correctamente tiene que subir en el formato indicado.
              <br>

              Si usted no Conoce ese Formato, Consulte con el <code> Administrador del Sistema</code> o un <code> Tecnico de DEMETER</code>
              para recibir la Auditoria correcta</p>
            <p>Puede Descargar los formatos desde aqui <a href="{{asset('format_excel/es/format.zip')}}" download="FormatoDemeter">
                Descargar Formato Demeter
              </a></p>

            <div class="form-group">
              <label>Seleccione el modulo del Excel: <span class="text-danger">*</span></label>
              <div class="controls">
                <select class="select2 form-control default-select" name="module" id="module">
                  @foreach(\App\Http\Controllers\WebController::$modules as $key => $item)
                    <option value="{{$key}}">{{$item}}</option>
                  @endforeach
                </select>
              </div>
            </div>

              <form action="{{url('upload_file_excel')}}" class="dropzone dropzone-area"  id="dpz-remove-thumb">
                @csrf

                <div class="dz-message">Arrastra o Selecciona tu Archivo</div>

              </form>

          </x-card>

        </div>
      </div>
  </section>
@endsection

@section('vendor-scripts')
  <script src="{{asset('vendors/js/extensions/dropzone.min.js')}}"></script>
  <script src="{{asset('vendors/js/ui/prism.min.js')}}"></script>

@endsection

{{-- page scripts --}}
@section('page-scripts')

  <script>
    Dropzone.options.dpzRemoveThumb = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 1, // MB
      addRemoveLinks: true,
      dictRemoveFile: "Eliminar de la Lista",
      init: function() {
        this.on("sending", function(file, xhr, formData) {
          let module = $('#module').val();
          formData.append("module", module);
        });
      }

    }


  </script>

@endsection
