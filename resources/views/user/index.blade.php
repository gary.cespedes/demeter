@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.user'))
@section('content')
<div id="index_user">
  <section class="input-validation" id="register_user">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_user')}}</x-slot>
          @include('user.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" style="display: none" id="update_user">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_user')}}</x-slot>
          @include('user.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_user')}}</x-slot>
        @include('user.list')
      </x-card>
    </x-col>
  </x-row>

</div>

<div id="config_permission" style="display: none">
  <x-row>

    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card id="permissions">
        <x-slot name="title">{{__('common.section_title.config_permission')}}</x-slot>
        <x-slot name="actions">
          <li>
            <a data-action="back_close">
              <i class="bx bx-x"></i>
            </a>
          </li>
        </x-slot>
        <div id="content_config">

        </div>
      </x-card>
    </x-col>

  </x-row>

</div>

@endsection

@section('vendor-scripts')

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset  ('js/scripts/forms/validation/form-validation.js')}}"></script>
  <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
  <script src="{{asset('js/scripts/datatables/datatable.js')}}"></script>
  <script src="{{asset('js/pages/users/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/users/main.js')}}" type="module"></script>
  <script>
    $('#thead_users').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.email')}}</th>
    </tr>`)
  </script>
  <script src="{{asset('js/scripts/extensions/sweet-alerts.js')}}"></script>
  <script src="{{asset('js/scripts/extensions/toastr.js')}}"></script>

@endsection
