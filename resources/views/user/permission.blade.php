<x-card id="permissions">

  <x-slot name="title">{{__('common.section_title.config_permission')}}</x-slot>
  <x-slot name="actions">
    <li>
      <a data-action="back_close">
        <i class="bx bx-x"></i>
      </a>
    </li>
  </x-slot>

  <div class="container">
      <div class="row">
        <div class="col-md-6">
          <x-select2>
            @slot('options', $templates)
            <x-slot name="name">permission_template_id</x-slot>
            <x-slot name="title">{{__('label.choose_template')}}</x-slot>
          </x-select2>
        </div>
        <div class="col-md-6">
          <button type="button" data-id = "{{$user->id}}" class="to_assign_template btn btn-secondary">
            {{__('label.update_permission')}}
          </button>
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create_permission">
            {{__('label.create_template')}}
          </button>
          <button type="button" data-id = "{{$user->id}}" disabled class="update_template btn btn-warning ">
            {{__('label.update_template')}}
          </button>
        </div>
      </div>

  </div>

</x-card>

<script>
  jQuery(".select2").select2({
    width: '100%'
  });
</script>
