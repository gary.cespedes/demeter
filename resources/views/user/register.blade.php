<x-form method="post" id="form_user" file="true">
  <x-slot name="route">{{route('store_user')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">

      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_user')}} </x-slot>
        <x-slot name="placeholder">{{__('label.name_user')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}" required
        </x-slot>
      </x-form-group>


      <x-form-group name="email" type="email">
        <x-slot name="text_label">{{__('label.email_user')}}</x-slot>
        <x-slot name="placeholder">{{__('label.email_user')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}" required
          data-validation-email-message="{{__('common.validation.email')}}"
        </x-slot>
      </x-form-group>


      <x-select2 >
        @slot('options', $branches)
        <x-slot name="title">{{__('label.choose_branch')}}</x-slot>
        <x-slot name="name">branch_id</x-slot>
      </x-select2>

    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">

      <x-form-group name="password" type="password">
        <x-slot name="text_label">{{__('label.password')}}</x-slot>
        <x-slot name="placeholder">{{__('label.password')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="{{__('common.validation.required')}}" required
        </x-slot>
      </x-form-group>

      <x-form-group name="password" type="password">
        <x-slot name="text_label">{{__('label.repeat_password')}}</x-slot>
        <x-slot name="placeholder">{{__('label.repeat_password')}}</x-slot>
        <x-slot name="rules">
            data-validation-match-match="password"
          data-validation-required-message="{{__('common.validation.required')}}" required
          data-validation-match-message="{{__('common.validation.match')}}" required

          required
        </x-slot>
      </x-form-group>


      <x-input-file title="Foto Usuario" name="file" labelFile="Escoja La foto">
      </x-input-file>


      <x-form-check  value="1" name="is_power_user" checked="0" id="is_power_user">
        <x-slot name="title">{{__('label.is_power_user')}}</x-slot>
      </x-form-check>

    </x-col>

    <hr>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
