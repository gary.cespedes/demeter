@extends('layouts.contentLayoutMaster')
{{-- page Title --}}
@section('title','Dashboard Ecommerce')
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/dashboard-ecommerce.css')}}">
@endsection

@section('content')
<!-- Dashboard Ecommerce Starts -->
<section>
  @if (session('status'))
    @if(session('status') == 1)
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
    @else
      <div class="alert alert-danger">
        {{ session('status') }}
      </div>
    @endif
  @endif
</section>

<section id="dashboard-ecommerce">
    <div class="row">
      <div class="col-xl-5 col-md-5 col-xs-12 col-sm-12">
        <div class="card">
          <div class="card-header">
            <h3 class="greeting-text">Vendedor "{{strtoupper(\Illuminate\Support\Facades\Auth::user()->name)}}" </h3>
            <p class="mb-0">Ventas de Hoy</p>
          </div>
          <div class="card-content">
            <div class="card-body">
              <div class="d-flex justify-content-between align-items-end">
                <div class="dashboard-content-left">
                  <h1 class="text-primary font-large-2 text-bold-500">S/ {{$total_sale}} </h1>
                  <p>Cantidad de Ventas: {{$quantity_sales}}</p>
                  <button type="button" id="close_box" class="btn btn-primary glow">Cerrar Caja</button>
                </div>
                <div class="dashboard-content-right">
                  <img src="{{asset('images/icon/cup.png')}}" height="220" width="220" class="img-fluid"
                    alt="Dashboard Ecommerce" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-7 col-md-7 col-xs-12 col-sm-12"  >
        <div class="card marketing-campaigns">
          <div class="card-header d-flex justify-content-between align-items-center pb-1">
            <h4 class="card-title">Lista de Ventas del Dia</h4>
          </div>
          <div class="card-content">
            <div class="card-body pb-0">
              <div class="row">
                <div class="col-md-3 col-12">
                  <button id="print_all" class="btn btn-sm btn-primary glow mt-md-2 mb-1">Imprimir Todo</button>
                </div>
              </div>
            </div>
          </div>
          <div class="table-responsive" style="height:220px;overflow-x: auto">
            <table id="list_sales" class="table table-borderless table-marketing-campaigns mb-0">
              <thead>
                <tr>
                  <th class="text-center">Action</th>
                  <th>Codigo</th>
                  <th>Tipo de Venta</th>
                  <th>Precio Inicial</th>
                  <th>Precio Final</th>
                  <th>Tipo de Pago</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sales as $item)
                  <tr>
                    <td>
                      <button id="show" class="btn btn-icon btn-info glow mr-1" data-id = "{{$item->id}}"> <i class="bx bx-show"></i></button>
                      <button id="print" class="btn btn-icon btn-warning glow mr-1" data-id = "{{$item->id}}"> <i class="bx bx-printer"></i></button>
                    </td>
                    <td>{{$item->code_sale}}</td>
                    <td>{{\App\Http\Controllers\WebController::$type_sale[$item->type_sale]}}</td>
                    <td>{{$item->price_initial}}</td>
                    <td>{{$item->price_end}}</td>
                    <td>{{\App\Http\Controllers\WebController::$payment_type[$item->payment_type]}}</td>

                  </tr>
                @endforeach
              </tbody>
            </table>
            <!-- table ends -->
          </div>
        </div>
      </div>
    </div>
</section>

<x-modal title="Detalle de Venta" id="info_sale">
  <div id="container_info_sale"></div>
</x-modal>
@endsection

@section('page-scripts')
  <script src="{{asset('js/pages/dashboard/crud.js')}}" type="module"></script>

@endsection

