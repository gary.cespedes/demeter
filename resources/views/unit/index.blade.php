@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.unit'))
@section('content')
  <section class="input-validation" id="register_unit">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_unit')}}</x-slot>
          @include('unit.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_unit" style="display: none">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_unit')}}</x-slot>
          @include('unit.edit')

        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_unit')}}</x-slot>
        @include('unit.list')
      </x-card>
    </x-col>
  </x-row>

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset('js/pages/unit/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/unit/main.js')}}" type="module"></script>
  <script>
    $('#thead_units').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.description')}}</th>
    </tr>`)
  </script>

@endsection
