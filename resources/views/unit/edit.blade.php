<x-form method="put" id="form_unit_edit" file="true">
  <x-slot name="route">{{route('update_unit')}}</x-slot>
  <x-row>

    <x-col sm="12" xs="12" md="12" lg="12">

      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_unit')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_unit')}}</x-slot>
        <x-slot name="rules">
          required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="12" lg="12">

      <x-form-group name="description" type="text">
        <x-slot name="text_label">{{__('label.description')}}</x-slot>
        <x-slot name="placeholder">{{__('label.description')}}</x-slot>
      </x-form-group>
    </x-col>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="warning" id="update">
        {{__('common.button.update')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel2">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
