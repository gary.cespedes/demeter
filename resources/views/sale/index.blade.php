@extends('layouts.contentLayoutMaster')
@section('title',__('common.title_pages.sale'))
@section('content')
  <section class="input-validation" id="register_sale">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_sale')}}</x-slot>
          @include('sale.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-modal title="Registro Cliente" id="client_register">
    @include('client.register')
  </x-modal>

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script>
    type_sale = 1
  </script>
  <script src="{{asset('js/pages/sale/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/sale/main.js')}}" type="module"></script>
  <script>
    $('#thead_sales').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.type_sale')}}</th>
        <th>{{__('common.table.date_sale')}}</th>
        <th>{{__('common.table.price_initial')}}</th>
        <th>{{__('common.table.price_end')}}</th>
    </tr>`)

    $('#thead_warehouse_with_stock').html(`<tr>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.stock')}}</th>
    </tr>`)
  </script>

@endsection
