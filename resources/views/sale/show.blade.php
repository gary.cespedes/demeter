<x-row>
  <div class="container">

    <table class="table table-bordered">
      <tr>
        <td> <b>CODIGO</b> </td>
        <td>{{$sale->code}}</td>
      </tr>
      <tr>
        <td> <b>FECHA</b> </td>
        <td>{{\Carbon\Carbon::parse($sale->date_sale)->format('d-m-Y')}}</td>
      </tr>
      <tr>
        <td> <b>TOTAL INICIAL</b> </td>
        <td>{{$sale->price_initial}}</td>
      </tr>
      <tr>
        <td> <b>TOTAL FINAL</b> </td>
        <td>{{$sale->price_end}}</td>
      </tr>

    </table>
  </div>
</x-row>

<hr>

<table class="table" style="width: 100%">
  <thead >
  <th>{{__('label.code')}}</th>
  <th>{{__('label.product')}}</th>
  <th>{{__('label.description')}}</th>
  <th>{{__('label.price_unit')}}</th>
  <th>{{__('label.quantity')}}</th>
  <th>{{__('label.price_total')}}</th>
  </thead>
  <tbody>
  @foreach($detail_sale as $item)
    <tr>
      <td>{{$item->code}}</td>
      <td>{{$item->name}}</td>
      <td>{{$item->description}}</td>
      <td>{{$item->price_unit}}</td>
      <td>{{$item->quantity}}</td>
      <td>{{$item->price_total}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
