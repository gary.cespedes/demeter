@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title',__('common.title_pages.provider'))
{{-- page styles --}}
@section('vendor-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
  <script src="{{asset('js/scripts/datatables/datatable.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/animate/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/sweetalert2.min.css')}}">
@endsection
@section('page-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/validation/form-validation.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">

@endsection
@include('pages.app-chat-sidebar')
@section('content')

  <section class="input-validation" id="register_provider">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_provider')}}</x-slot>
          @include('provider.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <section class="input-validation" id="update_provider">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.edit_provider')}}</x-slot>
          @include('provider.edit')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_provider')}}</x-slot>
        @include('provider.list')
      </x-card>
    </x-col>
  </x-row>



@endsection

@section('vendor-scripts')

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset  ('js/scripts/forms/validation/form-validation.js')}}"></script>
  <script src="{{asset('js/scripts/forms/select/form-select2.js')}}"></script>
  <script src="{{asset('js/pages/provider/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/provider/main.js')}}" type="module"></script>
  <script>
    $('#thead_providers').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.email')}}</th>
    </tr>`)
  </script>
  <script src="{{asset('js/scripts/extensions/sweet-alerts.js')}}"></script>
  <script src="{{asset('js/scripts/extensions/toastr.js')}}"></script>

@endsection
