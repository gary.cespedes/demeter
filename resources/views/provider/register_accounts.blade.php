<x-row class="p-2" style="background-color: #EDEDED">
  <x-col sm="6" xs="6" md="6" lg="6">
    <x-form-group name="name_bank" type="text">
      <x-slot name="text_label">{{__('label.name_bank')}}</x-slot>
      <x-slot name="id">name_bank</x-slot>
      <x-slot name="placeholder">{{__('label.insert_name_bank')}}</x-slot>
    </x-form-group>
  </x-col>
  <x-col sm="6" xs="6" md="6" lg="6">
    <x-form-group name="number_account" type="text">
      <x-slot name="text_label">{{__('label.number_account')}}</x-slot>
      <x-slot name="id">number_account</x-slot>
      <x-slot name="placeholder">{{__('label.insert_number_account')}}</x-slot>
    </x-form-group>
  </x-col>
  <x-col sm="10" xs="10" md="10" lg="10">
    <label>{{__('label.type_account')}}</label>
    <div class="form-group">
      <select class="select2 form-control default-select" name="type_document" id="type_account_bank">
        @foreach($type_account_bank as $key => $item)
          <option value="{{$key}}">{{$item}}</option>
        @endforeach
      </select>
    </div>
  </x-col>
  <x-col sm="2" xs="2" md="2" lg="2" style="margin-top: 10px">
    <x-button type="button"  color="info btn btn-sm" id="addAccountInList">
      <i class="bx bx-plus-circle"></i>
    </x-button>
  </x-col>

</x-row>

<x-row>
  <div class="col s12 l12 m12 table-responsive">
    <table class="table highlight text-nowrap" id="table_attributes_account" >
      <thead>
      <tr>
        <th>{{__('label.bank')}}</th>
        <th>{{__('label.number')}}</th>
        <th>{{__('label.type_account')}}</th>
        <th>{{__('common.table.options')}}</th>
      </tr>
      </thead>
      <tbody id="attributesAccountListBody">

      </tbody>
    </table>
    <div class="input-field">
      <div class="error_table_attributes"></div>
    </div>
  </div>
</x-row>
