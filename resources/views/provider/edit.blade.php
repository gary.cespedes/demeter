<x-form method="put" id="form_provider_edit" file="true">
  <x-slot name="route">{{route('update_provider')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_provider')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>
      <x-form-group name="phone" type="number">
        <x-slot name="text_label">{{__('label.phone')}}</x-slot>
        <x-slot name="placeholder">{{__('label.phone1_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>
      <x-form-group name="email" type="email">
        <x-slot name="text_label">{{__('label.email_provider')}}</x-slot>
        <x-slot name="placeholder">{{__('label.email_placeholder')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
          data-validation-required-message="Must be a valid email"
        </x-slot>
      </x-form-group>

      <x-row>
        <x-col sm="12" xs="12" md="6" lg="6">
          <label>{{__('label.type_document')}}</label>
          <div class="form-group">
            <select class="select2 form-control default-select" name="type_document" >
              @foreach(\App\Http\Controllers\WebController::$type_document as $key => $item)
                <option value="{{$key}}">{{$item}}</option>
              @endforeach
            </select>
          </div>
        </x-col>
        <x-col sm="12" xs="12" md="6" lg="6">
          <x-form-group name="number_document" type="text">
            <x-slot name="text_label">{{__('label.number_document')}}</x-slot>
            <x-slot name="placeholder">{{__('label.number_document_placeholder')}}</x-slot>
            <x-slot name="rules">
              data-validation-required-message="This field is required" required
            </x-slot>
          </x-form-group>
        </x-col>
      </x-row>

    </x-col>

    <hr>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="warning" id="update">
        {{__('common.button.update')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel2">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
