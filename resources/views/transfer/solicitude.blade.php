@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title',__('common.title_pages.transfer'))
{{-- page styles --}}

@section('content')
  <section class="input-validation">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">Realizar Solicitud</x-slot>
        <x-row>
          <div class="col-12">
            <h6>Buscar Producto</h6>
            <div class="form-group">
              <select class="select2 form-control default-select" name="product_id"  id="product_entity_id">
              </select>
            </div>
            <div id="list_warehouses" style="display: none">
              <h6>Lista de Almacenes con Stock</h6>
              <div class="col-12">
                <x-table id="list_warehouses_with_stock" thead="thead_warehouse_with_stock">
                </x-table>
              </div>
            </div>
          </div>
        </x-row>

        <div id="register_transfer" style="display:none;" >
          <x-form method="post" id="form_transfer" file="true">
            <x-slot name="route">{{route('store_transfer')}}</x-slot>
            <x-row class="mt-2">
              <x-col sm="12" xs="12" md="6" lg="6">
                <x-select2 >
                  <x-slot name="title">
                    Origen
                  </x-slot>
                  <x-slot name="id">origin_id</x-slot>
                  <x-slot name="name">origin_id</x-slot>
                  <x-slot name="rules">
{{--                    disabled--}}
                  </x-slot>
                </x-select2>
              </x-col>
              <x-col sm="12" xs="12" md="6" lg="6">
                <x-select2 >
                  @slot('options', $origin)
                  <x-slot name="title">
                    Destino
                  </x-slot>
                  <x-slot name="id">destine_id</x-slot>
                  <x-slot name="name">destine_id</x-slot>
                  <x-slot name="rules">
{{--                    disabled--}}
                  </x-slot>
                </x-select2>
              </x-col>

              <x-col sm="12" xs="12" md="6" lg="6">
                <x-form-group name="note_transfer" type="text">
                  <x-slot name="text_label">{{__('label.note_transfer')}}</x-slot>
                  <x-slot name="placeholder">{{__('label.note_transfer')}}</x-slot>
                  <x-slot name="rules">
                    data-validation-required-message="This field is required" required
                  </x-slot>
                </x-form-group>
              </x-col>

              <x-col sm="12" xs="12" md="6" lg="6">
                  <x-row>
                    <x-col sm="12" xs="12" md="6" lg="6">
                      <x-form-group name="date_transfer" type="datetime-local">
                        <x-slot name="text_label">{{__('label.date_transfer')}}</x-slot>
                        <x-slot name="placeholder">{{__('label.date_transfer')}}</x-slot>
                        <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d\TH:i')}}</x-slot>
                        <x-slot name="rules">
                          data-validation-required-message="This field is required" required
                        </x-slot>
                      </x-form-group>
                    </x-col>
                    <x-col sm="12" xs="12" md="6" lg="6">
                      <label>Tiempo de Hora limite:</label>
                      <div class="form-group">
                        <select class="select2 form-control default-select" name="time_active" id="time_active">
                          @for($i = 1; $i <= 24; $i++ )
                            <option value="{{$i}}">{{$i}} </option>
                          @endfor
                        </select>
                      </div>
                    </x-col>
                  </x-row>
              </x-col>
            </x-row>

            <x-row>
              <x-col sm="12" xs="12" md="6" lg="6">
                <x-select2 >
                  <x-slot name="title">
                    Productos
                  </x-slot>
                  <x-slot name="id">product_id</x-slot>
                  <x-slot name="name">product_id</x-slot>
                </x-select2>
              </x-col>

              <x-col  sm="12" xs="12" md="4" lg="4">
                <div style="margin-top: 20px">
                  <button id="addAttributeInList" class="btn btn-icon btn-info glow mr-1">
                    <i class="bx bx-send"></i>
                  </button>
                </div>
              </x-col>
            </x-row>

            @include('transfer.list_prices')

            <x-row>
              <div class="col-12 d-flex justify-content-end">
                <x-button type="submit" color="primary" id="create">
                  {{__('common.button.register')}}
                </x-button>
                <x-button type="reset" color="danger" id="cancel">
                  {{__('common.button.cancel')}}
                </x-button>
              </div>
            </x-row>
          </x-form>
        </div>

      </x-card>
    </x-col>
    </x-row>
  </section>

    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card>
          <x-slot name="title">{{__('common.table_title.list_transfer_solicitude')}}</x-slot>
          <x-table id="list_transfers" thead="thead_transfers">
          </x-table>
        </x-card>
      </x-col>
    </x-row>

  <x-modal title="Detalle de Solicitud" id="transfer_detail">
    <div id="container_transfer_detail"></div>
  </x-modal>

@endsection

@section('page-scripts')
  <script src="{{asset('js/pages/solicitude/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/solicitude/main.js')}}" type="module"></script>
  <script>
    $('#thead_transfers').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.date')}}</th>
    </tr>`)

    $('#thead_warehouse_with_stock').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.name')}}</th>
        <th>{{__('common.table.stock')}}</th>
    </tr>`)
  </script>

@endsection
