@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title',__('common.title_pages.transfer'))
{{-- page styles --}}
@section('content')
  <section class="input-validation" id="register_transfer">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_transfer')}}</x-slot>
          @include('transfer.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_transfer')}}</x-slot>
        @include('transfer.list')
      </x-card>
    </x-col>
  </x-row>


  <x-modal title="Detalle de Transferencia" id="transfer_detail">
    <div id="container_transfer_detail"></div>
  </x-modal>

@endsection
@section('page-scripts')
  <script src="{{asset('js/pages/transfer/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/transfer/main.js')}}" type="module"></script>
  <script>
    $('#thead_transfers').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.date')}}</th>
    </tr>`)
  </script>

@endsection
