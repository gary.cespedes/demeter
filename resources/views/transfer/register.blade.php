<x-form method="post" id="form_transfer" file="true">
  <x-slot name="route">{{route('store_transfer')}}</x-slot>
  <x-row class="mt-2">
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $branch)
        <x-slot name="title">
          {{__('label.origin')}}
        </x-slot>
        <x-slot name="id">origin_id</x-slot>
        <x-slot name="name">branch_origin_id</x-slot>
      </x-select2>
    </x-col>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $branch)
        <x-slot name="title">
          {{__('lable.destine')}}
        </x-slot>
        <x-slot name="id">destine_id</x-slot>
        <x-slot name="name">branch_destine_id</x-slot>
      </x-select2>
    </x-col>


    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="note_transfer" type="text">
        <x-slot name="text_label">{{__('label.note_transfer')}}</x-slot>
        <x-slot name="placeholder">{{__('label.note_transfer')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-row>
        <x-col sm="12" xs="12" md="12" lg="12">
          <x-form-group name="date_transfer" type="datetime-local">
            <x-slot name="text_label">{{__('label.date')}}</x-slot>
            <x-slot name="placeholder">{{__('label.date')}}</x-slot>
            <x-slot name="value">{{\Carbon\Carbon::now()->format('Y-m-d\TH:i')}}</x-slot>
            <x-slot name="rules">
              data-validation-required-message="This field is required" required
            </x-slot>
          </x-form-group>
        </x-col>
      </x-row>
    </x-col>
  </x-row>

  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $products)
        <x-slot name="title">
          {{__('label.products')}}
        </x-slot>
        <x-slot name="id">product_id</x-slot>
        <x-slot name="name">product_id</x-slot>
      </x-select2>
    </x-col>

    <x-col  sm="12" xs="12" md="4" lg="4">
      <div style="margin-top: 20px">
        <button id="open_new_windows" class="btn btn-icon btn-primary glow mr-1">
          <i class="bx bx-arrow-to-top"></i>
        </button>
        <button id="addAttributeInList" class="btn btn-icon btn-info glow mr-1">
          <i class="bx bx-send"></i>
        </button>
      </div>
    </x-col>
  </x-row>

  @include('transfer.list_prices')

  <x-row>
    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>

</x-form>
