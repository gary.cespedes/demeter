<x-row>
  <div class="container">
  <table class="table table-bordered">
    <tr>
      <td> <b>Codigo</b> </td>
      <td>{{$transfer->code}}</td>
    </tr>
    <tr>
      <td> <b>Fecha Transferencia</b> </td>
      <td>{{\Carbon\Carbon::parse($transfer->date_transfer)->format('d-m-Y')}}</td>
    </tr>
    <tr>
      <td> <b>Origen</b> </td>
      <td>{{$transfer->origin_name}}</td>
    </tr>
    <tr>
      <td> <b>Destino</b> </td>
      <td>{{$transfer->destine_name}}</td>
    </tr>

  </table>

  </div>
</x-row>

<hr>
<h3>Lista de Productos</h3>
<table class="table" id="show_prices"  style="width: 100%">
  <thead >
  <th>{{__('label.code')}}</th>
  <th>{{__('label.product')}}</th>
  <th>{{__('label.quantity')}}</th>
  </thead>

  <tbody>
    @foreach($details_transfer as $item)
      <tr >
        <td>{{$item->code}}</td>
        <td>{{$item->product}}</td>
        <td>{{$item->quantity}}</td>
      </tr>
    @endforeach
  </tbody>
</table>
