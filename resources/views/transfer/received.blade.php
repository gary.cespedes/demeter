@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title',__('common.title_pages.transfer'))
{{-- page styles --}}
@section('vendor-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/forms/select/select2.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/tables/datatable/datatables.min.css')}}">
  <script src="{{asset('js/scripts/datatables/datatable.js')}}"></script>
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/animate/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/sweetalert2.min.css')}}">
@endsection
@section('page-styles')
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/forms/validation/form-validation.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/plugins/extensions/toastr.css')}}">
@endsection
@include('pages.app-chat-sidebar')

@section('content')

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_transfer_received')}}</x-slot>
        <x-table id="list_transfers" thead="thead_transfers">
        </x-table>
      </x-card>
    </x-col>
  </x-row>


  <x-modal title="Detalle de Transferencia" id="transfer_detail">
    <div id="container_transfer_detail"></div>
  </x-modal>

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset('js/pages/transfer/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/received/main.js')}}" type="module"></script>
  <script>
    $('#thead_transfers').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.date')}}</th>
    </tr>`)
  </script>

@endsection
