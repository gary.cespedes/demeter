<x-form method="post" id="form_batch" file="true">
  <x-slot name="route">{{route('store_batch')}}</x-slot>
  <x-row class="mt-2">
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $providers)
        <x-slot name="title">
          {{__('label.provider')}} <a class="right text-primary" data-toggle="modal" data-target="#provider_register"><i class="bx bx-plus-medical"></i>{{__('label.add_more_item')}}</a>
        </x-slot>
        <x-slot name="id">provider_id</x-slot>
        <x-slot name="name">provider_id</x-slot>
      </x-select2>
    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-form-group name="note_batch" type="text">
        <x-slot name="text_label">{{__('label.note_batch')}}</x-slot>
        <x-slot name="placeholder">{{__('label.note_batch')}}</x-slot>
      </x-form-group>
    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $branch)
        <x-slot name="title">
          {{__('label.branch')}}
        </x-slot>
        <x-slot name="id">branch_id</x-slot>
        <x-slot name="name">branch_id</x-slot>
      </x-select2>
    </x-col>

{{--    <x-col sm="12" xs="12" md="6" lg="6">--}}
{{--      <x-form-group name="date_into" type="date">--}}
{{--        <x-slot name="text_label">{{__('label.date_into')}}</x-slot>--}}
{{--        <x-slot name="placeholder">{{__('label.date_into')}}</x-slot>--}}
{{--        <x-slot name="rules">--}}
{{--          data-validation-required-message="This field is required" required--}}
{{--        </x-slot>--}}
{{--      </x-form-group>--}}
{{--    </x-col>--}}
  </x-row>

  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">
      <x-select2 >
        @slot('options', $products)
        <x-slot name="title">
          {{__('label.products')}} <a class="right text-primary" data-toggle="modal" data-target="#product_register"><i class="bx bx-plus-medical"></i>{{__('label.add_more_item')}}</a>
        </x-slot>
        <x-slot name="id">product_id</x-slot>
        <x-slot name="name">product_id</x-slot>
      </x-select2>
    </x-col>

    <x-col  sm="12" xs="12" md="4" lg="4">
      <div style="margin-top: 20px">
        <button id="addAttributeInList" class="btn btn-icon btn-info glow mr-1">
          <i class="bx bx-send"></i>
        </button>
      </div>
    </x-col>
  </x-row>

  @include('batch.list_prices')

  <x-row>
    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="primary" id="create">
        {{__('common.button.register')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>

</x-form>
