@extends('layouts.contentLayoutMaster')

@section('title',__('common.title_pages.batch'))

@section('content')
  <section class="input-validation" id="register_income">
    <x-row>
      <x-col xs="12" sm="12" md="12" lg="12">
        <x-card >
          <x-slot name="title">{{__('common.section_title.register_income')}}</x-slot>
          @include('batch.register')
        </x-card>
      </x-col>
    </x-row>
  </section>

  <x-row>
    <x-col xs="12" sm="12" md="12" lg="12">
      <x-card>
        <x-slot name="title">{{__('common.table_title.list_income')}}</x-slot>
        @include('batch.list')
      </x-card>
    </x-col>
  </x-row>

  <x-modal title="Registro Proveedor" id="provider_register">
    @include('provider.register')
  </x-modal>

  <x-modal title="Registro Producto" id="product_register">
    @include('product.register')
  </x-modal>

  <x-modal title="Registro Familia" id="category_register">
    @include('category.register')
  </x-modal>

  <x-modal title="Detalle de Lote" id="lot_detail">
    <div id="container_lot_detail"></div>
  </x-modal>

@endsection

{{-- page scripts --}}
@section('page-scripts')
  <script src="{{asset('js/pages/batch/crud.js')}}" type="module"></script>
  <script src="{{asset('js/pages/batch/main.js')}}" type="module"></script>
  <script>
    $('#thead_incomes').html(`<tr>
        <th>{{__('common.table.options')}}</th>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.date')}}</th>
    </tr>`)
  </script>

@endsection
