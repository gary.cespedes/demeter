<x-form method="put" id="form_product_edit" file="true">
  <x-slot name="route">{{route('update_product')}}</x-slot>
  <x-row>
    <x-col sm="12" xs="12" md="6" lg="6">

      <x-select2 >
        @slot('options', $categories)
        <x-slot name="title">Categorias</x-slot>
        <x-slot name="name">category_id</x-slot>
      </x-select2>
      <x-form-group name="code" type="text">
        <x-slot name="text_label">{{__('label.code')}}</x-slot>
        <x-slot name="placeholder">{{__('label.code')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>



    </x-col>

    <x-col sm="12" xs="12" md="6" lg="6">

      <x-form-group name="name" type="text">
        <x-slot name="text_label">{{__('label.name_product')}}</x-slot>
        <x-slot name="placeholder">{{__('label.name_product')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>
      <x-form-group name="description" type="text">
        <x-slot name="text_label">{{__('label.description')}}</x-slot>
        <x-slot name="placeholder">{{__('label.description')}}</x-slot>
        <x-slot name="rules">
          data-validation-required-message="This field is required" required
        </x-slot>
      </x-form-group>

    </x-col>

    <hr>

    <div class="col-12 d-flex justify-content-end">
      <x-button type="submit" color="warning" id="update">
        {{__('common.button.update')}}
      </x-button>
      <x-button type="reset" color="danger" id="cancel2">
        {{__('common.button.cancel')}}
      </x-button>
    </div>

  </x-row>
</x-form>
