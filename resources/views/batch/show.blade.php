<x-row>
  <div class="container">
    <table class="table table-bordered">
      <tr>
        <td> <b>Codigo</b> </td>
        <td>{{$batch->code}}</td>
      </tr>
      <tr>
        <td> <b>Fecha Entrada</b> </td>
        <td>{{\Carbon\Carbon::parse($batch->date_into)->format('d-m-Y')}}</td>
      </tr>
      <tr>
        <td> <b>Proveedor</b> </td>
        <td>{{$batch->provider}} (+{{$batch->phone}})</td>
      </tr>
      <tr>
        <td> <b>Destino</b> </td>
        <td>{{$batch->branch}}</td>
      </tr>

    </table>

  </div>
</x-row>

<x-row>
  <div class="col-12">
    <h4>Generar Etiquetas por categoria</h4>
  </div>
  <div class="mx-auto">
    <label>{{__('label.category')}}</label>
    <div class="controls">
      <select class="select2 form-control default-select" name="category_batch[]" id="category_batch" multiple="multiple">
        <option value="0"> -- Ninguno --</option>
        @foreach($categories as $item)
          <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
      </select>
    </div>
    <button class="btn btn-dark mt-2" id="add_items_for_category">Seleccionar</button>
  </div>
</x-row>

<hr>

<div class="row d-block" style="background-color: red">
  <div class="float-md-right">
    <div id="loader" style="display: none">
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    </div>

    <button class="btn btn-info" id="generate_tag" style="display: none" >
      Generar Etiquetas <span id="count_generated"></span>
    </button>
  </div>
</div>

<div class="container">
  <h3>Lista de Productos</h3>

  <table class="table" id="show_products_in_batch"  style="width: 100%" >
    <thead >
    <th>{{__('common.table.options')}}</th>
    <th>{{__('label.code')}}</th>
    <th>{{__('label.product')}}</th>
    <th>{{__('label.price')}}</th>
    <th>{{__('label.cost')}}</th>
    <th>{{__('label.quantity')}}</th>
    <th>{{__('label.total')}}</th>
    </thead>

    <tbody>
    @foreach($batch_detail as $item)
      <tr>
        <td>
          <input type="checkbox" class="checkbox-input" data-quantity="{{$item->quantity}}"  data-id="{{$item->id}}" title="Imprimir Etiqueta" onchange="check_item_in_list(this, `{{$item->product_id}}`)">
        </td>
        <td>{{$item->code}}</td>
        <td>{{$item->product}}</td>
        <td>{{$item->price}}</td>
        <td>{{$item->cost}}</td>
        <td>{{$item->quantity}}</td>
        <td>{{$item->total}}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
