<x-row>
  <div class="col s12 l12 m12 table-responsive">
    <table class=" table highlight" id="table_attributes" >
      <thead>
      <tr>
        <th>{{__('common.table.code')}}</th>
        <th>{{__('common.table.product')}}</th>
        <th>{{__('common.table.quantity')}}</th>
        <th>{{__('common.table.cost')}}</th>
        <th>{{__('common.table.total')}}</th>
        <th>{{__('common.table.options')}}</th>
      </tr>
      </thead>
      <tbody id="attributesListBody">

      </tbody>
    </table>

  </div>
</x-row>
