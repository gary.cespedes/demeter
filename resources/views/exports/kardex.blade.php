<style>
  body{
    background-color: white !important;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
  }
  .table{
    border-collapse: collapse;
  }

</style>

<div style="background-color: #ffffff; padding: 10px;width: 100% ">
  <label for="period">Periodo: </label><span> {{$filters->date_start}} - {{$filters->date_end}}</span> <br>
  <label for="period">Tienda: </label><span> {{$filters->warehouse->name}}</span> <br>
  <label for="period">Producto: </label><span> {{$filters->product->name}}</span> <br>

  <table class="table">
      <thead>
        <tr>
          <th>Nº</th>
          <th>FECHA</th>
          <th>DESCRIPCION</th>
          <th>CODIGO</th>
          <th>Q. ENTRANTE</th>
          <th>Q. SALIENTE</th>
          <th>P. UNITARIO</th>
          <th>I. ENTRANTE</th>
          <th>I. SALIENTE</th>
        </tr>
      </thead>

    <tbody>
      @forelse($data_export as $item)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$item->created_at}}</td>
          <td>{{$item->description}}</td>
          <td>{{$item->code_operation}}</td>
          <td>{{$item->quantity_input}}</td>
          <td>{{$item->quantity_out}}</td>
          <td>{{$item->price_unit}}</td>
          <td>{{$item->income_input}}</td>
          <td>{{$item->income_out}}</td>
        </tr>
        @empty
          No existe datos
        @endforelse
    </tbody>

  </table>
</div>
