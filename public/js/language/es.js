var common  = {
    validation: {
        name: 'Nombre',
        email: 'Correo Electronico',
        password: 'Contraseña',
        confirm_password: 'Confirmar Contraseña',
        five_characters: 'Ingrese 5 Caracteres como minimo',
    },
    required : {
        name : 'El nombre es obligatorio',
        min_length : 'caracteres requeridos',
    },
    success: 'OPERACION REALIZADA SATISFACTORIAMENTE',
    error: 'SUCEDIO UN ERROR',
    a: {
        title : {
            show : 'VER INFORMACION'
        }
    },
};


