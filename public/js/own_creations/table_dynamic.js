function create_table_dynamic(id_table, id_body_table, callback) {
    let table_body = document.getElementById(id_body_table);
    let columns = $(`#${id_table} th`).length;
    let row = table_body.insertRow(0);
    callback(create_cells_in_table(row, columns), id_table)
}

function create_cells_in_table(row, quantity) {
    let cells = [];
    for (let i = 0; i < quantity; i++) {
        let cell = row.insertCell(i);
        cells.push(cell);
    }
    return cells;
}

function update_quantity(input){
    console.log('update_quantity')
    let index = $(input).data('index')
    attributeListArray[index].quantity = parseInt(input.value)
    let total = attributeListArray[index].quantity * attributeListArray[index].cost
    attributeListArray[index].total = total
    $(`#${attributeListArray[index].id}`).text(total)
}

function update_node(input){
  console.log('update_note')

  let index = $(input).data('index')
  attributeListArray[index].quantity = parseInt(input.value)
}

function update_quantity_transfer(input){
    console.log('update_quantity_transfer')
    let index = $(input).data('index')
    attributeListArray[index].quantity = parseInt(input.value)
}

function update_cost(input){
  console.log('update_cost')
  let index = $(input).data('index')
  attributeListArray[index].cost = parseInt(input.value)
  let total = attributeListArray[index].quantity * attributeListArray[index].cost
  attributeListArray[index].total = total
  $(`#${attributeListArray[index].id}`).text(total)
}
