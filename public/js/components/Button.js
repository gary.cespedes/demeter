class Button {
    constructor(data){
        this.data = data;
    }

    initialize(type){
        let return_button;
        let button_delete = `<button id="delete" class="btn btn-icon btn-danger glow mr-1" data-id = "${this.data}"> <i class="bx bx-trash"></i></button>`;
        let button_edit = `<button id="edit" class="btn btn-icon btn-warning glow mr-1" data-id = "${this.data}"> <i class="bx bx-edit"></i></button>`;
        let button_show = `<button id="show" class="btn btn-icon btn-info glow mr-1" data-id = "${this.data}"> <i class="bx bx-show"></i></button>`;
        let button_config = `<button id="config" class="btn btn-icon btn-dark glow mr-1" data-id = "${this.data}"> <i class="bx bx-cog"></i></button>`;
        switch (type) {
            case 'delete':
                return_button = button_delete;
                break;
            case 'update':
                return_button = button_edit;
                break;
            case 'show':
                return_button = button_show;
                break;
            case 'config':
                return_button = button_config;
                break;
            case 'all':
                return_button = `${button_edit} ${button_delete} ${button_show}`
                break;
            case 'default':
                return_button = ` ${button_edit} ${button_delete} `;
                break;
        }
        return return_button;
    }
}
