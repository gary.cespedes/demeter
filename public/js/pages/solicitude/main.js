import { Table_Component } from "../../components/Table.js";
import {View} from "../../components/View.js";


function formatRepoSelection (repo) {

  if (repo.text !== "Searching"){
    $('#list_warehouses').show();
    table_warehouse_instance.data = {id: parseInt(repo.id)}
    table_warehouse_instance.initialize(table_warehouse_instance.column_active).catch(error => {
      console.log(error)
    });

  }

  return repo.full_name || repo.text;
}

$(document).ready(function () {
    table_warehouse_instance = new Table_Component('list_warehouses_with_stock', routes.get_warehouse_with_stock, null, columns_active_warehouse_with_stock, null, table_warehouse);
    table_warehouse_instance.initialize(table_warehouse_instance.column_active).catch(error => {
      console.log(error)
    });


    table_transfer_instance = new Table_Component('list_transfers', routes.get_transfers_for_status, {status: 0}, columns_active_transfer_solicitude, null, table_transfer);
    table_transfer_instance.initialize(table_transfer_instance.column_active).catch(error => {
      console.log(error)
    });

  $("#product_entity_id").select2({
    width: '100%',
    ajax: {
      type: 'POST',
      url: routes.get_products,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term,
        };
      },
      processResults: function (data, params) {
        return {
          results: data.data,
        };
      },
      cache: true
    },
    templateResult : formatResult,
    templateSelection : formatRepoSelection,
    escapeMarkup: function(m) {
      // Do not escape HTML in the select options text
      return m;
    },
    placeholder: 'Buscar Producto',
  });


});

