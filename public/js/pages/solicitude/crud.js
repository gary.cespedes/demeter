import {Crud} from "../modules/crud.js";
import {messages_user, rules_user} from "./rules.js";
import { View } from "../../components/View.js";

let crud = new Crud('form_transfer', null, null);

$('#form_transfer').submit(function (e) {
    e.preventDefault();
    let form_data = new FormData($(`#form_transfer`)[0]);
    form_data.append('attributeListArray', JSON.stringify(attributeListArray))
    form_data.append('status', 0)
    crud.form = 'form_transfer'
    crud.route = routes.transfer_store;
    crud.data = serialize_array('form_transfer', form_data)
    crud.create_files(table_transfer_instance, () => {
      attributeListArray = [];
      $('#attributesListBody').html('');
    });
});

$('#list_transfers').on('click','#show',function () {
  let id = $(this).data('id');
  $("#transfer_detail").modal("show");
  load_view_show_transfer = new View(routes.show_transfer,
    'container_transfer_detail', {id}, null, null)
  load_view_show_transfer.load_view('register')
});

$('#list_warehouses_with_stock').on('click','#to_solicited',function () {
  let id = $(this).data('id');
  $('#register_transfer').show()
  $.post(routes.get_warehouse, {id}, (data) => {
    let data_ajax = [data.data]
    $(`#origin_id`).html('').select2()
    $('#origin_id').select2({
      data:data_ajax
    })
    $.post(routes.change_origin, {id},  (data) => {
      $(`#product_id`).html('').select2()
      $('#product_id').select2({
        data:data.data
      })
      attributeListArray = []
      $('#attributesListBody').html('');
      console.log(data.data);
    })

  })

});

$('#list_transfers').on('click','#accept_transfer',function () {
  let id = $(this).data('id');
  Swal.fire({
    title: 'Estas Seguro?',
    text: "Tu confirmaras la solicitud de transferencia!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Confirmar!',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    buttonsStyling: false,
  }).then(function (willDelete) {
    if (willDelete.value) {
      $.post(routes.change_status_transfer, {id, status: 1}, (data)=>{
        (table_transfer_instance !== null) ? table_transfer_instance.initialize(table_transfer_instance.column_active) : '';
        toastr.success('', common.success);
      })
    }
  });
});

$('#list_transfers').on('click','#transfer_print',function () {
  let id = $(this).data('id');
  window.open(`${routes.print_transfer}?id=${id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#list_transfers').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.form = 'form_transfer'
    crud.route = routes.transfer_delete;
    crud.delete(table_transfer_instance, id);
});

$('#addAttributeInList').on('click', function (e) {
  e.preventDefault()
  if ($('#product_id').val() === ""){
    swal({
      title: "Error de Envio",
      text: "No Seleecciono Ningun Atributo",
      icon: 'error',
    })
  }
  else{
    $.post(routes.get_detail_warehouse, {id : $('#destine_id').val(), product_id: $('#product_id').val()}, (data) => {
      console.log(data)
      let element = {
        id: data.data.id,
        code: data.data.product_code,
        name: data.data.product_name,
        stock: data.data.stock,
        product_id: data.data.product_id,
        quantity: 1,
      }
      attributeListArray.push(element);
      create_table_dynamic('table_attributes', 'attributesListBody', function (arrayCells, id_table) {

        let last_index = attributeListArray.length - 1;

        let text_name = document.createTextNode(element.name);
        let text_code = document.createTextNode(element.code);
        let text_stock = document.createTextNode(element.stock);

        let input_quantity = create_input('text', element.quantity, 'index', parseInt(last_index.toString()), 'Onkeyup', "update_quantity_transfer(this);")

        let i = document.createElement('i')
        i.className = 'bx bx-trash danger';

        let button = document.createElement("button")
        button.className = 'mb-6 btn item_center waves-effect waves-light gradient-45deg-purple-deep-orange gradient-shadow'
        button.setAttribute('data-index', parseInt(last_index))
        button.appendChild(i)
        button.onclick = function ()
        {
          let fila = this.parentNode.parentNode;
          let table = document.getElementById(id_table)
          let tbody = table.getElementsByTagName("tbody")[0];
          tbody.removeChild(fila);
          attributeListArray.splice($(this).data('index'), 1)
          console.log(attributeListArray)
        }

        arrayCells[0].appendChild(text_code);
        arrayCells[1].appendChild(text_name);
        arrayCells[2].appendChild(text_stock);
        arrayCells[3].appendChild(input_quantity)
        arrayCells[4].appendChild(button)
      })
    })

  }
})




