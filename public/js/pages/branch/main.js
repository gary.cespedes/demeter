import { Table_Component } from "../../components/Table.js";
import { View } from "../../components/View.js";

$(document).ready(function () {
    table_branch_instance = new Table_Component('list_branches', routes.get_branches, null, columns_active_branch, null, table_branch);
    table_branch_instance.initialize(table_branch_instance.column_active).catch(error => {
      console.log(error)
    });

});

