import {Crud} from "../modules/crud.js";
// import {messages_warehouse, rules_warehouse} from "./rules.js";

let crud_branch = new Crud('form_branch_register', null, null);
let id;

$('#form_branch_register').submit( (e) => {
    e.preventDefault();
    $('#form_branch_register').find('button[type=submit]').prop('disabled', true)
    crud_branch.form = 'form_branch_register'
    crud_branch.route = routes.branch_store
    crud_branch.create(table_branch_instance)
    console.log($('#form_branch_register').serialize());
});

$('#form_branch_edit').submit(function (e) {
    e.preventDefault();
    $('#form_branch_edit').find('button[type=submit]').prop('disabled', true)
    crud_branch.form ='form_branch_edit'
    crud_branch.route = routes.update_branch;
    crud_branch.edit(table_branch_instance, id, function () {
      $('#update_branch').hide()
      $('#register_branch').show()
    });
    console.log($('#form_member_update').serialize());
});

$('#list_branches').on('click','#edit',function () {
    id = $(this).data('id')

    $.post(routes.get_branchs, {id: id}, function (data) {
      $('#register_branch').hide();
      $('#update_branch').show();
      $('#form_branch_edit input[name="name"] ').val(data.data.name)
      $('#form_branch_edit input[name="email"] ').val(data.data.email)
      $('#form_branch_edit input[name="address"] ').val(data.data.address)
      $('#form_branch_edit input[name="phone"] ').val(data.data.phone)
      $('#form_branch_edit input[name="phone_alternative"] ').val(data.data.phone_alternative)
    })


});

$('#list_branches').on('click','#delete',function () {
    let id = $(this).data('id');
    crud_branch.route = routes.branch_delete;
    crud_branch.delete(table_branch_instance, id);
});

