import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {
    $('#update_client').hide()
    table_client_instance = new Table_Component('list_clients', routes.get_clients, null, columns_active_client, null, table_client);
    table_client_instance.initialize(table_client_instance.column_active).catch(error => {
      console.log(error)
    });

});

