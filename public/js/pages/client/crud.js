import {Crud} from "../modules/crud.js";
import {messages_client, rules_client} from "./rules.js";

let crud_client = new Crud('form_client_register', null, null);


$('#form_client_register').submit( (e) => {
    e.preventDefault();
    $('#form_client_register').find('button[type=submit]').prop('disabled', true)
    crud_client.form = 'form_client_register'
    crud_client.route = routes.client_store
    crud_client.create(table_client_instance)
    console.log($('#form_client_register').serialize());
});

$('#form_client_edit').submit(function (e) {
    e.preventDefault();
    $('#form_client_edit').find('button[type=submit]').prop('disabled', true)
    crud_client.form ='form_client_edit'
    crud_client.route = routes.update_client;
    crud_client.edit(table_client_instance, id, function () {
      $('#update_client').hide()
      $('#register_client').show()
    });
    console.log($('#form_member_update').serialize());
});

$('#list_clients').on('click','#edit',function () {
    id = $(this).data('id')

   $.post(routes.get_client, {id: id}, function (data) {
      $('#register_client').hide();
      $('#update_client').show();
      $('#form_client_edit input[name="name"] ').val(data.data.name)
      $('#form_client_edit input[name="last_name"] ').val(data.data.last_name)
      $('#form_client_edit input[name="firm_name"] ').val(data.data.firm_name)
      $('#form_client_edit input[name="tax_name"] ').val(data.data.tax_number)
      $('#form_client_edit input[name="tax_number"] ').val(data.data.tax_number)
      $('#form_client_edit input[name="city"] ').val(data.data.city)
      $('#form_client_edit input[name="email"]').val(data.data.email)
      $('#form_client_edit input[name="phone"] ').val(data.data.phone)
      $('#form_client_edit input[name="phone_alternative"] ').val(data.data.phone_alternative)
      $('#form_client_edit input[name="address"]').val(data.data.address)
      $('#form_client_edit select[name="document"] ').val(data.data.document)
      $('#form_client_edit input[name="number_document"] ').val(data.data.number_document)
      console.log(data)
    })


});

$('#list_clients').on('click','#delete',function () {
    let id = $(this).data('id');
    crud_client.route = routes.client_delete;
    crud_client.delete(table_client_instance, id);
});

$('#form_client_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_client').show();
  $('#update_client').hide();
});


