import { View } from "../../components/View.js";


$('#list_sales').on('click','#print',function () {
  let id = $(this).data('id');
  window.open(`${routes.print_sale}?id=${id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#print_all').click(function () {
  window.open(`${routes.print_all_sale}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#close_box').click(function () {
    window.open(`${routes.print_all_sale}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#list_sales').on('click','#show',function () {
  let id = $(this).data('id');
  $("#info_sale").modal("show");
  load_view_sale = new View(routes.show_sale,
    'container_info_sale', {id}, null, null)
  load_view_sale.load_view('register')
});
