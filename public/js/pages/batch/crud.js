import {Crud} from "../modules/crud.js";
import { View } from "../../components/View.js";

let crud = new Crud('form_batch', null, null);
let crud_category = new Crud('form_category', null, null);
let table = null

$('#form_batch').submit(function (e) {
    e.preventDefault();
    crud.form = 'form_batch'
    crud.route = routes.batch_store;
    crud.data = {
      data: $('#form_batch').serialize(),
      attributeListArray,
    }
    crud.create(table_income_instance, () => {
      attributeListArray = [];
      $('#attributesListBody').html('');
    });
});

$('body').on('change', '#category_batch', () => {
  if ($('#category_batch').val()[0] == 0){
    $('#category_batch').val(null).trigger('change');
  }
})



$('body').on('click', '#add_items_for_category', (e) => {
  e.preventDefault();
  array_tags = []
  let category_id = $('#category_batch').val()
  let count = 0;
  if (category_id[0] != 0){
    category_id.forEach( (value) => {
        $('#loader').show()
        $('#generate_tag').hide()
        $.post(routes.get_batch_for_category, {category_id:  value}, (data) => {
          console.log(data)
          data.data.forEach((element) => {
            array_tags.push(element)
          })

          count ++;
          verify_count(category_id.length, count, ()=> {
            $('#count_generated').text(`(${array_tags.length})`)
            $('#loader').hide()
            $('#generate_tag').show()
          })
        })
    })
  }
  else{
    $('#category_batch').val(null).trigger('change');
  }
})

$('#list_incomes').on('click','#show',function () {
  let id = $(this).data('id');
  $("#lot_detail").modal("show");

  load_view_show_income = new View(routes.show_batch,
    'container_lot_detail', {id}, null, null)
  load_view_show_income.load_view('register', () => {
    array_tags = [];
    $('#category_batch').select2({
      dropdownParent: $('#lot_detail')
    });
    let table = $('#show_products_in_batch').DataTable();
    // $.each(table.rows().data(), function(key, value) {
    //   $.post(routes.get_product, {code: value[1]}, (data) => {
    //     data.data.price = value[3];
    //     data.data.quantity = value[5];
    //     array_tags.push(data.data)
    //   })
    // });

  })
});



$('body').on('click', '#generate_tag', (e) => {
  e.preventDefault();
  let window_tag = window.open(`${routes.print_tags}` , "ETIQUETAS" , "width=700,height=700,scrollbars=NO")
  localStorage.clear();
  console.log(array_tags)
  window.localStorage.setItem('tags', JSON.stringify(array_tags));
})

$('body').on('change', '#select_all', (e) => {
  e.preventDefault();
})

$('#form_provider').submit(function (e) {
  e.preventDefault();
  e.preventDefault();
  let form_data = new FormData($(`#form_provider`)[0]);
  form_data.append('attributeListArray', JSON.stringify(attributeListArray))
  crud.data = serialize_array('form_provider', form_data)
  crud.route = routes.provider_store;
  crud.create_files(null,  (data) => {
    attributeListArray = [];
    $('#attributesListBody').html('');
    $("#provider_id").append($("<option selected></option>")
      .attr("value", data.data.id)
      .text(data.data.name));
    $('#provider_register').modal('toggle');
  });
});

$('#form_product').submit(function (e) {
  e.preventDefault();
  let form_data = new FormData($(`#form_product`)[0]);
  form_data.append('attributeListArray', JSON.stringify(attributeListArray))
  crud.data = serialize_array('form_product', form_data)
  crud.route = routes.product_store;
  crud.create_files(null, (data) => {
    attributeListArray = [];
    $('#attributesListBody').html('');
    $("#product_id").append($("<option selected></option>")
      .attr("value", data.data.id)
      .text(data.data.name));
    $('#product_register').modal('toggle');

  });
});

$('#form_category').submit(function (e) {
  e.preventDefault();
  $(`#form_category button[type="submit"]`).prop('disabled', true)
  crud_category.route = routes.category_store;
  crud_category.create(null, (data) => {

    $("#category_id").append($("<option selected></option>")
      .attr("value", data.data.id)
      .text(data.data.name));

    $('#category_register').modal('toggle');

  });
});


$('#list_incomes').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_income, {id: id}, function (data) {
      $('#register_income').hide();
      $('#update_income').show();
      $('#form_income_edit input[name="name"] ').val(data.data.name)
      $('#form_income_edit input[name="description"] ').val(data.data.description)
      $('#form_income_edit input[name="code"] ').val(data.data.code)
      $('#form_income_edit select[name="category_id"]').val(data.data.category_id).trigger('change')
      console.log(data)
    })
});

$('#list_incomes').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.form = 'form_income'
    crud.route = routes.batch_delete;
    crud.delete(table_income_instance, id);
});

$('#addAttributeInList').on('click', function (e) {
  e.preventDefault()
  if ($('#product_id').val() === ""){
    swal({
      title: "Error de Envio",
      text: "No Seleecciono Ningun Atributo",
      icon: 'error',
    })
  }
  else{

    let element = null;
    let verify = false;

    let id = $('#product_id').val();
    $.post(routes.get_product, {id: id}, (data) => {
      $.each(attributeListArray, (key, value) => {
        if (id == value.id){
          value.quantity ++
          verify = true
          element = value
          let input = $('#attributesListBody').find(`input[data-index = ${key}]`)
          $(input[0]).val(value.quantity)
          change_quantity(input[0], key, value.quantity)
        }
      })

      if (!verify) {
        element = {
          id: data.data.id,
          code: data.data.code,
          name: data.data.name,
          quantity: 1,
          cost: 0,
          total: 0
        }
        attributeListArray.push(element);
        console.log(attributeListArray)
        create_table_dynamic('table_attributes', 'attributesListBody', function (arrayCells, id_table) {
        count++;

        let last_index = attributeListArray.length - 1;

        let text_code = document.createTextNode(element.code);

        let text_total = document.createElement('span');
        let value_total = document.createTextNode(element.total);
        text_total.setAttribute('id',element.id)
        text_total.appendChild(value_total)

        let text_name = document.createTextNode(element.name);
        let input_quantity = create_input('text', element.quantity, 'index', parseInt(last_index.toString()), 'Onkeyup', "update_quantity(this);")
        let input_cost = create_input('text', element.cost, 'index', parseInt(last_index.toString()), 'Onkeyup', "update_cost(this);")

        let i = document.createElement('i')
        i.className = 'bx bx-trash danger';

        let button = document.createElement("button")
        button.className = 'mb-6 btn item_center waves-effect waves-light gradient-45deg-purple-deep-orange gradient-shadow'
        button.setAttribute('data-index', parseInt(last_index))
        button.appendChild(i)
        button.onclick = function ()
        {
          let fila = this.parentNode.parentNode;
          let table = document.getElementById(id_table)
          let tbody = table.getElementsByTagName("tbody")[0];
          tbody.removeChild(fila);
          attributeListArray.splice($(this).data('index'), 1)
          console.log(attributeListArray)
        }

        arrayCells[0].appendChild(text_code);
        arrayCells[1].appendChild(text_name);
        arrayCells[2].appendChild(input_quantity);
        arrayCells[3].appendChild(input_cost)
        arrayCells[4].appendChild(text_total)
        arrayCells[5].appendChild(button)
      })

      }

    })

  }
})

$('#addProductsInList').on('click', function (e) {
  e.preventDefault()
  if ($('#price').val() === ""){
    swal({
      title: "Error de Envio",
      text: "No Seleecciono Ningun Atributo o campos vacios",
      icon: 'error',
    })
  }
  else{
    let element = {
      price: $('#price').val(),
      description: $('#type_prices_bigger :selected').text(),
      // is_predetermined: ($('#is_predetermined').is(':checked')) ? 1 : 0
    };

    // if (element.is_predetermined){
    //   for (let i = 0; i < attributeListArray.length; i++){
    //     $(`#table_attributes input[type="checkbox"]`).prop('checked', false)
    //     attributeListArray[i].is_predetermined = 0;
    //   }
    // }
    attributeListArray.push(element);

    create_table_dynamic('table_attributes_products', 'productsListBody', function (arrayCells, id_table) {

      let last_index = attributeListArray.length - 1;

      let text = document.createTextNode(element.description);
      let input = create_input('text', element.price, 'index', parseInt(last_index.toString()), 'onblur', "update_node(this);")

      // let input_check = create_check(element.is_predetermined, 'index',  parseInt(last_index.toString()), 'onClick', "update_check_price(this, 'table_attributes');")

      let i = document.createElement('i')
      i.className = 'bx bx-trash';


      let button = document.createElement("button")
      button.className = 'btn btn-danger btn-icon'
      button.setAttribute('data-index', parseInt(last_index))
      button.appendChild(i)
      button.onclick = function ()
      {
        let fila = this.parentNode.parentNode;
        let table = document.getElementById(id_table)
        let tbody = table.getElementsByTagName("tbody")[0];
        tbody.removeChild(fila);
        attributeListArray.splice($(this).data('index'), 1)
      }

      arrayCells[0].appendChild(text);
      arrayCells[1].appendChild(input);
      // arrayCells[2].appendChild(input_check)
      arrayCells[2].appendChild(button)
    })

  }
})


$('#addAccountInList').on('click', function (e) {
  e.preventDefault()
  if ($('#name_bank').val() === "" && $('#number_account').val() === ""){
    swal({
      title: "Error de Envio",
      text: "No Seleecciono Ningun Atributo",
      icon: 'error',
    })
  }
  else{
    let element = {
      name_bank: $('#name_bank').val(),
      number_account: $('#number_account').val(),
      type_account_bank: $('#type_account_bank :selected').text(),
      id_type_account_bank: $('#type_account_bank').val(),
    };
    attributeListArray.push(element);

    create_table_dynamic('table_attributes_account', 'attributesAccountListBody', function (arrayCells, id_table) {

      let last_index = attributeListArray.length - 1;

      let text = document.createTextNode(element.name_bank);
      let input = create_input('text', element.number_account, 'index', parseInt(last_index.toString()), 'onblur', "update_node(this);")
      let text_type_account = document.createTextNode(element.type_account_bank);
      let i = document.createElement('i')
      i.className = 'bx bx-trash';
      let button = document.createElement("button")
      button.className = 'btn btn-danger btn-icon'
      button.setAttribute('data-index', parseInt(last_index))
      button.appendChild(i)
      button.onclick = function ()
      {
        let fila = this.parentNode.parentNode;
        let table = document.getElementById(id_table)
        let tbody = table.getElementsByTagName("tbody")[0];
        tbody.removeChild(fila);
        attributeListArray.splice($(this).data('index'), 1)
      }

      arrayCells[0].appendChild(text);
      arrayCells[1].appendChild(input);
      arrayCells[2].appendChild(text_type_account)
      arrayCells[3].appendChild(button)
    })

  }
})
