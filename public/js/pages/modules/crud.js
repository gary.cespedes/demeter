export class Crud {
    constructor(form, route, data){
        this.form = form;
        this.route = route;
        this.data = data;
    }

    create(table, callback = null){
        console.log(this.data)
        this.data = this.render_data();
        this.send_data(table, 'POST', callback);
    }

    create_files(table, callback = null){
      this.data = this.render_data();
      this.send_data_with_files(table, 'POST', callback);
    }

    edit(table, id, callback = null){
        this.data = this.render_data() + `&id=${id}`;
        this.send_data(table,'PUT' ,callback);
    }

    render_data(){
        if (this.data == null){
            return $(`#${this.form}`).serialize();
        }
        else{
            return this.data;
        }
    }

    send_data(table, type = 'POST', callback = null){
        ajax_request(this.route, this.data, type).then(data => {
          console.log(data.data)
          if(data.data !== -1){
            (table !== null) ? table.initialize(table.column_active) : '';
            toastr.success('', common.success);
            if (type !== 'DELETE'){
              $(`#${this.form}`)[0].reset();
            }
            this.data = null;
            if (callback !== null) {
              callback(data);
            }
          }
              $(`#${this.form} button[type="submit"]`).prop('disabled', false)
        }).catch(error => {
            console.log(error)
            this.data = null;
            toastr.error('', common.error);
            $(`#${this.form} button[type="submit"]`).prop('disabled', false)
        })
    }
    send_data_with_files(table, type = 'POST', callback = null){
        ajax_request_form_data(this.route, this.data, type).then(data => {
              (table !== null) ? table.initialize(table.column_active) : '';
              console.log(data)

              toastr.success('', common.success);
              $(`#${this.form}`)[0].reset();
              this.data = null;
              if (callback !== null) {
                callback(data);
              }
              $(`#${this.form} button[type="submit"]`).prop('disabled', false)

        }).catch(error => {
            console.log(error)
            this.data = null;
            toastr.error('', common.error);
            $(`#${this.form} button[type="submit"]`).prop('disabled', false)
        })
    }

    delete(table, id, callback){
        this.data = {id: id};
        let obj = this;

      Swal.fire({
        title: 'Estas Seguro?',
        text: "Tu eliminaras este registro!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
      }).then(function (willDelete) {
            if (willDelete.value) {
                obj.send_data(table, 'DELETE', callback);
            }
        });
    }
}
