import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {
    $('#update_provider').hide()
    table_provider_instance = new Table_Component('list_providers', routes.get_providers, null, columns_active_provider, null, table_provider);
    table_provider_instance.initialize(table_provider_instance.column_active).catch(error => {
      console.log(error)
    });

});

