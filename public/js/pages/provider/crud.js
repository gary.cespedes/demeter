import {Crud} from "../modules/crud.js";
import {messages_provider, rules_provider} from "./rules.js";

let crud_provider = new Crud('form_provider_register', null, null);

$('#form_provider_register').submit( (e) => {
    e.preventDefault();
    let form_data = new FormData($(`#form_provider_register`)[0]);
    form_data.append('attributeListArray', JSON.stringify(attributeListArray))
    crud_provider.data = serialize_array('form_provider_register', form_data)
    crud_provider.route = routes.provider_store;
    crud_provider.create_files(table_provider_instance, () => {
      attributeListArray = [];
      $('#attributesListBody').html('');
    });
    console.log($('#form_provider_register').serialize());

});

$('#form_provider_edit').submit(function (e) {
    e.preventDefault();
    $('#form_provider_edit').find('button[type=submit]').prop('disabled', true)
    crud_provider.form ='form_provider_edit'
    crud_provider.route = routes.update_provider;
    crud_provider.edit(table_provider_instance, id, function () {
      $('#update_provider').hide()
      $('#register_provider').show()
    });
    console.log($('#form_provider_edit').serialize());
});

$('#list_providers').on('click','#edit',function () {
    id = $(this).data('id')

    $.post(routes.get_provider, {id: id}, function (data) {
      $('#register_provider').hide();
      $('#update_provider').show();
      $('#form_provider_edit input[name="name"] ').val(data.data.name)
      $('#form_provider_edit input[name="phone"] ').val(data.data.phone)
      $('#form_provider_edit input[name="email"] ').val(data.data.email)
      $('#form_provider_edit select[name="type_document"] ').val(data.data.type_document)
      $('#form_provider_edit input[name="number_document"] ').val(data.data.number_document)
      console.log(data)
    })
});

$('#list_providers').on('click','#delete',function () {
    let id = $(this).data('id');
    crud_provider.route = routes.provider_delete;
    crud_provider.delete(table_provider_instance, id);
});

$('#form_provider_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_provider').show();
  $('#update_provider').hide();
});


$('#addAccountInList').on('click', function (e) {
  e.preventDefault()
  if ($('#name_bank').val() === "" && $('#number_account').val() === ""){
    swal({
      title: "Error de Envio",
      text: "No Seleecciono Ningun Atributo",
      icon: 'error',
    })
  }
  else{
    let element = {
      name_bank: $('#name_bank').val(),
      number_account: $('#number_account').val(),
      type_account_bank: $('#type_account_bank :selected').text(),
      id_type_account_bank: $('#type_account_bank').val(),
    };
    attributeListArray.push(element);
    console.log(attributeListArray);
    create_table_dynamic('table_attributes_account', 'attributesAccountListBody', function (arrayCells, id_table) {

      let last_index = attributeListArray.length - 1;

      let text = document.createTextNode(element.name_bank);
      let input = create_input('text', element.number_account, 'index', parseInt(last_index.toString()), 'onblur', "update_node(this);")
      let text_type_account = document.createTextNode(element.type_account_bank);
      let i = document.createElement('i')
      i.className = 'bx bx-trash';
      let button = document.createElement("button")
      button.className = 'btn btn-danger btn-icon'
      button.setAttribute('data-index', parseInt(last_index))
      button.appendChild(i)
      button.onclick = function ()
      {
        let fila = this.parentNode.parentNode;
        let table = document.getElementById(id_table)
        let tbody = table.getElementsByTagName("tbody")[0];
        tbody.removeChild(fila);
        attributeListArray.splice($(this).data('index'), 1)
      }

      arrayCells[0].appendChild(text);
      arrayCells[1].appendChild(input);
      arrayCells[2].appendChild(text_type_account)
      arrayCells[3].appendChild(button)
    })

  }
})


