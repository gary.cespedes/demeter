export let rules_provider = {
    name: {
        required: true,
        minlength: 4,
        maxlength: 20
    },
    email: {
        required: true,
        email: true,
        maxlength: 30

    },
    password: {
        required: true,
        minlength: 6,
        maxlength: 15

    },
    confirmed_password: {
        required: true,
        minlength: 6,
        equalTo: "#password",
        maxlength: 15

    },
};

export let messages_provider = {

};





