import {Crud} from "../modules/crud.js";

let crud = new Crud('form_unit', null, null);

$('#form_unit').submit(function (e) {
    e.preventDefault();
    $('#form_unit').find('button[type="submit"]').prop('disabled', true)
    crud.route = routes.unit_store;
    crud.create(table_unit_instance);
});

$('#form_unit_edit').submit(function (e) {
    e.preventDefault();
    $('#form_unit_edit').find('button[type="submit"]').prop('disabled', true)
    crud.form ='form_unit_edit'
    crud.route = routes.update_unit;
    crud.edit(table_unit_instance, id, function () {
      $('#update_unit').hide()
      $('#register_unit').show()
    });
});

$('#list_units').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_unit, {id: id}, function (data) {
      $('#register_unit').hide();
      $('#update_unit').show();
      $('#form_unit_edit input[name="name"] ').val(data.data.name)
      $('#form_unit_edit input[name="description"] ').val(data.data.description)
    })

});

$('#list_units').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.route = routes.unit_delete;
    crud.delete(table_unit_instance, id);
});

$('#form_unit_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_unit').show();
  $('#update_unit').hide();
});


