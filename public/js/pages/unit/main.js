import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {
    table_unit_instance = new Table_Component('list_units', routes.get_units, null, columns_active_unit, null, table_unit);
    table_unit_instance.initialize(table_unit_instance.column_active).catch(error => {
      console.log(error)
    });

});

