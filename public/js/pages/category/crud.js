import {Crud} from "../modules/crud.js";
import {View} from "../../components/View.js";
import { Table_Component } from "../../components/Table.js";

let crud = new Crud('form_category', null, null);

$('#form_category').submit(function (e) {
    e.preventDefault();
    $('#form_category').find('button[type="submit"]').prop('disabled', true)
    crud.route = routes.category_store;
    crud.create(table_category_instance);
});

$('#form_category_edit').submit(function (e) {
    e.preventDefault();
    $('#form_category_edit').find('button[type="submit"]').prop('disabled', true)
    crud.form ='form_category_edit'
    crud.route = routes.update_category;
    crud.edit(table_category_instance, id, function () {
      $('#update_category').hide()
      $('#register_category').show()
    });
});

$('#list_categories').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_category, {id: id}, function (data) {
      $('#register_category').hide();
      $('#update_category').show();
      $('#form_category_edit input[name="name"] ').val(data.data.name)
      $('#form_category_edit input[name="description"] ').val(data.data.description)
      console.log(data)
    })
});

$('#list_categories').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.route = routes.category_delete;
    crud.delete(table_category_instance, id);
});

$('#list_categories').on('click','#show',function () {
  let id = $(this).data('id');
  $("#category_detail").modal("show");
  load_view_category = new View(routes.get_sub_category,
    'container_detail_category', {id}, null, null)
  load_view_category.load_view('register', function(categories){ 
    table_sub_categories_instance = new Table_Component('list_sub_categories', routes.get_sub_category, {id}, columns_active_sub_categories, null, table_sub_categories);
    table_sub_categories_instance.initialize(table_sub_categories_instance.column_active).catch(error => {
      console.log(error)
    });
  })
});

$('#form_category_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_category').show();
  $('#update_category').hide();
});


