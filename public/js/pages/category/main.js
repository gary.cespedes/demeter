import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {
    table_category_instance = new Table_Component('list_categories', routes.get_categories, null, columns_active_category, null, table_category);
    table_category_instance.initialize(table_category_instance.column_active).catch(error => {
      console.log(error)
    });

});

