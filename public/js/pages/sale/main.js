import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {

    table_sale_instance = new Table_Component('list_sales', routes.get_sales, null, columns_active_sale, null, table_sale);
    table_sale_instance.initialize(table_sale_instance.column_active).catch(error => {
      console.log(error)
    });
});

