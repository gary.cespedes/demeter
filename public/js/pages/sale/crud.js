import {Crud} from "../modules/crud.js";
import { View } from "../../components/View.js";

let crud = new Crud('form_sale', null, null);
let crud_client = new Crud('form_client_register', null, null);
let count = 1;

$('#form_sale').submit(function (e) {
    e.preventDefault();
    $(`#form_sale button[type="submit"]`).prop('disabled', true)
   if (price_end > 700 && $('#client_id').val() == 1){
     Swal.fire({
       title: 'Error!',
       text: "El Precio excedio de lo Establecido, por favor seleccione un Cliente Real",
       confirmButtonText: 'Aceptar',
     })
     $(`#form_sale button[type="submit"]`).prop('disabled', false)
   }else{
     crud.route = routes.sale_store;
     crud.data = {
       data: $('#form_sale').serialize() + `&type_sale=${type_sale}`,
       attributeListArray,
       'price_initial': price_init,
       price_end,
     }
     crud.create(table_sale_instance, (data) => {
       attributeListArray = [];
       price_init = 0
       price_end = 0
       $('#attributesListBody').html('');
       refresh_totals(attributeListArray)
       if ($('input[name="print_note"]').is(':checked')){
         window.open(`${routes.print_sale}?id=${data.data.id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
       }
     });
   }
});

$('body').barcodeListener().on('barcode.valid', function(e, code){
  console.log(code)
  $.post(routes.get_product, {code: code}, (data) => {
    console.log(data)
    if (data.data !== null){
      add_car(data.data.id)
    }
    else{
      alert('No existe el producto')
    }
  })
})

$('#form_sale_edit').submit(function (e) {
  e.preventDefault();
  $(`#form_sale_edit button[type="submit"]`).prop('disabled', true)
  if (price_end > 700 && $('#client_id').val() == 1){
    Swal.fire({
      title: 'Error!',
      text: "El Precio excedio de lo Establecido, por favor seleccione un Cliente Real",
      confirmButtonText: 'Aceptar',
    })
    $(`#form_sale_edit button[type="submit"]`).prop('disabled', false)
  }else{
    crud.route = routes.sale_store;
    crud.data = {
      data: $('#form_sale_edit').serialize(),
      attributeListArray,
      'price_initial': price_init,
      price_end,
      id_sale_edit
    }
    crud.create(null, (data) => {
      attributeListArray = [];
      price_init = 0
      price_end = 0
      $('#attributesListBody').html('');
      refresh_totals(attributeListArray)
      if ($('input[name="print_note"]').is(':checked')){
        window.open(`${routes.print_sale}?id=${data.data.id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
      }
      location.href = routes.sale_index
    });
  }
});



$('#all_sales').change(() => {
    $('#sale_detail_date').toggle('slow')
})


$('#category_id').change(() => {
    let category = $('#category_id').val();
    let type_sale = $('#type_sale').val();
    $.post(routes.get_products_by_filters, {category, type_sale},  (data) => {
      $(`#product_id`).html('').select2()
      $('#product_id').select2({
        data:data.data
      })
    })
})

$('#list_sales').on('click','#print',function () {
  let id = $(this).data('id');
  window.open(`${routes.print_sale}?id=${id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#form_sale_edit').submit(function (e) {
    e.preventDefault();
    crud.form ='form_sale_edit'
    crud.route = routes.update_sale;
    crud.edit(table_sale_instance, id, function () {
      $('#update_sale').hide()
      $('#register_sale').show()
    });
});

$('#form_sale_edit').on('click','#cancel2', function (e) {
  e.preventDefault();
  $('#register_sale').show();
  $('#update_sale').hide();
});

$('#list_sales').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_sale, {id: id}, function (data) {
      $('#register_sale').hide();
      $('#update_sale').show();
      $('#form_sale_edit input[name="name"] ').val(data.data.name)
      $('#form_sale_edit input[name="description"] ').val(data.data.description)
      $('#form_sale_edit input[name="code"] ').val(data.data.code)
      $('#form_sale_edit select[name="category_id"]').val(data.data.category_id).trigger('change')
      console.log(data)
    })
});

$('#list_sale_actions a[data-action="all_deleted"]').click(() => {
  table_sale_instance.column_inactive = columns_innactive_sale
  table_sale_instance.route = routes.get_sales_inactive
  table_sale_instance.initialize(table_sale_instance.column_inactive)
})

$('#list_sale_actions a[data-action="all"]').click(() => {
  table_sale_instance.column_inactive = columns_active_sale
  table_sale_instance.route = routes.get_sales
  table_sale_instance.initialize(table_sale_instance.column_active)
})

$('#list_sales').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.route = routes.sale_delete;
    crud.delete(table_sale_instance, id);
});

$('#list_sales').on('click','#edit',function () {
  let id = $(this).data('id');
  location.href = `${routes.sale_edit}?id=${id}`
});


$('#list_sales').on('click','#show',function () {
  let id = $(this).data('id');
  $("#info_sale").modal("show");
  load_view_sale = new View(routes.show_sale,
    'container_info_sale', {id}, null, null)
  load_view_sale.load_view('register')
});

$('#payment_type').change(() => {

  if ($('#payment_type').val() == 1){
    $('input[name="detail_sale"]').prop('readonly', true)
  }
  else{
    $('input[name="detail_sale"]').prop('readonly', false)
  }
})


$('#addAttributeInList').on('click', function (e) {
  e.preventDefault()
  if ($('#product_id').val() === "" ){
    swal({
      title: "Error de Envio",
      text: "El valor esta vacio ",
      icon: 'error',
    })
  }
  else{
    add_car($('#product_id').val())
    let list_price_html = `<div class='row'>`
    $.post(routes.get_branch_with_stock, {id: parseInt($('#product_id').val())}, (data) => {
      data.data.forEach((value, key) => {
         list_price_html += list_price(value)
      })

      list_price_html += `</div>`
      $('#container_product_price').html(list_price_html)
    })
  }

})


$('#form_sale').on('click', '#send_sale', function (e) {
  e.preventDefault()
  add_car($(this).data('id'))

})


$('#form_client_register').submit(function (e) {
  e.preventDefault();
  let form_data = new FormData($(`#form_client_register`)[0]);
  crud_client.data = serialize_array('form_client_register', form_data)
  crud_client.route = routes.client_store;
  crud_client.create_files(null, (data) => {
    $("#client_id").append($("<option selected></option>")
      .attr("value", data.data.id)
      .text(data.data.name));
    $('#client_register').modal('toggle');
  });
});


$('#descount_to_total').keyup(function (e) {
  if ($(this).val() != 0 && $(this).val() != "" ){
    price_end = price_end - (price_init * parseFloat($(this).val()) / 100)
  }
  else{
    price_end = price_init
  }
  $('#price_end').html(price_end.toFixed(2))

})

$('#efectivo_payment').keyup(function (e) {
  if ($(this).val() > 0 ){
    let total = parseFloat($('#price_end').text()) - parseFloat($(this).val())
    $('#vuelto_payment').html(total.toFixed(2) / -1)

  }
  else{
    $(this).val('')
  }

})


$('#sale_generate').submit(function (e) {
  e.preventDefault();
  let data_serialize = $('#sale_generate').serialize();
  if ($('#all_sales').is(':checked')){
    table_sale_instance.data = null
  }
  else{
    table_sale_instance.data = data_serialize
  }
  table_sale_instance.route = routes.get_sales
  table_sale_instance.initialize(table_sale_instance.column_active).catch(error => {
    console.log(error)
  });
});


$('#applicate_discount').click( (e) => {
  e.preventDefault()
  Swal.fire({
    title: 'Ingrese Contraseña',
    input: 'password',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Aceptar',
    showLoaderOnConfirm: true,
    preConfirm: (password) => {
      return $.post(routes.enabled_discount_total, { password} )
        .then(response => {
          if (!response) {
            throw new Error(response.statusText)
          }
          return response
        })
        .catch(error => {
          Swal.showValidationMessage(
            `Error de Contraseña`
          )
        })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    console.log(result)
    if (result.value) {
      $('#applicate_discount').hide()
      $('#descount_to_total').show()
    }
  })
})
