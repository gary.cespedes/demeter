import {Crud} from "../modules/crud.js";
import { View } from "../../components/View.js";


let crud = new Crud('form_kardex', null, null);

$('#form_kardex').submit(function (e) {
    e.preventDefault();
    $(`#form_sale button[type="submit"]`).prop('disabled', true)
    let data_serialize = $('#form_kardex').serialize();
    $.get(routes.get_kardexs,data_serialize , (data) => {
      if ($('#excel').is(':checked')){
        window.location=routes.get_kardexs+`?${data_serialize}`;
      }
      else{
        if ($('#pdf').is(':checked')){

          window.location=routes.get_kardexs+`?${data_serialize}`;
        }
        else{
          $('#container_kardex_result').html(data)
        }
      }
    })
});

$('#list_sales').on('click','#print',function () {
  let id = $(this).data('id');
  window.open(`${routes.print_sale}?id=${id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#list_sales').on('click','#show',function () {
  let id = $(this).data('id');
  $("#info_sale").modal("show");
  load_view_sale = new View(routes.show_sale,
    'container_info_sale', {id}, null, null)
  load_view_sale.load_view('register')
});

