import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {
    table_transfer_instance = new Table_Component('list_transfers', routes.get_transfers_for_status, {status: 3}, columns_active_transfer_received, null, table_transfer);
    table_transfer_instance.initialize(table_transfer_instance.column_active).catch(error => {
      console.log(error)
    });
});

