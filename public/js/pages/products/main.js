import { Table_Component } from "../../components/Table.js";

$(document).ready(function () {

  table_product_instance = new Table_Component('list_products', routes.get_products, null, columns_active_product, null, table_product);
  table_product_instance.initialize(table_product_instance.column_active).catch(error => {
    console.log(error)
  });
});

