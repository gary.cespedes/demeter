import {Crud} from "../modules/crud.js";
import {View} from "../../components/View.js";

let crud = new Crud('form_product', null, null);
let crud_category = new Crud('form_category', null, null);
let crud_unit = new Crud('form_unit', null, null);

$('#form_product').submit(function (e) {
    e.preventDefault();
    $('#form_product').find('button[type="submit"]').prop('disabled', true)
    let form_data = new FormData($(`#form_product`)[0]);
    form_data.append('attributeListArray', JSON.stringify(attributeListArray))
    crud.data = serialize_array('form_product', form_data)
    crud.route = routes.product_store;
    crud.create_files(table_product_instance, () => {
      attributeListArray = [];
      $('#attributesListBody').html('');
    });
});

$('#form_product_edit').submit(function (e) {
    e.preventDefault();
    $('#form_product_edit').find('button[type="submit"]').prop('disabled', true)
    crud.form ='form_product_edit'
    crud.route = routes.update_product;
    crud.edit(table_product_instance, id, function () {
      $('#update_product').hide()
      $('#register_product').show()
    });
});

$('#form_category').submit(function (e) {
  e.preventDefault();
  $(`#form_category button[type="submit"]`).prop('disabled', true)
  crud_category.route = routes.category_store;
  crud_category.create(null, (data) => {
  $("#category_id").append($("<option selected></option>")
    .attr("value", data.data.id)
    .text(data.data.name));

  $('#category_register').modal('toggle');

  });


});

$('#form_unit').submit(function (e) {
  e.preventDefault();
  $(`#form_unit button[type="submit"]`).prop('disabled', true)
  crud_unit.route = routes.unit_store;
  crud_unit.create(null, (data) => {

    $("#unit_id").append($("<option selected></option>")
      .attr("value", data.data.id)
      .text(data.data.name));

    $('#unit_register').modal('toggle');

  });


});


$('#list_products').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_product, {id: id}, function (data) {
      $('#register_product').hide();
      $('#update_product').show();
      $('#form_product_edit input[name="code"] ').val(data.data.code)
      $('#form_product_edit input[name="name"] ').val(data.data.name)
      $('#form_product_edit input[name="description"] ').val(data.data.description)
      $('#form_product_edit input[name="quantity_minim"] ').val(data.data.quantity_minim)
      $('#form_product_edit select[name="category_id"]').val(data.data.category_id).trigger('change')
      $('#form_product_edit select[name="unit_id"]').val(data.data.unit_id).trigger('change')
      console.log(data)
    })


});

$('#list_products').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.route = routes.product_delete;
    crud.delete(table_product_instance, id);
});

$('#form_product_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_product').show();
  $('#update_product').hide();
});

$('#list_products').on('click','#show',function () {
  let id = $(this).data('id');
  $("#info_product").modal("show");
  load_view_product = new View(routes.show_product,
    'container_info_product', {id}, null, null)
  load_view_product.load_view('register')
});


$('#addProductsInList').on('click', function (e) {
  e.preventDefault()

  if (!$('#price').val() || !$('#price_description').val()){
    swal({
      title: "Error de Envio",
      text: "Campo(s) vacios",
      icon: 'error',
    })
  }
  else{
    let element = {
      price: $('#price').val(),
      description: $('#price_description').val(),
      is_predetermined: ($('#is_predetermined').is(':checked')) ? 1 : 0
    };

    if (element.is_predetermined){
      for (let i = 0; i < attributeListArray.length; i++){
        $(`#table_attributes_products input[type="checkbox"]`).prop('checked', false)
        attributeListArray[i].is_predetermined = 0;
      }
    }
    attributeListArray.push(element);

    create_table_dynamic('table_attributes_products', 'productsListBody', function (arrayCells, id_table) {

      let last_index = attributeListArray.length - 1;

      let text = document.createTextNode(element.description);
      let input = create_input('text', element.price, 'index', parseInt(last_index.toString()), 'onblur', "update_node(this);")

      let input_check = create_check(element.is_predetermined, 'index',  parseInt(last_index.toString()), 'onClick', "update_check_price(this, 'table_attributes');")

      let i = document.createElement('i')
      i.className = 'bx bx-trash';

      let button = document.createElement("button")
      button.className = 'btn btn-danger btn-icon'
      button.setAttribute('data-index', parseInt(last_index))
      button.appendChild(i)
      button.onclick = function ()
      {
        let fila = this.parentNode.parentNode;
        let table = document.getElementById(id_table)
        let tbody = table.getElementsByTagName("tbody")[0];
        tbody.removeChild(fila);
        attributeListArray.splice($(this).data('index'), 1)
      }

      arrayCells[0].appendChild(text);
      arrayCells[1].appendChild(input);
      arrayCells[2].appendChild(input_check)
      arrayCells[3].appendChild(button)
    })

  }
})

