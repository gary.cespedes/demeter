import {Crud} from "../modules/crud.js";
import {View} from "../../components/View.js";

let crud = new Crud('form_user', null, null);

$('#form_user').submit(function (e) {
    e.preventDefault();
    $('#form_user').find('button[type="submit"]').prop('disabled', true)
    let form_data = new FormData($(`#form_user`)[0]);
    crud.data = serialize_array('form_user', form_data)
    crud.route = routes.user_store;
    crud.create_files(table_user_instance);

});

$('#form_user_edit').submit(function (e) {
    e.preventDefault();
    $('#form_user_edit').find('button[type="submit"]').prop('disabled', true)
    crud.form ='form_user_edit'
    crud.route = routes.update_user;
    crud.edit(table_user_instance, id, function () {
      $('#update_user').hide()
      $('#register_user').show()
    });
});

$('#list_users').on('click','#edit',function () {
    id = $(this).data('id')
    $.post(routes.get_user, {id: id}, function (data) {
      $('#register_user').hide();
      $('#update_user').show();
      $('#form_user_edit input[name="name"] ').val(data.data.name)
      $('#form_user_edit input[name="password"] ').val("")
      $('#form_user_edit input[name="email"] ').val(data.data.email)
      $('#form_user_edit input[name="is_power_user"] ').prop('checked', data.data.is_power_user)
    })


});

$('#list_users').on('click','#delete',function () {
    let id = $(this).data('id');
    crud.route = routes.user_delete;
    crud.delete(table_user_instance, id);
});

$('#list_users').on('click','#config',function () {

  let id = $(this).data('id');

  load_view_permission = new View(routes.user_permission_view, 'config_permission', {id}, null, null)
  load_view_permission.load_view('register', () => {
    $('#index_user').toggle('slow')
    $('#config_permission').toggle('slow')
  })
});

$('body').on('click', 'a[data-action="back_close"]', function () {

  $('#index_user').toggle('slow')
  $('#config_permission').toggle('slow')

});


$('#form_user_edit').on('click',"button[type='reset']",function (e) {
  e.preventDefault()
  $('#register_user').show();
  $('#update_user').hide();
});
