import {View} from "../../components/View.js";

$('#list_transfers').on('click','#transfer',function () {
  let id = $(this).data('id');
  Swal.fire({
    title: 'Estas Seguro?',
    text: "Tu realizaras la transferencia!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Confirmar!',
    confirmButtonClass: 'btn btn-primary',
    cancelButtonClass: 'btn btn-danger ml-1',
    buttonsStyling: false,
  }).then(function (willDelete) {
    if (willDelete.value) {
      $.post(routes.change_status_transfer, {id, status: 1}, (data)=>{
        (table_transfer_instance !== null) ? table_transfer_instance.initialize(table_transfer_instance.column_active) : '';
        toastr.success('', common.success);
      })
    }
  });
});

$('#list_transfers').on('click','#transfer_print',function () {
  let id = $(this).data('id');
  window.open(`${routes.print_transfer}?id=${id}` , "VENTA" , "width=700,height=700,scrollbars=NO")
});

$('#list_transfers').on('click','#show',function () {
  let id = $(this).data('id');
  $("#transfer_detail").modal("show");
  load_view_show_transfer = new View(routes.show_transfer,
    'container_transfer_detail', {id}, null, null)
  load_view_show_transfer.load_view('register')
});
