import {Crud} from "../modules/crud.js";
import {View} from "../../components/View.js";

let crud_sub_category = new Crud('form_sub_category', null, null);
let id;

$('#form_sub_category').submit(function (e) {
    e.preventDefault();
    $('#form_sub_category').find('button[type="submit"]').prop('disabled', true)
    crud_sub_category.form = 'form_sub_category'
    crud_sub_category.route = routes.sub_category_store;
    crud_sub_category.create(table_sub_categories_instance);
    $('#register_sub_category').hide();
    console.log($('#form_sub_category').serialize());
});

$('#form_sub_category_edit').submit(function (e) {
    e.preventDefault();
    $('#form_sub_category_edit').find('button[type=submit]').prop('disabled', true)
    crud_sub_category.form ='form_sub_category_edit'
    crud_sub_category.route = routes.update_sub_category;
    crud_sub_category.edit(table_sub_categories_instance, id, function () {
      $('#update_sub_category').hide()
    });
    console.log($('#form_sub_category_edit').serialize());
});

$('#list_sub_categories').on('click','#edit',function () {
    id = $(this).data('id')

    $.post(routes.sub_category_get, {id: id}, function (data) {
      $('#update_sub_category').show();
      $('#form_sub_category_edit input[name="name"] ').val(data.data.name)
      $('#form_sub_category_edit input[name="description"] ').val(data.data.description)

    })
});


$('#list_sub_categories').on('click','#delete',function () {
    let id = $(this).data('id');
    crud_sub_category.route = routes.sub_category_delete;
    crud_sub_category.delete(table_sub_categories_instance, id);
});

$('#list_categories').on('click','#show',function () {
  let id = $(this).data('id');
  $("#category_detail").modal("show");
  load_view_category = new View(routes.get_sub_category,
    'container_detail_category', {id}, null, null)
  load_view_category.load_view('register')
});

$('#form_category_edit').on('click','#cancel2',function (e) {
  e.preventDefault()
  $('#register_category').show();
  $('#update_category').hide();
});


