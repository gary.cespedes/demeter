var power_user;
var branch_id;
var type_sale;

var array_tags = [];


var price_init = 0;
var price_end = 0;

var id;

var load_view_show_income;
var load_view_category;
var load_view_permission;
var table_user_instance;
var table_user;

var table_sub_categories_instance;
var table_sub_categories;


var load_view_show_warehouses;

var table_branch_instance;
var table_branch;

var load_view_show_transfer;
var table_transfer_instance;
var table_transfer;

var table_category_instance;
var table_category;

var id_sale_edit

var table_income_instance;
var table_income;

var load_view_product;
var table_product_instance;
var table_product;

var table_sale;
var table_sale_instance;
var sale_product;

var table_provider_instance;
var table_provider;


var table_unit_instance;
var table_unit;


var table_client_instance;
var table_client;

var table_warehouse_instance;
var table_warehouse;

var attributeListArray = [];

var load_view_sale;
var load_view_kardex;


var columns_active_sale = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      let button_print = `<button id="print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-printer"></i></button>`;
      let button_show = buttons.initialize('show');
      let button_delete = buttons.initialize('delete');
      return button_show + button_delete + button_print;

    }
  },
  {
    data: "code",
  },
  {
    data: "type_sale",
  },
  {
    data: "date_sale",
  },
  {
    data: "price_initial"
  },
  {
    data: "price_end"
  },
];

var columns_innactive_sale = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      return buttons.initialize('update');


    }
  },
  {
    data: "code_sale",
  },
  {
    data: "type_sale",
  },
  {
    data: "date_sale",
  },
  {
    data: "price_initial"
  },
  {
    data: "price_end"
  },
];

var columns_active_user = [
    {
        data: "id",
        render: function (data) {
          let buttons = new Button(data);
          return buttons.initialize('default') + buttons.initialize('config');
        }
    },
    {
        data: "name",
    },
    {
        data: "email"
    },

];

var columns_active_income = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      let delete_button =  buttons.initialize('delete');
      let show_button =  buttons.initialize('show');

      return delete_button + show_button;
    }
  },
  {
    data: "code",
  },
  {
    data: "date_into"
  },

];

var columns_active_transfer = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      let delete_button =  buttons.initialize('delete');
      let show_button =  buttons.initialize('show');
      let button_print  =`<button id="transfer_print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-file-find"></i></button>`;

      return delete_button + show_button + button_print;
    }
  },
  {
    data: "code",
  },
  {
    data: "date_transfer"
  },

];

var columns_active_transfer_pending = [
  {
    data: "id",
    render: function (data, value, value2) {
      let buttons = new Button(data);
      // let delete_button =  buttons.initialize('delete');
      let show_button =  buttons.initialize('show');
      let button_print  =`<button id="transfer_print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-file-find"></i></button>`;
      let button_confirm =  '';

      if (value2.status === 0 ){
        button_confirm  =`<button id="transfer" class="btn btn-icon btn-success glow mr-1" data-id = "${data}"> <i class="bx bx-check-double"></i></button>`;
      }
      return ` ${show_button} ${button_confirm} ` + button_print;

    }
  },
  {
    data: "code",
  },
  {
    data: "date_transfer"
  },

];

var columns_active_transfer_transfer = [
  {
    data: "id",
    render: function (data, value, value2) {
      let buttons = new Button(data);
      // let delete_button =  buttons.initialize('delete');
      let show_button =  buttons.initialize('show');
      let button_print  =`<button id="transfer_print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-file-find"></i></button>`;
      let button_confirm =  '';
      if (value2.status === 2 ){
        button_confirm  =`<button id="transfer_transfer" class="btn btn-icon btn-success glow mr-1" data-id = "${data}"> <i class="bx bx-check-double"></i></button>`;
      }
      return `${show_button} ${button_confirm}` + button_print;

    }
  },
  {
    data: "code",
  },
  {
    data: "date_transfer"
  },

];

var columns_active_transfer_received = [
  {
    data: "id",
    render: function (data, value, value2) {
      let buttons = new Button(data);
      let show_button =  buttons.initialize('show');
      let button_print  =`<button id="transfer_print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-file-find"></i></button>`;

      return `${show_button} ` + button_print;

    }
  },
  {
    data: "code_transfer",
  },
  {
    data: "date_transfer"
  },

];


var columns_active_transfer_solicitude = [
  {
    data: "id",
    render: function (data, value, value2) {
      let buttons = new Button(data);
      // let delete_button =  buttons.initialize('delete');
      let show_button =  buttons.initialize('show');
      let button_confirm = ''
      let button_print  =`<button id="transfer_print" class="btn btn-icon btn-warning glow mr-1" data-id = "${data}"> <i class="bx bx-file-find"></i></button>`;

      if (value2.status === 0 ){
        button_confirm  =`<button id="accept_transfer" class="btn btn-icon btn-success glow mr-1" data-id = "${data}"> <i class="bx bx-check-double"></i></button>`;
      }
      return `${show_button} ${button_confirm} ${button_print}`;
    }
  },
  {
    data: "code_transfer",
  },
  {
    data: "date_transfer"
  },

];


var columns_active_provider = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      return buttons.initialize('default');
    }
  },
  {
    data: "name",
  },
  {
    data: "email"
  },

];



var columns_active_warehouse = [
    {
        data: "id",
        render: function (data) {
          let buttons = new Button(data);
          return buttons.initialize('default');
        }
    },
    {
        data: "name",
    },
];

var columns_active_warehouse_with_stock = [
  {
    data: "id",
    render: function (data) {
      let button = `<button id="to_solicited" title="Elegir almacen" class="btn btn-icon btn-success glow mr-1" data-id = "${data}"> <i class="bx bx-send"></i></button>`;
      return `${button}`;
    }
  },
  {
    data: "name",
  },
  {
    data: "stock",
  },
];

var columns_active_warehouse_with_stock_sale = [
  {
    data: "name",
  },
  {
    data: "stock",
  },
];

var columns_active_product = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      return buttons.initialize('all');
    }
  },
  {
    data: "name",
  },
  {
    data: "description"
  },
  {
    data: "quantity_minim"
  },
];

var columns_active_attribute = [
    {
        data: "id",
        render: function (data) {
            let buttons = new Button(data);
            return buttons.initialize('default');
        }
    },
    {
        data: "name",
        render: function (data, type, customer) {
            $(".tooltipped").tooltip({
                delay: 50
            });
            return `<a class="tooltipped" data-position="top"
                    data-delay="50" data-tooltip="${common.a.title.show} ${customer.name.toUpperCase()}">${customer.name}</a>`;
        }
    },
    {
        data: "description"
    },
    {
        data: "user"
    },

];

var columns_active_attribute_rom = [

    {
        data: "room",
        render: function (data, type, room) {
            $(".tooltipped").tooltip({
                delay: 50
            });
            return `<a class="tooltipped" data-position="top"
                    data-delay="50" data-tooltip="${common.a.title.show} ${room.room.toUpperCase()}">${room.room}</a>`;
        }

    },
    {
        data: "branch"
    },

];

var columns_active_client = [
    {
        data: "id",
        render: function (data) {
            let buttons = new Button(data);
            return buttons.initialize('default');
        }
    },
    { data: "name" },
    { data: "last_name" },
    { data: "number_document" },
    { data: "email" },

];

var columns_active_branch = [
    {
        data: "id",
        render: function (data) {
            let buttons = new Button(data);
            return buttons.initialize('default');
        }
    },
    {
        data: "name",
    },
    { data: "address" },
    { data: "phone" },
    { data: "is_warehouse" },
    { data: "user" },

];

var columns_active_room = [
    {
        data: "id",
        render: function (data) {
            let buttons = new Button(data);
            return buttons.initialize('default');
        }
    },
    {
        data: "name",
        render: function (data, type, branch) {
            $(".tooltipped").tooltip({
                delay: 50
            });
            return `<a class="tooltipped" data-position="top"
                    data-delay="50" data-tooltip="${common.a.title.show} ${branch.name.toUpperCase()}">${branch.name}</a>`;
        }
    },
    { data: "description" },
    { data: "floor" },
    { data: "category" },
    { data: "branch" },
    { data: "is_free" },
    { data: "is_clean" },
    { data: "user" },

];

var columns_active_category = [
    {
        data: "id",
        render: function (data) {
            let buttons = new Button(data);
            return buttons.initialize('all');
        }
    },
    {
        data: "name",
    },
    {
      data: "description",
    },

];

var columns_active_sub_categories = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      return buttons.initialize('default');
    }
  },
  {
    data: "name",
  },
  {
    data: "description",
  },

];

var columns_active_unit = [
  {
    data: "id",
    render: function (data) {
      let buttons = new Button(data);
      return buttons.initialize('default');
    }
  },
  {
    data: "name",
  },
  {
    data: "description",
  },

];

