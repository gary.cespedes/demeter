
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

let disabled = false;
let count = 1;

function button_disabled(form){
  disabled = !disabled;
  $(`#${form} button[type="submit"]`).prop('disabled', disabled)
}

function create_check(checked,data, setData, event, function_event) {

  let div = document.createElement('div')
  div.setAttribute('class', 'form-check')

  let label = document.createElement('label')
  label.setAttribute('class', 'form-check-label')

  let input = document.createElement('input')
  input.type = 'checkbox'
  if (checked) {
    input.setAttribute(`checked`,true)
  }
  input.setAttribute(`class`,'form-check-input check-price')
  input.setAttribute(`data-${data}`, setData)
  input.setAttribute(event, function_event);

  let span = document.createElement('span')
  span.setAttribute('class', 'form-check-sign')
  let span2 = document.createElement('span')
  span2.setAttribute('class', 'check')
  span.appendChild(span2)
  label.appendChild(input)
  label.appendChild(span)
  div.appendChild(label)
  return div;
}

function refresh_totals(array){

  let total_init = 0
  let total_end = 0
  $.each(array, function (key, value) {
    if (value !== null ){
      total_init += value.price_init
      total_end += value.price_end
    }
  })

  price_init = total_init
  price_end = total_end
  $('#price_init').html(total_init.toFixed(2))
  $('#price_end').html(total_end.toFixed(2))
  $('#total_sale').html(total_end.toFixed(2))
}


function list_price(element) {

  let prices = "";
  element.price.forEach((value, key) => {
    prices += `<p>${value}</p>`
  })


  return `<div style="height: auto; border: 1px solid black; width: 25%; margin: 3px">
                    <h4><span class="badge badge-light-info">${element.name} </span></h4>
                    <h6>${element.product}</h6>
                    <p>
                      <b>Stock : ${element.stock}</b>
                    </p>
                    <p>
                      ${prices}
                    </p>

              </div>`
}

function create_select(array, selected,data, setData,event, function_event) {

  let select = `<select class="custom-select" data-${data}="${setData}" ${event}="${function_event}">`;
  let option = "";
  $.each(array, (key, value) => {
    option += `<option value="${value.id}" data-id="${value.id}" data-price="${value.price}">${value.name} (${value.price})</option>`;
  })
  select +=option;
  select += `</select>`
  return select;
}

function search_value_in_array(id, array) {
  let bandera = false;
  $.each(array,  function (key, value) {
    if (value.id === id){
      bandera = true;
    }
  })

  return bandera;
}

function ajax_request(route, data, type) {

    console.log(data)
    return $.ajax({
        'url': route,
        'data': data,
        'type': type,
        success: function(response) {
            console.log(response);
            console.log(data);
        },
        // 'contentType': false,
        // 'processData': false,
    })
}

function ajax_request_form_data(route, data, type) {

  return $.ajax({
    'url': route,
    'data': data,
    'type': type,
    'contentType': false,
    'processData': false,
  })
}

function scroll_to_section(id_div) {
  console.log(id_div)
    $('html,body').animate({
            scrollTop: $(`#${id_div}`).offset().top},
        1000);
}

function rules(form, rules, messages) {
    $(`#${form}`).validate({
        rules,
        messages,
        errorElement: 'div',
        errorPlacement: function (error, element) {
            let placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });
}

function create_input(type, value, data, setData, event, function_event) {
    let input = document.createElement('input')
    input.type = type
    input.value = value
    input.setAttribute(`data-${data}`, setData)
    input.setAttribute(`class`, 'form-control')
    input.setAttribute(`width`, '60')
    input.setAttribute(event, function_event);
    return input;
}

function change_price(input){
  console.log('change_price')
  let index = $(input).data('index')
  let price = $(input).val()
  let quantity = attributeListArray[index].quantity
  attributeListArray[index].price = price
  attributeListArray[index].total_price = price * quantity
  attributeListArray[index].total_price_res = attributeListArray[index].total_price
  $(`#${attributeListArray[index].count}`).html(attributeListArray[index].total_price)
  attributeListArray[index].price_init = attributeListArray[index].total_price
  attributeListArray[index].price_end = attributeListArray[index].total_price
  if (attributeListArray[index].discount){
    change_discount(null, attributeListArray[index].discount, index)
  }

  refresh_totals(attributeListArray)

}

function serialize_array(form, formData){
  let data = $(`#${form}`).serializeArray();
  $.each(data, (key, value) => {
    formData.append(value.name, value.value);
  })
  return formData
}


function show_list_product(data, container) {
  $(`#${container}`).html(data.text)
}

function update_select(select) {
  let data_index = $(select).data('index');
  let data_price = $(select).find(':selected').data('price')
  let data_id_price = $(select).find(':selected').data('id')

  attributeListArray[data_index].price = parseInt(data_price)

  attributeListArray[data_index].price_id = parseInt(data_id_price)
  attributeListArray[data_index].total_price = attributeListArray[data_index].price * attributeListArray[data_index].quantity

  attributeListArray[data_index].total_price_res = attributeListArray[data_index].total_price
  $(`.${attributeListArray[data_index].count}`).val(data_price)
  $(`#${attributeListArray[data_index].count}`).html(attributeListArray[data_index].total_price)
  attributeListArray[data_index].price_init = attributeListArray[data_index].total_price
  attributeListArray[data_index].price_end = attributeListArray[data_index].total_price

  if (attributeListArray[data_index].discount){
    change_discount(null, attributeListArray[data_index].discount, data_index)
  }

  refresh_totals(attributeListArray)
}

function change_discount(input , value, index){

  let discount;
  if (index === null){
    index = $(input).data('index')
  }
  if (value === null){
    discount = parseInt(input.value)
  }
  else{
    discount = value
  }

  let total_price = attributeListArray[index].total_price_res
  let total_discount;

  if (discount === "" || !discount ){
    discount = 0;
  }

  total_discount = (total_price * discount) / 100
  attributeListArray[index].total_price = total_price - total_discount
  attributeListArray[index].discount = discount
  $(`#${attributeListArray[index].count}`).html(attributeListArray[index].total_price.toFixed(2))

  attributeListArray[index].price_end = attributeListArray[index].total_price
  refresh_totals(attributeListArray)
}

function formatResult (result){
  if (result.loading) return result.text;
  let option = `<option value="${result.id}" >${result.name}</option>`
  return option
}

function disabled_input(valor){
  if ( valor == 1){
    $('input[name="code_bar"]').prop('readonly', true)
    $('input[name="code_bar"]').val('Sin codigo')
  }
  else{
    $('input[name="code_bar"]').prop('readonly', false)
    $('input[name="code_bar"]').val('')
  }
}

function verify_count(total, count, callback){
  if (total === count){
    callback()
  }
}

function check_item_in_list(checkbox, id) {
  $('#loader').show()
  $('#generate_tag').hide()
  let find = array_tags.find((element) => {
    return element.id === id
  })

  if ($(checkbox).is(':checked')){
    if (!find){
      $.post(routes.get_product, {id}, (data) => {
        data.data.quantity = $(checkbox).data('quantity')
        array_tags.push(data.data)
        $('#loader').hide()
        $('#generate_tag').show()
      })
    }

  }
  else{
      let index = array_tags.indexOf(find)
      array_tags.splice(index, 1)
      console.log(array_tags.length)
      if (array_tags.length <= 0) {
        $('#generate_tag').hide()
      }else{
        $('#generate_tag').show()
      }
      $('#loader').hide()
  }
}

function change_quantity(input, i = null, q = null){

    let index
    let quantity

    if (i === null ){
      index = $(input).data('index')
    }
    else{
      index = i
    }
    if (q === null ){
      quantity =  parseInt($(input).val());
    }
    else{
      quantity = q
    }

    if (quantity > attributeListArray[index].quantity_allow ){

        quantity = attributeListArray[index].quantity_allow
        $(input).val(quantity)
    }

    attributeListArray[index].quantity = quantity
    attributeListArray[index].total_price = attributeListArray[index].price * quantity
    attributeListArray[index].total_price_res = attributeListArray[index].total_price

    $(`#${attributeListArray[index].count}`).html(attributeListArray[index].total_price)

    attributeListArray[index].price_init = attributeListArray[index].total_price
    attributeListArray[index].price_end = attributeListArray[index].total_price

    if (attributeListArray[index].discount){
      change_discount(null, attributeListArray[index].discount, index)
    }

    refresh_totals(attributeListArray)

}


function add_car(id, quantity = 1) {

  let verify = false
  let element = null
  $.post(routes.get_product_with_prices, {id: id}, function (data) {

    console.log(data)
    $.each(attributeListArray, (key, value) => {

      if (id == value.id){
        value.quantity ++
        verify = true
        element = value
        let input = $('#attributesListBody').find(`input[data-index = ${key}]`)
        $(input[1]).val(value.quantity)
        change_quantity(input[1], key, value.quantity)
      }
    })

    if (!verify) {
      element = {
        id: data.product.id,
        code: data.product.code,
        count: count,
        quantity_allow: data.product.stock,
        quantity: quantity,
        discount: null,
        name: data.product.name,
        type_prices : data.prices,
        product_id: data.product.id,
        price_init: data.product.price  * quantity,
        price_end: data.product.price  * quantity,
        price_id: data.product.price_id,
        price: data.product.price,
        total_price: data.product.price  * quantity,
        total_price_res: data.product.price * quantity,
      };

      attributeListArray.push(element);

      refresh_totals(attributeListArray)
      count++

      create_table_dynamic('table_attributes', 'attributesListBody', function (arrayCells, id_table) {
        let last_index = attributeListArray.length - 1;

        let text_code = document.createTextNode(element.code);
        let text = document.createTextNode(element.name);

        let price_input = create_input('text', element.price, 'index', parseInt(last_index.toString()), 'Onkeyup', "change_price(this, null, null);")
        let select = create_select(element.type_prices, element.price_id, 'index', parseInt(last_index.toString()),'onChange', 'update_select(this)')
        price_input.setAttribute('class',`${element.count} form-control`)
        price_input.setAttribute('readonly',true)

        let input_quantity = create_input('text', element.quantity, 'index', parseInt(last_index.toString()), 'Onkeyup', "change_quantity(this);")

        // let input_discount = create_input('text', "", 'index', parseInt(last_index.toString()), 'Onkeyup', "change_discount(this, null, null);")

        let price_total = document.createElement('span');
        let value_total = document.createTextNode(element.total_price);
        let quantity_allow = document.createElement('span');
        let quantity_allow_value = document.createTextNode(element.quantity_allow);

        price_total.setAttribute('id',element.count)
        price_total.appendChild(value_total)

        quantity_allow.appendChild(quantity_allow_value)

        let i = document.createElement('i')
        i.className = 'bx bx-trash';


        let button = document.createElement("button")
        button.className = 'btn btn-danger btn-icon'
        button.setAttribute('data-index', parseInt(last_index))
        button.appendChild(i)
        button.onclick = function (element)
        {
          let fila = this.parentNode.parentNode;
          let table = document.getElementById(id_table)
          let tbody = table.getElementsByTagName("tbody")[0];
          tbody.removeChild(fila);
          // attributeListArray.splice($(this).data('index'), 1)
          attributeListArray[$(this).data('index')] = null
          refresh_totals(attributeListArray)
          console.log(attributeListArray)
        }

        arrayCells[0].appendChild(text_code);
        arrayCells[1].appendChild(text);
        arrayCells[2].innerHTML = select;
        arrayCells[3].appendChild(price_input);
        arrayCells[4].appendChild(quantity_allow);
        arrayCells[5].appendChild(input_quantity);
        // arrayCells[5].appendChild(input_discount);
        // arrayCells[5].appendChild(price_init);
        arrayCells[6].appendChild(price_total);
        arrayCells[7].appendChild(button)
      })
    }

  })

}
