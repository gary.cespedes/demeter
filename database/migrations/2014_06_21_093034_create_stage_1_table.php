<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStage1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('users', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->string('email', 100)->unique();
        $table->timestamp('email_verified_at')->nullable();
        $table->string('password');
        $table->string('branch_id')->nullable();
        $table->string('photo')->nullable();
        $table->boolean('is_client')->default(0);
        $table->boolean('is_power_user')->default(0);
        \App\Helpers\MigrationHelper::default_time_stamps($table);
        $table->rememberToken();

      });

      Schema::create('password_resets', function (Blueprint $table) {
        $table->string('email', 100)->index();
        $table->string('token');
        $table->timestamp('created_at')->nullable();
      });

      Schema::create('permissions', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_integer($table);
        $table->string('name');
        $table->string('module');
        $table->boolean('visible')->default(1);
        \App\Helpers\MigrationHelper::default_time_stamps($table);

      });

      Schema::create('user_permissions', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::foreign_integer($table, 'permission_id', 'permissions');
        \App\Helpers\MigrationHelper::default_time_stamps($table);

      });

      Schema::create('permission_templates', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->text('description');
        \App\Helpers\MigrationHelper::default_time_stamps($table);

      });

      Schema::create('permission_template_detail', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        \App\Helpers\MigrationHelper::foreign_string($table, 'permission_template_id', 'permission_templates');
        \App\Helpers\MigrationHelper::foreign_integer($table, 'permission_id', 'permissions');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('units', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->string('description')->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('categories', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->string('description')->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('sub_categories', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->string('description')->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'category_id', 'categories');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('providers', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->string('phone');
        $table->string('email');
        $table->tinyInteger('type_document');
        $table->string('number_document');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('products', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('code');
        $table->string('name');
        $table->text('description');
        $table->integer('quantity_minim');
        \App\Helpers\MigrationHelper::foreign_string($table, 'unit_id', 'units');
        \App\Helpers\MigrationHelper::foreign_string($table, 'category_id', 'categories');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('branch', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        $table->text('address');
        $table->string('phone');
        $table->string('phone_alternative')->nullable();
        $table->boolean('is_warehouse')->default(0);
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('branch_details', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->bigInteger('stock');
        $table->float('acquisition_price');
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_id', 'branch');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('transfers', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->text('note_transfer');
        $table->string('code');
        $table->date('date_transfer');
        $table->integer('status')->default(0);
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_origin_id', 'branch');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_destine_id', 'branch');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('transfer_details', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->bigInteger('quantity');
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'transfer_id', 'transfers');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('batch', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->integer('code');
        $table->date('date_into');
        $table->text('note_batch')->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::foreign_string($table, 'provider_id', 'providers');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_id', 'branch');
        \App\Helpers\MigrationHelper::default_time_stamps($table);

      });

      Schema::create('batch_details', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->float('cost');
        $table->bigInteger('quantity');
        $table->float('total');
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'batch_id', 'batch');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('prices', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->float('price');
        $table->text('description');
        $table->boolean('is_predetermined')->default(0);
        $table->date('due_date')->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('bank_accounts', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name_bank');
        $table->string('number_account');
        $table->string('type_account_bank');
        \App\Helpers\MigrationHelper::foreign_string($table, 'provider_id', 'providers');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('file_cabinets', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('code');
        $table->text('description');
        $table->string('code_operation');
        $table->bigInteger('quantity_input');
        $table->bigInteger('quantity_out');
        $table->float('price_unit');
        $table->float('income_out');
        $table->float('income_input');
        $table->date('date');
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_id', 'branch');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('clients', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('number_document');
        $table->string('issued_in', 200);
        $table->string('name', 200);
        $table->string('last_name', 200)->nullable();
        $table->string('firm_name', 300)->nullable();
        $table->string('tax_name', 200)->nullable();
        $table->string('tax_number', 200)->nullable();
        $table->string('city', 100)->nullable();
        $table->string('address', 300)->nullable();
        $table->string('phone', 200);
        $table->string('phone_alternative', 30)->nullable();
        $table->string('email', 100)->nullable();
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('sales', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('code');
        $table->text('sale_note')->nullable();
        $table->tinyInteger('type_sale');
        $table->tinyInteger('payment_type');
        $table->float('price_initial');
        $table->float('price_end');
        $table->date('date_sale');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_id', 'branch');
        \App\Helpers\MigrationHelper::foreign_string($table, 'client_id', 'clients');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('sale_details', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->float('price_unit');
        $table->integer('quantity');
        $table->float('price_total');
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'sale_id', 'sales');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('product_sub_categories', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        \App\Helpers\MigrationHelper::foreign_string($table, 'product_id', 'products');
        \App\Helpers\MigrationHelper::foreign_string($table, 'sub_category_id', 'sub_categories');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('checkout', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('name');
        \App\Helpers\MigrationHelper::foreign_string($table, 'branch_id', 'branch');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('checkout_details', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->float('amount_initial');
        $table->float('amount_end')->nullable();
        $table->date('date');
        $table->boolean('is_closed');
        \App\Helpers\MigrationHelper::foreign_string($table, 'checkout_id', 'checkout');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('payments', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('code');
        $table->tinyInteger('payment_type');
        $table->float('amount_total');
        $table->date('next_payment_date');
        $table->integer('days');
        $table->float('money_fine');
        $table->boolean('is_paid_out');
        \App\Helpers\MigrationHelper::foreign_string($table, 'sale_id', 'sales', true);
        \App\Helpers\MigrationHelper::foreign_string($table, 'provider_id', 'providers', true);
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

      Schema::create('shares', function (Blueprint $table) {
        \App\Helpers\MigrationHelper::id_string($table);
        $table->string('code');
        $table->float('amount');
        $table->date('date_payment');
        \App\Helpers\MigrationHelper::foreign_string($table, 'payment_id', 'payments');
        \App\Helpers\MigrationHelper::foreign_string($table, 'user_id', 'users');
        \App\Helpers\MigrationHelper::default_time_stamps($table);
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('user_permissions');
        Schema::dropIfExists('permission_template');
        Schema::dropIfExists('permission_template_detail');
        Schema::dropIfExists('units');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('sub_categories');
        Schema::dropIfExists('providers');
        Schema::dropIfExists('products');
        Schema::dropIfExists('branch');
        Schema::dropIfExists('branch_details');
        Schema::dropIfExists('transfers');
        Schema::dropIfExists('transfer_details');
        Schema::dropIfExists('batch');
        Schema::dropIfExists('batch_details');
        Schema::dropIfExists('prices');
        Schema::dropIfExists('bank_accounts');
        Schema::dropIfExists('file_cabinets');
        Schema::dropIfExists('clients');
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sale_details');

    }
}
