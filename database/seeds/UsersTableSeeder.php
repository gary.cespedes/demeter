<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user_id = \Faker\Provider\Uuid::uuid();
      $branch_id = \Faker\Provider\Uuid::uuid();

      \App\User::create([
        'id' => $user_id,
        'name' => 'root',
        'email' => 'root@gmail.com',
        'password' => bcrypt('admin123'),
        'photo' => 'image/profile/user-uploads/user_profile.png',
        'branch_id' => $branch_id
      ]);

      \App\Models\Branch::create([
        'id' => $branch_id,
        'name' => 'Central',
        'address' => 'Sin Direccion',
        'phone' => 'Sin telefono',
        'is_warehouse' => 1,
        'user_id' => $user_id
      ]);

      \App\Models\Provieder::create([
        'name' => 'GENERAL',
        'phone' => '+000000',
        'email' => 'sinEmail',
        'type_document' => 1,
        'number_document' => '00000000',
        'user_id' => $user_id
      ]);


    }
}
