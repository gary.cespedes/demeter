<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  private static $items = [
    'permission' => [
      'id' => 1000,
      'name' => 'PRINCIPAL',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_two' => [
      'id' => 1001,
      'name' => 'PANEL DE STOCK',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_three' => [
      'id' => 1002,
      'name' => 'PANEL DE VENTAS POR PRODUCTO',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_four' => [
      'id' => 1003,
      'name' => 'PANEL DE CURVA DE VENTAS',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_five' => [
      'id' => 1004,
      'name' => 'PANEL DE PANEL DE CREDITOS',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_six' => [
      'id' => 1005,
      'name' => 'PANEL DE RESUMEN DEL DIA',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_seven' => [
      'id' => 1006,
      'name' => 'PANEL DE ACCESOS DIRECTOS',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_eight' => [
      'id' => 1007,
      'name' => 'PANEL DE VENTAS DEL DIA',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_nine' => [
      'id' => 1008,
      'name' => 'PANEL DE ALERTAS',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_ten' => [
      'id' => 2000,
      'name' => 'VER REPORTES',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'permission_eleven' => [
      'id' => 2001,
      'name' => 'VER REPORTES POR V',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'NUEVO' => [
      'id' => 2002,
      'name' => 'VER  POR V',
      'module' =>'PANEL DE CONTROL',
      'visible' => TRUE
    ],
    'Usuarios' => [
      'id' => 2003,
      'name' => 'Control de Usuarios',
      'module' =>'Control Users',
      'visible' => TRUE
    ],

  ];

  public function run()
  {

    foreach (static::$items as $item){
      $permission = \App\Models\Permission::find($item['id']);
      if ($permission){
        \App\Models\Permission::find($item['id'])->update($item);
      }
      else{
        \App\Models\Permission::create($item);
      }
    }

  }
}
